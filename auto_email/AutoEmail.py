# -*- coding: utf-8 -*-
"""
Created on Mon Mar 18 13:42:57 2019

@author: rudolf.gilanik
"""

import Common as comm
from AutoEmailClasses import TraverseFolders


log_main = comm.AutoEmailLogger('main script', (comm.MAILHOST, comm.MAILPORT), comm.LOG_FILE, comm.SEND_ERROR_IN_CODE_ALERT_TO)
log_main.SendInfoLog('Spustenie scriptu')

try:
    TraverseFolders(comm.METADATA_FILE).GoTraversing()
    log_main.SendInfoLog('Uspesne ukoncenie scriptu')
    print('Uspesne ukoncene')

except ConnectionRefusedError:
    log_main.SendErrorLog(f'Prekroceny pocet povolenych odoslani emailovych sprav ({comm.EMAIL_COUNT_TRESHOLD}) v ramci jednej session!\n\n')

except ConnectionError:
    log_main.SendErrorLog('Nedokazem sa pripojit k exchange serveru alebo zlyhala komunikacia s databazou!\n\n')

except Exception:
    log_main.SendErrorLog('Nastala neosetrena chyba v kode!\n')

comm.mysqldb.CloseConnection()
comm.SMTP_sender.QuitSMTP()


    


