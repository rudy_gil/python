# -*- coding: utf-8 -*-
"""
Created on Mon Mar 18 13:36:21 2019

@author: rudolf.gilanik
"""

import os
import json
import json.decoder
import re
from pathlib import Path, PurePath

import pandas as pd

import Common as comm



EXCLUDE_FOLDER = {'_VYNECHAT.TXT', '_VYNECHAT.XLSX', '_VYNECHAT.DOCX', '_VYNECHAT.JSON'}
SUPPORT_FILES = {'_PODKLADY*.*'}     #tieto subory mozu existovat vo foldri aj bez suborov do prilohy
DEFS_FILE = '_DEFS.JSON'                                # len 1 string, nie set

#EXCLUDE_FOLDER = {'_VYNECHAT.*'}
#SUPPORT_FILES = {'_PODKLADY*.TXT', '_PODKLADY*.HTML', '_PODKLADY*.JSON', '_PODKLADY*.XLSX', '_PODKLADY*.DOCX'}
#RENAME_FILES_IN_FOLDER = '_PREMENOVAT.TXT'              # len 1 string, nie set

#FROM_ADDRESS = 'invoice@starmedia.sk'             #string s jednou adresou odosielatela
#FROM_INFO_ADDRESS = ''              #string s jednou adresou odosielatela, pre interne alerty a upozornenia


class EmailFolder:
    """
    Trieda na odoslanie suborov z daneho foldra, (ne)vymazanie prikazoveho suboru podla definovaneho pravidla
    ak to vyzaduje situacia, rozposlanie upozorneni ze existuju \n
        -nespracovane subory (t.j. bez tagu FIN)\n
        -nadbytocne subory (t.j. tie, ktore sa nikdy neodoslu, lebo nesplnaju ziadne pravidlo)\n
    \n
    Argumenty:\n
        -Folder - Path objekt s cestou k foldru, z ktoreho sa maju odoslat subory\n
        -DefsDict - dictionary, ktory obsahuje udaje o definicnom subore platny pre Folder\n
                  - {'folder_P': Path, 'filename_S': str, 'complete_P': Path}\n
        -CommandDictList - list of dicts, ktory obsahuje prikazy platne pre Folder\n
                     - [{'folder_P': Path, 'filename_S': str, 'complete_P': Path}]\n
        -AllCommandsFileList - list vsetkych existujucich prikazov v tvare napr. _FIN.TXT. Nacitane z Metadata.xlsx suboru\n
        -FromAddressesList - list vsetkych existujucich adries odosielatela v tvare napr.invoice@starmedia.sk, bez ' a ". Nacitane z Metadata.xlsx suboru
    """

    def __init__(self, Folder, DefsDict, CommandDictList, AllCommandsFileList, FromAddressesList):        
        # vytvorim log object pre zaslanie emailov pre folder
        self.log_email = comm.AutoEmailLogger('Email Folder', (comm.MAILHOST, comm.MAILPORT), comm.LOG_FILE, comm.SEND_ERROR_IN_CODE_ALERT_TO)

        self.folder = Folder
        self.defs_obj = Defs(Folder, DefsDict, CommandDictList, AllCommandsFileList, FromAddressesList)      #objekt Defs
        self.process_files_folders = ProcessFilesFolders(self.defs_obj)     #objekt na spracovanie suborov a foldrov
        self.from_addresses_list = FromAddressesList
        


    def ProcessFolder(self):
        """
        Som vo foldri, ktory neobsahuje prikazy z EXCLUDE_FOLDER.\n
        Ulohy:\n
        -spracovanie textu do emailu podla definicie\n
        -odoslanie suborov z daneho foldra podla definovanych filtrov, s priponami alebo bez\n
        -(ne)vymazanie prikazoveho suboru podla definovaneho pravidla,\n
        (ak je prikaz z vyssieho foldra ako defs, tak ho vymazat nemoze)\n
        -rozposlanie upozorneni ze existuju \n
            -nespracovane subory (t.j. bez tagu FIN)\n
        """
        print('current folder: ', self.folder)
        print('defs: ', self.defs_obj.defs_access_data['complete_P'])
        print('commands: ', self.defs_obj.command_list_access_data, '\n')

        
        # idem spracovat jednotlive prikazy
        for access_command_dict in self.defs_obj.command_list_access_data:
            try:
                command_dict = self.defs_obj.GetCommandDict(access_command_dict['filename_S'].upper())
            except KeyError:
                continue

            # zistim zoznam suborov vyhovujucich defsom
            for rule_dict in command_dict['rules']:
                # ak je command-done == ''  tak GetFilesByFilter & tag tychto suborov v db != command_dict['command']&rule_dict['rule']
                # ak je command-done != ''  tak zisti subory v db ktore maju command&rule == command-done & prienik so subormi vo foldri
                if rule_dict['command-done'] == '':
                    # z db zistim subory, ktore uz tymto prikazom boli spracovane
                    db_files = comm.mysqldb.GetFilesFromDB(Folder = self.defs_obj.folder, Command = command_dict["command"].upper(), Rule = rule_dict["rule"])
                    filtered_files = self.process_files_folders.GetFilesByFilter(command_dict, rule_dict)
                    filtered_files.difference_update(db_files)
                else:
                    db_files = comm.mysqldb.GetFilesFromDB(Folder = self.defs_obj.folder, Command = rule_dict['command-done'].upper(), Rule = rule_dict["rule"])
                    # command_dict a rule_dict v tomto volani GetFilesByFilter nemaju vyznam, cielom je ziskat vsetky subory *.* bez rezijnych
                    #filtered_files = self.process_files_folders.GetFilesByFilter(command_dict, rule_dict)
                    filtered_files = self.process_files_folders.GetFilesByFilter(None, None)
                    filtered_files.intersection_update(db_files)
                    # mam zoznam suborov, ktore boli uz spracovane predoslym prikazom a nachadzaju sa aj vo foldri
                    # teraz este musim z nich vylucit tie, ktore uz tymto prikazom boli spracovane
                    db_files = comm.mysqldb.GetFilesFromDB(Folder = self.defs_obj.folder, Command = command_dict["command"].upper(), Rule = rule_dict["rule"])
                    filtered_files.difference_update(db_files)
                    

                if len(filtered_files) == 0: continue

                from_address = rule_dict['from']
                to_address = rule_dict['to']
                cc_address = rule_dict['cc']
                bcc_address = rule_dict['bcc']
                reply_address = rule_dict['reply'] if rule_dict['reply'] else [from_address]
                
                # ak byone == True, tak pracujem so zoznamom ktory obsahuje zoznamy s jednym suborom, 
                # inak vytvorim zoznam ktory obsahuje iba jeden zoznam so vsetkymi subormi
                for file_item in [[f] for f in filtered_files] if rule_dict['byone'] else [list(filtered_files)]:
                    # vytvorim subject do emailu
                    process_email_text = ProcessEmailText(self.defs_obj, command_dict, rule_dict, file_item) 
                    email_subject = process_email_text.CreateContent('subject')
                    if email_subject is None: continue
    
                    # vytvorim text do emailu
                    email_body = process_email_text.CreateContent('body')
                    if email_body is None: continue

                    try:
                        comm.SMTP_sender.SendEmail(from_address, to_address, email_subject, email_body, Cc = cc_address, Bcc = bcc_address, Reply = reply_address, \
                                       Files = file_item, SendTextAsHTML = process_email_text.send_as_html, DontSendFiles = False)
                    except ConnectionError:
                        self.log_email.SendErrorLog('Nedokazem sa pripojit k exchange serveru\n')
                        raise
                    except Exception as exc:
                        self.log_email.SendErrorLog((
                            'Som vo funkcii ProcessFolder() a zlyhalo odoslanie suborov klientovi cez funkciu SendEmail().\n'
                            'Definicny subor {df}, folder: {fld}\n'
                            'Command: {cm}, rule: {rl}\n\n'
                            'Vstupne argumenty do funkcie SendEmail(): \n'
                            'from:{fr}\n'
                            'to: {toa}\n'
                            'cc: {cca}, bcc: {bcca}\n'
                            'reply: {rp}\n'
                            'subject: {sb}\n'
                            'body: {bd}\n'
                            'sent_as_html: {ht}\n'
                            'files:\n{fl}\n\n'
                            'Exception:\n{ex}'
                            ).format(df=self.defs_obj.defs_access_data["complete_P"], fld=self.defs_obj.folder, cm=command_dict["command"], rl=rule_dict["rule"], \
                                          fr=from_address, toa=to_address, cca=cc_address, bcca=bcc_address, rp=reply_address, sb=email_subject, bd=email_body, \
                                          ht=process_email_text.send_as_html, fl=", \n".join(str(f) for f in filtered_files), ex=exc)
                            )
                        raise
                    
                    # zapisem do db status a sent
                    try:
                        email_id = comm.mysqldb.InsertSent(From = from_address, To = to_address, Reply = reply_address, Subject = email_subject, Body = email_body, \
                                            Files = file_item, Folder = self.defs_obj.folder, Command = command_dict["command"], Rule = rule_dict['rule'], \
                                            Cc = cc_address, Bcc = bcc_address, Sent_as_html = process_email_text.send_as_html)
                    except Exception as exc:
                        self.log_email.SendErrorLog((
                            'Koncim beh scriptu! \n\n'
                            'Som vo funkcii ProcessFolder(), poslal som klientovi subory a zlyhal zapis o odoslani do db.\n'
                            'Definicny subor {df}, folder: {fld}\n'
                            'Command: {cm}, rule: {rl}\n\n'
                            'Vstupne argumenty do funkcie InsertSent(): \n'
                            'from:{fr}\n'
                            'to: {toa}\n'
                            'cc: {cca}, bcc: {bcca}\n'
                            'reply: {rp}\n'
                            'subject: {sb}\n'
                            'body: {bd}\n'
                            'sent_as_html: {ht}\n'
                            'files:\n{fl}\n\n'
                            'Exception:\n{ex}'
                            ).format(df=self.defs_obj.defs_access_data["complete_P"], fld=self.defs_obj.folder, cm=command_dict["command"], rl=rule_dict["rule"], \
                                          fr=from_address, toa=to_address, cca=cc_address, bcca=bcc_address, rp=reply_address, sb=email_subject, bd=email_body, \
                                          ht=process_email_text.send_as_html, fl=", \n".join(str(f) for f in filtered_files), ex=exc)
                            )
                        raise ConnectionError
    
                    
                    try:
                        comm.mysqldb.InsertStatus(Folder = self.defs_obj.folder, Files = file_item, Command = command_dict["command"], Rule = rule_dict['rule'], EmailID = email_id, Defs = self.defs_obj.defs_access_data['complete_P'])
                    except Exception as exc:
                        self.log_email.SendErrorLog((
                            'Koncim beh scriptu! \n\n'
                            'Som vo funkcii ProcessFolder(), poslal som klientovi subory a zlyhal zapis o spracovani suborov do db.\n'
                            'Definicny subor {df}, folder: {fld}\n'
                            'Command: {cm}, rule: {rl}\n\n'
                            'Vstupne argumenty do funkcie InsertStatus(): \n'
                            'files:\n{fl}\n\n'
                            'Exception:\n{ex}'
                            ).format(df=self.defs_obj.defs_access_data["complete_P"], fld=self.defs_obj.folder, cm=command_dict["command"], rl=rule_dict["rule"], fl=", \n".join(str(f) for f in filtered_files), ex=exc)
                            )
                        raise ConnectionError
                

        # vypisem status z db do txt vo foldri
        txt_output = comm.mysqldb.GetCurrentStatus(self.defs_obj.folder)
        db_output_to_file = self.defs_obj.folder.joinpath('_PODKLADY_VYPIS.txt')
        if txt_output == '':
            # ak nie je nic v db, tak vymazem existujuci/nezapisem vobec vypis do suboru
            if db_output_to_file.exists(): db_output_to_file.unlink()
        else:
            with open(db_output_to_file, 'w', encoding="utf-8") as f:
                f.write(txt_output)

        # ak ostali vo foldri nespracovane subory tak sa posle reminder
        self.process_files_folders.SendReminder()
        


class ProcessEmailText:
    """
    Trieda operacii nad textom v emaili. Pre kazde pravidlo sa vytvori samostatny objekt.\n
    
    Argumenty:\n
    -CommandDict - dict prikazu, ktory sa ma aplikovat\n
    -RuleDict - dict pravidla zo zoznamu pravidiel z prikazu v CommandDict, ktory sa ma aplikovat\n
    -FileItem - list suborov ktore vyhovuju podmienkam ak byone=false, list s jednym suborom ak byone=true
    """     
    def __init__(self, DefsObj, CommandDict, RuleDict, FileItem):
        self.defs_obj = DefsObj
        self.log_defs = comm.AutoEmailLogger('ProcessEmailText', (comm.MAILHOST, comm.MAILPORT), comm.LOG_FILE, comm.SEND_ERROR_IN_CODE_ALERT_TO, Subject='Problem so spracovanim textu do emailu!')
        self.command_dict = CommandDict
        self.rule_dict = RuleDict
        self.file_item = FileItem
        self.send_as_html = RuleDict['send-as-html']    # bude True, ak sa niektory z placeholders nahradil html suborom cez 'filehtml'
    
    def CreateContent(self, Destination):
        """
        Vrati text pre Destination emailu s nahradenymi placeholders.\n
                
        Argumenty:\n
        -Destination - bud "subject" alebo "body", podla toho, co sa ma tvorit\n
        -FileItem - bud zoznam suborov ak byone=false, alebo jeden subor ak byone=true\n
        """
        
        # mam istotu, ze atribut subject a body existuju a nie su prazdne
        # subject je vseobecny pojem stringu, v ktorom tvorim vysledny text, nechcelo sa mi to prepisovat ;)
        subject = self.rule_dict[Destination]
        
        # prechadzam vsetky placeholders v texte, aj tie ktore sa vyskytnu po nahradeni
        try:
            fin_subject = re.sub('@ph\d+', self.ReplacePlaceholder, subject)
            while fin_subject != subject:
                subject = fin_subject
                fin_subject = re.sub('@ph\d+', self.ReplacePlaceholder, subject)
        except (KeyError, ValueError, FileNotFoundError) as exc:
            self.log_defs.SendErrorLog((
                    'Definicny subor {df}, folder: {fol}\n'
                    'Command: {comm}, rule: {rl}\n'                        
                    'Vytvaram {dest} emailu a nastal problem v niektorej z definicii pre placeholders:\n'
                    '{ph}\n\n'
                    '{ex}\n'
                    ).format(df = self.defs_obj.defs_access_data['complete_P'], fol = self.defs_obj.folder, comm = self.command_dict['command'], rl = self.rule_dict['rule'], dest = Destination, ph = self.rule_dict['placeholders'], ex = exc))
            return None
        
        return fin_subject
                


    def ReplacePlaceholder(self, PHMatchObj):
        """
        Vrati text, ktory sa ma nahradit za dany Placeholder.\n
        
        Argument:\n
        -PHMatchObj - match objekt pre najdeny placeholder\n
        """

        start_keywords = [('month', self.PHMonth), ('pos_folder', self.PHPosFolder), ('pos_file', self.PHPosFile), ('txt', self.PHTxt), ('file', self.PHFile)]
        ph_value = self.rule_dict['placeholders'][PHMatchObj.group(0)]
        kw = list(filter(lambda tpl: ph_value.startswith(tpl[0]), start_keywords))
        if len(kw) == 1:
            return kw[0][1](ph_value)





    def PHFile(self, Arg):
        """
        Vrati obsah z txt alebo html suboru pre dany prikaz a dane pravidlo, napr. z _PODKLADY_BODY_APP0.txt \n
                
        Argument:\n
        -Arg - string v tvare 'filetxt' alebo 'filehtml'\n
        """
        # otvorim podkladovy subor podla prislusneho prikazu a pravidla
        if Arg not in ['filetxt', 'filehtml']:
            raise ValueError(f'Hodnota placeholdra "{Arg}" pre vlozenie txt alebo html subora je nespravna. Ma byt bud "filetxt" alebo "filehtml"')
            
        file_ext = Arg[4:]
        self.send_as_html = self.send_as_html or file_ext == 'html'


        supp_fname = ''
        if len(self.file_item) == 1:
            supp_fname = '*_DONT_SEND_BODY{cmd}{rl}.{ext}'.format(cmd = self.command_dict['command'].split('.')[0], rl = self.rule_dict['rule'], ext = file_ext)
            if PurePath(self.file_item[0].name).match(supp_fname):
                try:
                    supp_content = open(self.file_item[0], 'r', encoding = 'utf-8').read()
                except FileNotFoundError:
                    raise FileNotFoundError(f'Problem pri nacitani suboru {self.file_item[0]} s podkladmi pre placeholders.\n')
                supp_fname = self.file_item[0]
            else:
                supp_fname = ''
        
        if supp_fname == '':
            try:
                supp_content = ''
                supp_fname = self.defs_obj.folder.joinpath('_PODKLADY_BODY{cmd}{rl}.{ext}'.format(cmd = self.command_dict['command'].split('.')[0], rl = self.rule_dict['rule'], ext = file_ext))
                supp_content = open(supp_fname, 'r', encoding = 'utf-8').read()
            except FileNotFoundError:
                raise FileNotFoundError(f'Neexistuje subor {supp_fname} pre vlozenie jeho obsahu do subjectu alebo body emailu.\n')
        
        return supp_content


    def PHTxt(self, Arg):
        """
        Vrati n-ty element v textovom subore pre dany prikaz a dane pravidlo, napr. z _PODKLADY_APP0.txt \n
        Polozky v subore su oddelene znakom '>', prip. pre prehladnost ich davaj na novy riadok.\n
        
        Argument:\n
        -Arg - string v tvare 'txt-n', kde n je cislo od 1 po pocet poloziek v txt subore, napr. txt2 \n
        
        """
        # self.file_item je list
        # ak spracuvavam subor _DONT_SEND_ tak je iba jeden subor v liste (byone=True) a vezmem obsah z neho
        supp_fname = ''
        if len(self.file_item) == 1:
            supp_fname = '*_DONT_SEND{cmd}{rl}.txt'.format(cmd = self.command_dict['command'].split('.')[0], rl = self.rule_dict['rule'])
            if PurePath(self.file_item[0].name).match(supp_fname):
                try:
                    supp_txt = open(self.file_item[0], 'r', encoding = 'utf-8').read()
                except FileNotFoundError:
                    raise FileNotFoundError(f'Problem pri nacitani suboru {self.file_item[0]} s podkladmi pre placeholders.\n')
                supp_fname = self.file_item[0]
            else:
                supp_fname = ''
        
        if supp_fname == '':
            # nepracujem teda s _DONT_SEND suborom
            # otvorim podkladovy subor podla prislusneho prikazu a pravidla
            try:
                supp_txt = ''
                supp_fname = self.defs_obj.folder.joinpath('_PODKLADY{cmd}{rl}.txt'.format(cmd = self.command_dict['command'].split('.')[0], rl = self.rule_dict['rule']))
                supp_txt = open(supp_fname, 'r', encoding = 'utf-8').read()
            except FileNotFoundError:
                raise FileNotFoundError(f'Neexistuje textovy subor s podkladmi pre placeholders: {supp_fname}\n')
        
        try:
            part_index = int(re.search('\d+$', Arg).group(0))
        except AttributeError:
            raise ValueError(f'V hodnote placeholdra {Arg} som nenasiel na konci cislo polozky.')
        
        supp_list = supp_txt.split('>')
        
        try:
            return re.sub('\n$', '', supp_list[part_index - 1])
        except IndexError:
            raise ValueError(f'V podkladovom subore {supp_fname} neexistuje polozka c. {part_index} podla hodnoty placeholdra {Arg}.')



    def PHMonth(self, Arg):
        """
        Vrati textovu reprezentaciu mesiaca v sk alebo eng ktoru zisti bud z nazvu foldra alebo nazvu niektoreho zo suborov v zozname vyselektovanych suborov\n
        Zo suborov vrati prvy mesiac, ktory najde v niektorom z nazvov\n
        
        Argument:\n
        -Arg - string v tvare 'month(fil|fol)(s|e)', napr. monthfils \n
        
        """
        months = [('január', 'January'), ('február', 'February'), ('marec', 'March'), ('apríl', 'April'), ('máj', 'May'), ('jún', 'June'), \
                  ('júl', 'July'), ('august', 'August'), ('september', 'September'), ('október', 'October'), ('november', 'November'), ('december', 'December')]
        
        # overim spravne hodnoty v Arg, ak nenajde, generuje ValueError osetrenu vyssie
        source_index = ['fil', 'fol'].index(Arg[5:8])
        lang_index = ['s', 'e'].index(Arg[-1])
        
        source_list = [f.name for f in self.file_item] if source_index == 0 else [str(self.defs_obj.folder)]
        # ak hladam mesiac v nazvoch suborov, prvy ktory najdem beriem
        for f in source_list:
            # najde mm-yyyy kdekolvek v texte a pri mesiacoch kontroluje spravne hodnoty 1-12, na zaciatku moze byt aj 0
            m = re.search('(0?[1-9]|1[012])[-_ ](\d{4})', f)
            if m is not None:
                return '{month} {year}'.format(month = months[int(m.group(1))-1][lang_index], year = m.group(2))

        raise ValueError('Nenasiel som obdobie mm-yyyy z placeholdra v nazve alebo nazvoch:\n{}'.format(', \n'.join(source_list)))


    def PHPosFolder(self, Arg):
        """
        Vrati casti v kompletnej ceste foldra ak Arg je typu 'pos_folder_path-m'.\n
        Vrati elementy z nazvu foldra ak Arg je typu 'pos_folderm', 'pos_foldermn' alebo 'pos_folderm-'.\n
        Argument Arg: \n
        pos_folder_path-m: kde m je cislo zodpovedajuce m-tej casti od konca v kompletnej ceste foldra. Jednociferne. Oddelovac je \ . \n
            Napr. ak je cesta '\\srvdc01\STARMEDIA\General\Buying\Archiv faktur\Skeny faktur Test\4Life\03_2019' \n
            tak 'pos_folder_path-2' nahradi textom '4Life'. Cast '\\srvdc01\STARMEDIA\' neberie do uvahy a neda sa pouzit \n
            ako nahrada za placeholder. Celkovy pocet casti je v tejto ceste 6.
        pos_folderm: kde m je cislo zodpovedajuce m-temu elementu v nazve foldra od zaciatku. Jednociferne. Oddelovac je medzera. \n
            Napr. ak je nazov foldra 'abc def gh' tak 'pos_folder2' nahradi textom 'def'  
        pos_foldermn: vrati m-ty az n-ty element v nazve foldra od zaciatku. Ak n je vyssie ako pocet elementov, \n
            nevyhlasi chybu a vrati m-ty az posledny element. Jednociferne cisla m a n. Oddelovac je medzera. \n
            Napr. ak je nazov 'abc def gh' tak 'pos_folder12' nahradi textom 'abc def'  
        pos_folderm-: vrati m-ty az posledny element v nazve foldra od zaciatku. Jednociferne. Oddelovac je medzera. \n
            Napr. ak je nazov 'abc def gh' tak 'pos_folder2-' nahradi textom 'def gh'   
        """

        if Arg.startswith('pos_folder_path-'):
            # idem hladat m-ty part od konca
            try:
                numof_parts = len(self.defs_obj.folder.parts)
                part_index_m = int(Arg[-1])
                # zamerne nastavim part_index mimo rozsah, aby sa generovala vynimka ak nenadobuda povolene hodnoty od 1 po pocet urovni foldra - 1
                # prvy part je napr W:\ alebo \\srvdc01\STARMEDIA\, ten nezaradim do spracovania preto numof_parts-1
                if not (1 <= part_index_m <= numof_parts-1): part_index_m = 100 
                return self.defs_obj.folder.parts[-part_index_m]
            except (ValueError, IndexError):
                raise ValueError('Chybne definovany placeholder s hodnotou "{}". Bud nie je cislo na konci alebo je mimo dovoleny rozsah od 1 po {} pre tento folder.\n\n'.format(Arg, numof_parts-1))

        else:
            # idem pracovat s poslednym partom foldra
            # a vyuzijem uz hotovy parser pre elementy v nazve suboru, len vstupom bude 
            try:
                return self.PHPosFile('pos_file' + Arg[10:], FileItem = self.defs_obj.folder.parts[-1])
            except ValueError:
                raise ValueError('Chybne definovany placeholder s hodnotou "{}". Detaily v prvotnej exception, kde sa pos_folder namapoval na pos_file.\n\n'.format(Arg))



    def PHPosFile(self, Arg, FileItem = None):
        """
        Vrati elementy z nazvu suboru. Ak je suborov viac, spracuje prvy subor v zozname. \n
        Argument Arg: \n
        pos_filem: kde m je cislo zodpovedajuce m-temu elementu v nazve suboru od zaciatku. Jednociferne. \n
            Napr. ak je nazov 'abc def gh.txt' tak 'pos_file2' nahradi textom 'def'  \n
        pos_filemn: vrati m-ty az n-ty element v nazve suboru od zaciatku. Ak n je vyssie ako pocet elementov, \n
            nevyhlasi chybu a vrati m-ty az posledny element. Jednociferne cisla m a n. \n
            Napr. ak je nazov 'abc def gh.txt' tak 'pos_file12' nahradi textom 'abc def'  \n
        pos_filem-: vrati m-ty az posledny element v nazve suboru od zaciatku. Jednociferne. \n
            Napr. ak je nazov 'abc def gh.txt' tak 'pos_file2-' nahradi textom 'def gh'  \n
        """

        # vo file_item je list s jednym alebo viacerymi Path suborovymi objektami
        sel_file_item = self.file_item[0].stem if FileItem is None else FileItem
        filename_parts = sel_file_item.split()
        numof_parts = len(filename_parts)

        try:
            part_index_m = int(Arg[8])
            part_index_n = 0 if len(Arg[8:]) == 1 else Arg[9]
                
            # zamerne nastavim part_index mimo rozsah, aby sa generovala vynimka ak nenadobuda povolene hodnoty od 1 po pocet casti nazvu subora
            if not (1 <= part_index_m <= numof_parts): part_index_m = 100 
            
            if part_index_n == 0:
                #vratim len m-ty element
                return filename_parts[part_index_m - 1]
            elif part_index_n == '-':
                #vratim vsetko od m-teho elementu do konca
                return ' '.join(filename_parts[part_index_m - 1:])
            else:
                #vratim vsetko m-ty az n-ty element, m znizim ak treba
                return ' '.join(filename_parts[part_index_m - 1:min(int(part_index_n), numof_parts)])
                
        except (ValueError, IndexError):
            raise ValueError('Chybne definovany placeholder s hodnotou "{}". Bud nie je cislo na konci alebo je mimo dovoleny rozsah od 1 po {} pre {}.\n\n'.format(Arg, numof_parts, sel_file_item))




class ProcessFilesFolders:
    """
    Trieda operacii nad subormi a foldrami.
    """
    def __init__(self, DefsObj):
        self.defs_obj = DefsObj
        self.log_defs = comm.AutoEmailLogger('ProcessFilesFolders', (comm.MAILHOST, comm.MAILPORT), comm.LOG_FILE, comm.SEND_ERROR_IN_CODE_ALERT_TO, Subject='Problem so spracovanim suborov!')
    
    def ExcludeWorkFiles(self):
        """
        Vrati mnozinu rezijnych suborov z daneho foldra
        """

        # set rezijnych suborov: DEFS_FILE, EXCLUDE_FOLDER
        dont_include_set = {Path(self.defs_obj.folder).joinpath(f) for f in EXCLUDE_FOLDER.union({DEFS_FILE})}
        
        # podporne subory SUPPORT_FILES s postfixom prikazu a cisla pravidla, presnejsie podla patternu v SUPPORT_FILES
        support_files = set()
        for sf in SUPPORT_FILES:
            support_files = support_files.union(self.defs_obj.folder.glob(sf))

        # vsetkych prikazov z METADATA_FILE, ulozene v self.defs_obj.all_commands_list
        metadata_files = {Path(self.defs_obj.folder).joinpath(f) for f in self.defs_obj.all_commands_list}
        return dont_include_set.union(support_files).union(metadata_files)
    
    
    def GetFilesByFilter(self, CommandDict, RuleDict):
        """Vrati set suborov s realnymi nazvami (nie velkymi pismenami) ktore vyhovuju 
        filtrom daneho prikazu v a bez rezijnych suborov typu:\n
        -DEFS_FILE\n
        -EXCLUDE_FOLDER\n
        -vsetkych prikazov z METADATA_FILE, ulozene v self.defs_obj.all_commands_list\n
        -SUPPORT_FILES s postfixom prikazu a cisla pravidla\n\n
        
        Argumenty:\n
        -CommandDict - dict prikazu, ktory sa ma aplikovat\n
        -RuleDict - dict pravidla zo zoznamu pravidiel z prikazu v CommandDict, ktory sa ma aplikovat\n
        -ak niektory z argumentov je None, tak vratim zoznam vsetkych suborov vo foldri okrem rezijnych\n
        """

        files = set()
        if (CommandDict is None) or (RuleDict is None):
            files = files.union(self.defs_obj.folder.glob('*.*'))
            files = files.difference(self.ExcludeWorkFiles())
            return files
        
        # ziskam zoznam suborov podla files
        try:
            for lst in RuleDict['files'] if len(RuleDict['files'])>0 else ['*.*']:
                files = files.union(self.defs_obj.folder.glob(lst))
        except Exception:
            self.log_defs.SendErrorLog((
                    'Definicny subor {df}, folder: {fol}\n'
                    'Command: {comm}, rule: {rl}\n'                        
                    'Aplikujem filtre z atributu files na subory a nastala chyba.\n'
                    'Atribut files: {fl}\n\n'
                    ).format(df = str(self.defs_obj.defs_access_data['complete_P']), fol = self.defs_obj.folder, comm = CommandDict['command'], rl = RuleDict['rule'], fl = RuleDict['files']))
            #ak nastala chyba pri nacitani suborov pre aktualny rule, pokracujem dalsim rule v poradi
            return set()
        
        
        files = files.difference(self.ExcludeWorkFiles())

        # aplikujem podmienky podla filters
        try:
            files = self.ApplyFileNameRoot(files, RuleDict)
            files = self.ApplyFileNamePos(files, RuleDict)
            files = self.ApplyFolderRoot(files, RuleDict)
            files = self.ApplyFolderPos(files, RuleDict)

            files = self.ApplyNotFileNameRoot(files, RuleDict)
            files = self.ApplyNotFileNamePos(files, RuleDict)
            files = self.ApplyNotFolderRoot(files, RuleDict)
            files = self.ApplyNotFolderPos(files, RuleDict)
            
        except ValueError:
            self.log_defs.SendErrorLog((
                    'Definicny subor {df}, folder: {fol}\n'
                    'Command: {comm}, rule: {rl}\n'                        
                    'Aplikujem filtre z atributu filters na subory a nastala chyba. Skontroluj napr. prazdne retazce, namiesto nich ma byt prazdny list.\n'
                    'Atribut filters: {fl}\n\n'
                    ).format(df = str(self.defs_obj.defs_access_data['complete_P']), fol = self.defs_obj.folder, comm = CommandDict['command'], rl = RuleDict['rule'], fl = RuleDict['filters']))
            #ak nastala chyba pri nacitani suborov pre aktualny rule, pokracujem dalsim rule v poradi
            return set()            
        except Exception:
            self.log_defs.SendErrorLog((
                    'Definicny subor {df}, folder: {fol}\n'
                    'Command: {comm}, rule: {rl}\n'                        
                    'Aplikujem filtre z atributu filters na subory a nastala chyba.\n'
                    'Atribut filters: {fl}\n\n'
                    ).format(df = str(self.defs_obj.defs_access_data['complete_P']), fol = self.defs_obj.folder, comm = CommandDict['command'], rl = RuleDict['rule'], fl = RuleDict['filters']))
            #ak nastala chyba pri nacitani suborov pre aktualny rule, pokracujem dalsim rule v poradi
            return set()                



        return files
    
    
    def ApplyFolderRoot(self, FilesSet, RuleDict):
        """
        Vrati subory, ktore obsahuju vo svojej ceste niektoru z poloziek z atributu 'folder-root'.\n
        Mam istotu, ze atribut 'folder-root' je list.\n\n
        Argumenty: \n
        - FilesSet - mnozina Path objektov\n
        - RuleDict - pravidlo vo formate dict ktore obsahuje atribut 'folder-root' v atribute 'filters'\n
        """
        
        apply = False
        fin_files_set = set()
        for flt in RuleDict['filters']['folder-root']:
            apply = True
            fin_files_set = fin_files_set.union(filter(lambda p: flt.upper() in str(p.parent).upper(), FilesSet))

        return FilesSet.intersection(fin_files_set) if apply else FilesSet

    def ApplyNotFolderRoot(self, FilesSet, RuleDict):
        """
        Vrati subory, ktore neobsahuju vo svojej ceste ziadnu polozku z atributu 'not-folder-root'.\n
        Mam istotu, ze atribut 'not-folder-root' je list.\n\n
        Argumenty: \n
        - FilesSet - mnozina Path objektov\n
        - RuleDict - pravidlo vo formate dict ktore obsahuje atribut 'not-folder-root' v atribute 'filters'\n
        """
        
        fin_files_set = set()
        for flt in RuleDict['filters']['not-folder-root']:
            fin_files_set = fin_files_set.union(filter(lambda p: flt.upper() in str(p.parent).upper(), FilesSet))

        return FilesSet.difference(fin_files_set)


    def ApplyFolderPos(self, FilesSet, RuleDict):
        """
        Vrati subory, ktore obsahuju vo svojej ceste polozky na danej pozicii z atributu 'folder-pos-reversed'.\n
        Mam istotu, ze atribut 'folder-pos-reversed' je list listov.\n
        Pozicia je pocitana od konca, t.j. 'folder-pos-reversed':[[posledny],[predposledny],...]\n\n
        Argumenty: \n
        - FilesSet - mnozina Path objektov\n
        - RuleDict - pravidlo vo formate dict ktore obsahuje atribut 'folder-pos-reversed' v atribute 'filters'\n
        """

        def for_filter(p, i, flt2):
            pt = p.parts[:-1]       # -1 preto, lebo posledny part je filename
            if i < len(pt):
                return flt2.upper() in pt[-i].upper()
            return False
        
        apply = False
        fin_files_set = set()
        for i, flt1 in enumerate(RuleDict['filters']['folder-pos-reversed']):
            for flt2 in flt1:
                apply = True
                fin_files_set = fin_files_set.union(filter(lambda p: for_filter(p, i+1, flt2), FilesSet))

        return FilesSet.intersection(fin_files_set) if apply else FilesSet

    def ApplyNotFolderPos(self, FilesSet, RuleDict):
        """
        Vrati subory, ktore neobsahuju vo svojej ceste ziadnu polozku na danej pozicii z atributu 'not-folder-pos-reversed'.\n
        Mam istotu, ze atribut 'not-folder-pos-reversed' je list listov.\n
        Pozicia je pocitana od konca, t.j. 'not-folder-pos-reversed':[[posledny],[predposledny],...]\n\n
        Argumenty: \n
        - FilesSet - mnozina Path objektov\n
        - RuleDict - pravidlo vo formate dict ktore obsahuje atribut 'not-folder-pos-reversed' v atribute 'filters'\n
        """

        def for_filter(p, i, flt2):
            pt = p.parts[:-1]       # -1 preto, lebo posledny part je filename
            if i < len(pt):
                return flt2.upper() in pt[-i].upper()
            return False
        
        fin_files_set = set()
        for i, flt1 in enumerate(RuleDict['filters']['not-folder-pos-reversed']):
            for flt2 in flt1:
                fin_files_set = fin_files_set.union(filter(lambda p: for_filter(p, i+1, flt2), FilesSet))

        return FilesSet.difference(fin_files_set)


    def ApplyFileNameRoot(self, FilesSet, RuleDict):
        """
        Vrati subory, ktore obsahuju vo svojom nazve niektoru z poloziek z atributu 'filename-root'.\n
        Mam istotu, ze atribut 'filename-root' je list.\n\n
        Argumenty: \n
        - FilesSet - mnozina Path objektov\n
        - RuleDict - pravidlo vo formate dict ktore obsahuje atribut 'filename-root' v atribute 'filters'\n
        """
        
        apply = False
        fin_files_set = set()
        for flt in RuleDict['filters']['filename-root']:
            apply = True
            fin_files_set = fin_files_set.union(self.defs_obj.folder.glob('*{}*.*'.format(flt)))

        return FilesSet.intersection(fin_files_set) if apply else FilesSet
        
    def ApplyNotFileNameRoot(self, FilesSet, RuleDict):
        """
        Vrati subory, ktore neobsahuju vo svojom nazve ziadnu polozku z atributu 'not-filename-root'.\n
        Mam istotu, ze atribut 'not-filename-root' je list.\n\n
        Argumenty: \n
        - FilesSet - mnozina Path objektov\n
        - RuleDict - pravidlo vo formate dict ktore obsahuje atribut 'filename-root' v atribute 'filters'\n
        """
        
        fin_files_set = set()
        for flt in RuleDict['filters']['not-filename-root']:
            fin_files_set = fin_files_set.union(self.defs_obj.folder.glob('*{}*.*'.format(flt)))

        return FilesSet.difference(fin_files_set)


    def ApplyFileNamePos(self, FilesSet, RuleDict):
        """
        Vrati subory, ktore obsahuju vo svojom nazve na danom mieste polozky z atributu 'filename-pos'.\n
        Polozky v nazve suboru su oddelene medzerou\n
        Mam istotu, ze atribut 'filename-pos' je list listov.\n\n
        Argumenty: \n
        - FilesSet - mnozina Path objektov\n
        - RuleDict - pravidlo vo formate dict ktore obsahuje atribut 'filename-pos' v atribute 'filters'\n
        """
        def for_filter(p, i, flt2):
            pl = p.name.upper().split()
            if i < len(pl):
                return flt2.upper() in pl[i]
            return False
        
        apply = False
        fin_files_set = set()
        for i, flt1 in enumerate(RuleDict['filters']['filename-pos']):
            for flt2 in flt1:
                apply = True
                fin_files_set = fin_files_set.union(filter(lambda p: for_filter(p, i, flt2), FilesSet))

        return FilesSet.intersection(fin_files_set) if apply else FilesSet

    def ApplyNotFileNamePos(self, FilesSet, RuleDict):
        """
        Vrati subory, ktore neobsahuju vo svojom nazve na danom mieste ziadnu z poloziek z atributu 'not-filename-pos'.\n
        Polozky v nazve suboru su oddelene medzerou\n
        Mam istotu, ze atribut 'not-filename-pos' je list listov.\n\n
        Argumenty: \n
        - FilesSet - mnozina Path objektov\n
        - RuleDict - pravidlo vo formate dict ktore obsahuje atribut 'not-filename-pos' v atribute 'filters'\n
        """
        def for_filter(p, i, flt2):
            pl = p.name.upper().split()
            if i < len(pl):
                return flt2.upper() in pl[i]
            return False
        
        fin_files_set = set()
        for i, flt1 in enumerate(RuleDict['filters']['not-filename-pos']):
            for flt2 in flt1:
                fin_files_set = fin_files_set.union(filter(lambda p: for_filter(p, i, flt2), FilesSet))

        return FilesSet.difference(fin_files_set)


    

    def SendReminder(self, Check = False):
        """
        Odosle reminder ak najde nespracovane subory vo foldri.\n
        Pripadna chybova hlaska pride tolkokrat, kolko foldrov dany DEFS_FILE pokryva\n
        
        Argumenty:\n
        -Check - je True ak ma zbehnut iba kontrola DEFS_FILE cez funkciu Check_JSON(), v produkcii=false\n
        """
        
        # zistim pre kazde pravidlo, v akom poradi nasleduju prikazy pre plnenie daneho pravidla
        # v zozname potom vidim prvy aj posledny prikaz
        # vytvorim dict s klucmi = pravidla a hodnotami je zoznam ich prikazov
        
        reminders_addresses = set()     # mnozina vsetkych reminder adries
        all_rules_dict = {}
        # potrebujem nejaku from adresu, z ktorej sa odosle info o nadbytocnych suboroch aj ked nenajde pri kontrole na nespracovane subory ziadny subor
        # a teda sa nedostane k moznosti nastavit from_address pre zaslanie nespracovanych suborov
        
        from_address_for_left_files = ''     
        
        for command_dict in self.defs_obj.defs_data:
            for rule_dict in command_dict['rules']:
                # zistim from adresu z prveho prikazu a prveho rule, predpokladam, ze from adresa bude vacsinou v celom definicnom subore rovnaka
                if not from_address_for_left_files: from_address_for_left_files = rule_dict['from']
                
                if rule_dict['command-done'] == '':
                    # dane pravidlo sa este nesmelo vyskytnut v all_rules_dict, inak je to chyba
                    if rule_dict['rule'] in all_rules_dict.keys():
                        error_text = (
                                'Chyba v definicnom subore {df} pre folder: {fol}. '
                                'Command: "{comm}". '
                                'Rule "{rl}" ma atribut "command-done" prazdny a pouziva uz existujuci nazov, ktory je definovany v nejakom inom commande. '
                                'Premenuj rule "{rl}" tak, aby mal unikatny nazov. Alebo vypln "command-done" s nazvom prikazu, ktory ma rovnaky rule "{rl}".'
                                ).format(df = self.defs_obj.defs_access_data['complete_P'], fol = self.defs_obj.folder, comm = command_dict['command'], rl = rule_dict['rule'])
                        if Check:
                            raise ValueError(error_text)
                        else:
                            self.log_defs.SendErrorLog(error_text)
                            return
                    else:
                        all_rules_dict[rule_dict['rule']] = [command_dict['command'].upper()]
                        reminders_addresses.update(set(rule_dict['reminder']))
                else:
                    # dane pravidlo sa musi vyskytovat v prikaze "command-done", inak je to chyba
                    try:
                        command_done = self.defs_obj.GetCommandDict(rule_dict['command-done'])
                    except KeyError:
                        error_text = (
                                'Chyba v definicnom subore {df} pre folder: {fol}. '
                                'Command: "{comm}". '
                                'V rule "{rl}" ma atribut "command-done" hodnotu "{cmd_done}" ale taky prikaz sa v def. subore nevyskytuje. '
                                'Oprav hodnotu "{cmd_done}" tak, aby sa zhodovala s nazvom predosleho prikazu procese spracovania, ktory uz je definovany.'
                                ).format(df = self.defs_obj.defs_access_data['complete_P'], fol = self.defs_obj.folder, comm = command_dict['command'],\
                                rl = rule_dict['rule'], cmd_done = rule_dict['command-done'])
                        if Check: 
                            raise KeyError(error_text)
                        else:
                            self.log_defs.SendErrorLog(error_text)
                            return
                    
                    # mam istotu, ze taky prikaz command_done v DEFS_FILE existuje
                    # command_done musi obsahovat rovnaky rule
                    try:
                        # tento riadok je len na kontrolu, rule_dict_done je len pomocna premenna, v kode ju uz dalej nevyuzivam
                        self.defs_obj.GetRuleDict(command_done['rules'], rule_dict['rule'])
                    except KeyError:
                        error_text = (
                                'Chyba v definicnom subore {df} pre folder: {fol}. '
                                'Command: "{comm}". '
                                'V rule "{rl}" ma atribut "command-done" hodnotu "{cmd_done}" ale tento prikaz nema definovany rovnaky rule "{rl}". '
                                'Skontroluj nadvaznost prikazov a ich pravidiel. Rules musia mat rovnake nazvy.'
                                ).format(df = self.defs_obj.defs_access_data['complete_P'], fol = self.defs_obj.folder, comm = command_dict['command'],\
                                rl = rule_dict['rule'], cmd_done = rule_dict['command-done'])
                        if Check: 
                            raise KeyError(error_text)
                        else:
                            self.log_defs.SendErrorLog(error_text)
                            return
                    
                    # a command_done musi byt zapisany all_rules_dict lebo prikazy v DEFS_FILE musia byt v poradi 
                    # v akom sa vykonavaju
                    if (rule_dict['rule'] in all_rules_dict.keys()) and (command_done['command'].upper() in all_rules_dict[rule_dict['rule']]):
                        # vsetko je v poriadku, aktualny prikaz umiestnim hned za command_done
                        cmd_done_index = all_rules_dict[rule_dict['rule']].index(command_done['command'].upper())
                        all_rules_dict[rule_dict['rule']].insert(cmd_done_index + 1, command_dict['command'].upper())
                        reminders_addresses.update(set(rule_dict['reminder']))
                    else:
                        error_text = (
                                'Chyba v definicnom subore {df} pre folder: {fol}. '
                                'Command: "{comm}". '
                                'V rule "{rl}" ma atribut "command-done" hodnotu "{cmd_done}". Prikaz "{cmd_done}" musi byt definovany v poradi skor '
                                'ako prikaz "{comm}". Skontroluj nadvaznost prikazov, v def. subore - musia byt definovane v takom poradi, v akom sa vykonavaju.'
                                ).format(df = self.defs_obj.defs_access_data['complete_P'], fol = self.defs_obj.folder, comm = command_dict['command'],\
                                rl = rule_dict['rule'], cmd_done = rule_dict['command-done'])
                        if Check: 
                            raise KeyError(error_text)
                        else:
                            self.log_defs.SendErrorLog(error_text)
                            return
        
        if Check: return


        not_processed_files = set()         # mnozina vsetkych nespracovanych suborov ktore vyhovuju podmienkam
        
        for rule_cmds in all_rules_dict.items():
            # potrebujem zistit prvy command pre dany rule, aby som z neho vytiahol podmienky pre vyber suborov v atributoch 'files' a 'filters'
            # potrebujem zistit aj posledny command, aby som z neho vytiahol reminder adresy
            first_command_dict = self.defs_obj.GetCommandDict(rule_cmds[1][0])
            last_command_dict = self.defs_obj.GetCommandDict(rule_cmds[1][-1])
            rule_dict = self.defs_obj.GetRuleDict(first_command_dict['rules'], rule_cmds[0])
            
            filtered_files = self.GetFilesByFilter(first_command_dict, rule_dict)
            if len(filtered_files) == 0: continue
            
            # mam zoznam vyhovujucich suborov a zistim u nich vyskyt finalneho stavu v db
            db_files = comm.mysqldb.GetFilesFromDB(Folder = self.defs_obj.folder, Command = last_command_dict['command'].upper(), Rule = rule_dict["rule"])
            filtered_files.difference_update(db_files)
            not_processed_files.update(filtered_files)
        
            if len(filtered_files)>0:
                notfinal_text = (
                    'Vo foldri <a href="{fld}">{fld}</a> sa nachadzaju subory, ktore neboli odoslane na finalne spracovanie prikazom "{com}".<br>'
                    'Zoznam suborov:<br>'
                    '{fil} <br>'
                    ).format(fld = self.defs_obj.folder, fil = ', <br>'.join('<a href="' + str(f) + '">' + str(f) + '</a>' for f in filtered_files), \
                            com = last_command_dict['command'].upper())

                reminder_address=''
                try:
                    # potrebujem atribut reminder z last_command_dict preto cez GetRuleDict a
                    # nemozem pouzit reminder_address = rule_dict['reminder']
                    reminder_address = self.defs_obj.GetRuleDict(last_command_dict['rules'], rule_dict['rule'])['reminder']
                    from_address = self.defs_obj.GetRuleDict(last_command_dict['rules'], rule_dict['rule'])['from']
                    comm.SMTP_sender.SendEmail(from_address, reminder_address, f'Nespracovane subory vo foldri "{self.defs_obj.folder}"', notfinal_text, SendTextAsHTML = True)
                except ConnectionError:
                    self.log_defs.SendErrorLog('Nedokazem sa pripojit k exchange serveru\n')
                    raise
                except (FileNotFoundError, TypeError, Exception):
                    self.log_defs.SendErrorLog("""
                        Som vo funkcii SendReminder() a chcem poslat reminder na nespracovane subory vo foldri {fld}.\n
                        Definicny subor {defs}\n
                        Kontrolovany posledny command v poradi a rule: {com}, {rul}\n
                        
                        Vstupne argumenty do funkcie SendEmail: from:{frm}, reminder: {reminder}, text: {txt}
                        
                        """.format(fld = self.defs_obj.folder, defs = self.defs_obj.defs_access_data['complete_P'], com = last_command_dict, rul = rule_dict['rule'], frm = from_address , reminder = reminder_address, txt = notfinal_text))
                    continue
        
        
        # ak ostali subory nespracovane ziadnym prikazom, napr. kvoli preklepu v nazve suboru a tym padom nesplna podmienky ktore by inak splnal
        # tak poslem samostatny reminder s tymito ostavajucimi subormi, ktore nesplnaju ziadnu z podmienok na spracovanie
        left_files = self.GetFilesByFilter(None, None)      # zistim vsetky subory vo foldri okrem rezijnych
        
        # z tychto vsetkych suborov treba vylucit tie, ktore vyhovuju podmienkam v rules. 
        # A tie mozu byt este nespracovane (v not_processed_files)
        # alebo su uz spracovane v db (db_files)
        left_files.difference_update(not_processed_files)
        db_files = comm.mysqldb.GetFilesFromDB(Folder = self.defs_obj.folder, Command = None, Rule = None)
        left_files.difference_update(db_files)

        if len(left_files)>0:
            notfinal_text = (
                'Vo foldri <a href="{fld}">{fld}</a> sa nachadzaju subory, ktore nebudu odoslane ziadnym prikazom!<br>'
                'Zoznam suborov:<br>'
                '{fil} <br>'
                ).format(fld = self.defs_obj.folder, fil = ', <br>'.join('<a href="' + str(f) + '">' + str(f) + '</a>' for f in left_files))

            try:
                comm.SMTP_sender.SendEmail(from_address_for_left_files, list(reminders_addresses), f'NADBYTOCNE subory vo foldri "{self.defs_obj.folder}"', notfinal_text, SendTextAsHTML = True)
            except ConnectionError:
                self.log_defs.SendErrorLog('Nedokazem sa pripojit k exchange serveru\n')
                raise
            except (FileNotFoundError, TypeError, Exception):
                self.log_defs.SendErrorLog("""
                    Som vo funkcii SendReminder() a chcem poslat reminder na nadbytocne subory vo foldri {fld}.\n
                    Definicny subor {defs}\n
                    
                    Vstupne argumenty do funkcie SendEmail: from:{frm}, reminder: {reminder}, text: {txt}
                    
                    """.format(fld = self.defs_obj.folder, defs = self.defs_obj.defs_access_data['complete_P'], frm = from_address_for_left_files, reminder = list(reminders_addresses), txt = notfinal_text))
        
                        
        
class Defs:
    """
    Trieda pre nacitanie a pracu s definicnym suborom.\n
    
    Argument:\n
        -Folder - Path objekt, pre ktory folder na spracovanie je DefsDict urceny\n
        -DefsDict - dictionary, ktory obsahuje udaje o definicnom subore platny pre Folder\n
        -CommandList - list of dicts, ktory obsahuje prikazy platne pre Folder\n
                     - [{'folder_P': Path, 'filename_S': str, 'complete_P': Path}]\n
        -AllCommandFileList - list vsetkych existujucich prikazov v tvare napr. _FIN.TXT. Nacitane z Metadata.xlsx suboru\n
        -FromAddressesList - list vsetkych existujucich adries odosielatela v tvare napr.invoice@starmedia.sk, bez ' a ". Nacitane z Metadata.xlsx suboru
    """    
    def __init__(self, Folder, DefsDict, CommandList, AllCommandsFileList, FromAddressesList):
        self.log_defs = comm.AutoEmailLogger('Defs', (comm.MAILHOST, comm.MAILPORT), comm.LOG_FILE, comm.SEND_ERROR_IN_CODE_ALERT_TO, Subject='Problem s _defs.json')
        self.folder = Folder
        self.defs_access_data = DefsDict
        self.command_list_access_data = CommandList
        self.all_commands_list = AllCommandsFileList
        self.from_addresses_list = FromAddressesList


        # najskor overim ,ci cesta k def. suboru je obsiahnuta v cste k spracovavanemu foldru
        if not (str(self.defs_access_data['folder_P']) in str(Folder)):
            self.log_defs.SendErrorLog((
                    'Chyba v urceni definicneho suboru {df} pre folder: {fol}.\n'
                    'Folder definicneho suboru nie je sucastou cesty k foldru na spracovanie!\n'
                    ).format(df = str(DefsDict['complete_P']), fol = Folder))
            raise LookupError('Chyba pri nacitani definicneho suboru {}'.format(str(DefsDict['complete_P'])))                    


        try:
            with open(DefsDict['complete_P'], 'r', encoding = 'utf-8') as fj:
                self.defs_data = json.load(fj)      #obsahuje nacitany json subor, je to list slovnikov
        except json.decoder.JSONDecodeError:
            raise ValueError('Problem s nacitanim definicneho suboru {} \n pre folder: {}. \nAsi je porusena JSON struktura.\n\n'.format(str(DefsDict['complete_P']), Folder))            


        # kontrolna sekcia
        for item1 in self.defs_data:
            # skontrolujem najskor pritomnost vsetkych atributov
            # prva uroven
            attr_list = ['command', 'rules', 'delete-command']
            try:
                for attr in attr_list:
                    item1[attr]
            except KeyError as exc:
                raise KeyError((
                        f'Chyba v definicnom subore {str(DefsDict["complete_P"])} pre folder: {Folder}. '
                        f'Chyba atribut 1. urovne: {exc} '
                        ))            
            
            # atribut command - kontrola spravnych prikazov
            if item1['command'] not in AllCommandsFileList:
                raise ValueError((
                        'Chyba v definicnom subore {df} pre folder: {fol}. '
                        'Prikaz {cmd} nie je v zozname povolenych prikazov! '
                        ).format(df = str(DefsDict['complete_P']), fol = Folder, cmd = item1['command']))
            cmd = item1['command']

            # delete-command je boolean
            if not isinstance(item1['delete-command'], bool):
                raise ValueError((
                        'Chyba v definicnom subore {df} pre folder: {fol}. '
                        'Command: {comm}. '
                        'Atribut delete-command nie je boolean.'
                        ).format(df = str(DefsDict['complete_P']), fol = Folder, comm = cmd))

            # atribut rules - kontrola typu list
            if not (isinstance(item1['rules'], list)):
                raise ValueError((
                        'Chyba v definicnom subore {df} pre folder: {fol}. '
                        'Command: {comm}. '
                        'Atribut rules nie je list: '
                        '{rl}'
                        ).format(df = str(DefsDict['complete_P']), fol = Folder, comm = cmd, rl = item1['rules']))
            ruls = item1['rules']

            # druha uroven            
            attr_list = ['rule', 'from', 'to', 'cc', 'bcc', 'reply', 'subject', 'body', 'placeholders', 'send-as-html', 'files', 'command-done', 'byone', 'filters', 'alert', 'reminder', 'reminder-sch', 'checkpoints']
            try:
                for rule_dict in ruls:
                    for attr in attr_list:
                        rule_dict[attr]
            except KeyError as exc:
                raise KeyError((
                        'Chyba v definicnom subore {df} pre folder: {fol}. '
                        'Command: {comm}. '
                        'Chyba atribut 2. urovne: {ex} '
                        ).format(df = str(DefsDict['complete_P']), fol = Folder, comm = cmd, ex = exc))            

            # tretia uroven            
            attr_list = ['filename-root', 'filename-pos', 'folder-root', 'folder-pos-reversed', 'not-filename-root', 'not-filename-pos', 'not-folder-root', 'not-folder-pos-reversed']
            try:
                for rule_dict in ruls:
                    for attr in attr_list:
                        rule_dict['filters'][attr]
            except KeyError as exc:
                raise KeyError((
                        'Chyba v definicnom subore {df} pre folder: {fol}. '
                        'Command: {comm}. '
                        'Chyba atribut 3. urovne: {ex} '
                        ).format(df = str(DefsDict['complete_P']), fol = Folder, comm = cmd, ex = exc))      
            # po tomto checku sa mozem spolahnut, ze v item1 dicte mam vsetky atributy a mozem sa na ne odkazovat
            


            
            for rule_dict in ruls:
                # rule_dict je jedno z pravidiel pre dany command
                # atribut rule, from a command-done- kontrola typu string
                if not (isinstance(rule_dict['rule'], str) and isinstance(rule_dict['from'], str) and isinstance(rule_dict['command-done'], str)):
                    raise ValueError((
                            'Chyba v definicnom subore {df} pre folder: {fol}. '
                            'Command: {comm}, rule: {rl}, from: {frm}, command-done: {cd}. '
                            'Atribut rule, from alebo command-done nie je string.'
                            ).format(df = str(DefsDict['complete_P']), fol = Folder, comm = cmd, rl = rule_dict['rule'], frm = rule_dict['from'], cd = rule_dict['command-done']))

                # from adresa - kontrola platnej adresy
                if rule_dict['from'] not in FromAddressesList:
                    raise ValueError((
                            'Chyba v definicnom subore {df} pre folder: {fol}. '
                            'Command: {comm}, rule: {rl}. '
                            'From adresa {frm} nie je v zozname platnych adries odosielatela! '
                            ).format(df = str(DefsDict['complete_P']), fol = Folder, comm = cmd, rl = rule_dict['rule'], frm = rule_dict['from']))


                # kontrola atributov typu list
                attr_list = ['to', 'cc', 'bcc', 'reply', 'files', 'alert', 'reminder', 'checkpoints']
                not_list = [(attr, rule_dict[attr]) for attr in attr_list if not isinstance(rule_dict[attr], list)]
                if not_list:
                     raise ValueError((
                            'Chyba v definicnom subore {df} pre folder: {fol}. '
                            'Command: {comm}, rule: {rl}. '
                            'Tieto atributy nie su list: {lst} '
                            ).format(df = str(DefsDict['complete_P']), fol = Folder, comm = cmd, rl = rule_dict['rule'], lst = not_list))

                # kontrola listov vo filters
                # prva uroven
                attr_list = ['filename-root', 'filename-pos', 'folder-root', 'folder-pos-reversed', 'not-filename-root', 'not-filename-pos', 'not-folder-root', 'not-folder-pos-reversed']
                not_list = [('1-uroven', attr, rule_dict['filters'][attr]) for attr in attr_list if not isinstance(rule_dict['filters'][attr], list)]
                # druha uroven v 'filename-pos' a 'folder-pos-reversed'
                attr_list = ['filename-pos', 'folder-pos-reversed', 'not-filename-pos', 'not-folder-pos-reversed']
                not_list += [('2-uroven', attr, i) for attr in attr_list for i in rule_dict['filters'][attr] if not isinstance(i, list)]
                if not_list:
                    raise ValueError((
                            'Chyba v definicnom subore {df} pre folder: {fol}. '
                            'command: {comm}, rule: {rl}. '
                            'Tieto atributy v atribute filters nie su list: {lst} '
                            ).format(df = str(DefsDict['complete_P']), fol = Folder, comm = cmd, rl = rule_dict['rule'], lst = not_list))
                     

                # to a alert nie su prazdne
                attr_list = ['to', 'alert']
                empty_list = [attr for attr in attr_list if len(rule_dict[attr]) == 0]
                if empty_list:
                    raise ValueError((
                            'Chyba v definicnom subore {df} pre folder: {fol}. '
                            'command: {comm}, rule: {rl}. '
                            'Tieto atributy musia byt vyplnene: {lst}'
                            ).format(df = str(DefsDict['complete_P']), fol = Folder, comm = cmd, rl = rule_dict['rule'], lst = empty_list))
                    

                # niekto zo starmedie je v (to, cc alebo bcc), from a alert        
                cond1 = any(['@starmedia.sk' in s for s in rule_dict['to'] + rule_dict['cc'] + rule_dict['bcc']])
                cond2 = any(['@starmedia.sk' in s for s in rule_dict['alert']])
                cond3 = '@starmedia.sk' in rule_dict['from']
                if not (cond1 and cond2 and cond3):
                    raise ValueError((
                            'Chyba v definicnom subore {df} pre folder: {fol}. '
                            'command: {comm}, rule: {rl}. '
                            'V atributoch (to, cc, bcc), from a v alert musi byt pritomna adresa zo Starmedie! '
                            ).format(df = str(DefsDict['complete_P']), fol = Folder, comm = cmd, rl = rule_dict['rule']))

                # subject a body nie su prazdne
                if (len(rule_dict['subject']) == 0) or (len(rule_dict['body']) == 0):
                    raise ValueError((
                            'Chyba v definicnom subore {df} pre folder: {fol}. '
                            'command: {comm}, rule: {rl}. '
                            'V subject a v body nesmie byt prazdny text! '
                            ).format(df = str(DefsDict['complete_P']), fol = Folder, comm = cmd, rl = rule_dict['rule']))

                # ak je definovany command-done, tak musia byt files a filters prazdne, lebo sa neaplikuju
                if len(rule_dict['command-done']) > 0:
                    if (len(rule_dict['files']) > 0) or (len(rule_dict['filters']['filename-root']) > 0) or (len(rule_dict['filters']['folder-root']) > 0) or \
                    (len(rule_dict['filters']['filename-pos']) > 1) or (len(rule_dict['filters']['folder-pos-reversed']) > 1) or \
                    (len(rule_dict['filters']['filename-pos'][0]) > 0) or (len(rule_dict['filters']['folder-pos-reversed'][0]) > 0):
                        raise ValueError((
                                'Chyba v definicnom subore {df} pre folder: {fol}. '
                                'command: {comm}, rule: {rl}. '
                                'files: {fl}, filters: {ft}.  '
                                'Ak je definovany "command-done" tak files a filters musia byt prazdne!\n'
                                ).format(df = str(DefsDict['complete_P']), fol = Folder, comm = cmd, rl = rule_dict['rule'], fl = rule_dict['files'], ft = rule_dict['filters']))

                # 'send-as-html', byone su boolean
                attr_list = ['send-as-html', 'byone']
                not_bool = [(attr, rule_dict[attr]) for attr in attr_list if not isinstance(rule_dict[attr], bool)]
                if not_bool:
                    raise ValueError((
                            'Chyba v definicnom subore {df} pre folder: {fol}.\n'
                            'command: {comm}, rule: {rl}\n'
                            'Tieto atributy nie su boolean: {lst}\n'
                            ).format(df = str(DefsDict['complete_P']), fol = Folder, comm = cmd, rl = rule_dict['rule'], lst = not_bool))

                # reminder-sch je int
                if not isinstance(rule_dict['reminder-sch'], int):
                    raise ValueError((
                            'Chyba v definicnom subore {df} pre folder: {fol}. '
                            'command: {comm}, rule: {rl}. '
                            'Atribut reminder-sch nie je int. '
                            ).format(df = str(DefsDict['complete_P']), comm = cmd, rl = rule_dict['rule'], fol = Folder))

                
                # kontrola checkpoints pre definicny subor
                # overim, ci checkpoints sa nachadzaju v ceste, musi to byt nieco, co je spolocne pre vsetky subfoldre
                # pre ktore tieto defsy platia, napr. nazov klienta, nemoze tam byt nazov kampane ak je defs spolocny pre vsetky kampane
                chcklist = []
                for chck in rule_dict['checkpoints']:
                    if chck.upper() not in str(Folder).upper(): chcklist.append(chck)

                if len(chcklist) > 0:
                    raise LookupError((
                            'Chyba v urceni definicneho suboru {df} pre folder: {fol}. '
                            'Folder {fol} obsahuje v definicnom subore nespravne checkpoints. '
                            'Checkpoints: {ch} '
                            ).format(df = str(DefsDict['complete_P']), fol = Folder, ch = chcklist))         
                


        # zoznam prikazov podla poradia, napr.: [{0:'_APP.TXT', 1:'_FIN.TXT'}]
        #self.command_order = [{idx:item['command']} for idx, item in enumerate(self.defs_data)]



    def GetCommandDict(self, Command):
        """
        Pre zadany prikaz Command vrati dict z defsov
        """
        for cmd in self.defs_data:
            if cmd['command'].upper() == Command.upper(): return cmd
        raise KeyError('Neexistuje zapis "command": {} v definicnom subore: {}\n'.format(Command, self.defs_access_data['complete_P']))
        #return None

    def GetRuleDict(self, Rules_List, Rule_Name):
        """
        Pre zadane Rule_Name vrati dict z Rules_List v ramci konkretneho commandu, ktory sa navolil uz skor v procese
        """
        for rl in Rules_List:
            if rl['rule'] == Rule_Name: return rl
        raise KeyError('Neexistuje zapis "rule": {} v definicnom subore: {}\n'.format(Rule_Name, self.defs_access_data['complete_P']))
        #return None


class TraverseFolders:

    def __init__(self, MetadataFile):
        """
        Trieda na rekurzivne prechadzanie foldrov. \n
        
        Argumenty:
            -MetadataFile - uplna cesta k suboru, obsahujucom definicie pre beh scriptu, napr. zoznma foldrov, prikazove subory\n
        """

        # vytvorim log object pre Traverse Folders
        self.log_traverse = comm.AutoEmailLogger('Traverse Folders', (comm.MAILHOST, comm.MAILPORT), comm.LOG_FILE, comm.SEND_ERROR_IN_CODE_ALERT_TO)

        # nacitam definicny subor pre beh scriptu
        try:
            df = pd.read_excel(MetadataFile, sheet_name = 'defs1')
        except FileNotFoundError:
            self.log_traverse.SendErrorLog('Nepodarilo sa nacitat definicny subor pre beh scriptu:\n{}\n\n'.format(MetadataFile))
            raise 
        
        # v MetadataFile je zoznam foldrov na spracovanie
        self.folder_list = map(Path, df['Folders'].dropna().tolist())   #map objekt so zoznamom foldrov na scanovanie
        self.command_file_list = df['Commands'].dropna().tolist()       #list prikazovych suborov v string formate
        self.from_addresses_list = df['From addresses'].dropna().tolist()       #list from adries v string formate

    def GoTraversing(self):
        """
        Postupne prechadza kazdy folder v zozname na spracovanie.
        """
        
        for current_folder in self.folder_list:
            self.ValidFolders(current_folder, None, [])
        
        
    def ValidFolders(self, CurrentFolder, Defs, Commands):
        """
        Metoda rekurzivne vracia foldre, ktore maju platny definicny subor a obsahuju nejake subory. 
        Nekontroluje, ci subory na poslanie splnaju podmienky na odoslanie definovane v definicnom subore. 
        Vynechava foldre, ktore obsahuju subor pre vynechanie foldra (definovany v TraverseFolders.EXCLUDE_FOLDER). 
        Vynechava foldre, ktore obsahuju subor pre manualne spracovanie foldra (definovany v TraverseFolders.PROCESS_FOLDER).\n
        \n
        Navratova hodnota je tuple Path objektov na:\n
        -(platny folder s nejakymi subormi, cestu k definicnemu suboru pre dany platny folder)
        """
 
        # Rekurzivne prehladavam strukturu foldrov a hladam platne foldre s definicnym a prikazovym suborom.
        # Popri tom si znacim definicny a prikazovy subor platny pre aktualny folder a subfoldre       
        
        # nevyuzivam "rekurziu" z os.walk ale svoju vlastnu, lebo si potrebujem pocas cesty pamatat hodnoty premennych platne len pre danu uroven foldra
        root, dir_names, file_names = next(os.walk(CurrentFolder))
        
        defs = Defs
        commands = Commands
        skip_folder = False             # ak najde prikaz na vynechanie, tak preskoci folder
        #rename_file = ''               # ak najde prikaz na premenovanie, tak namiesto spracovania suborov odosle email
        
        for file_name in file_names:
            file_name_upp = file_name.upper()
            
            # ak najdem definicny subor, poznacim si aktualny folder, v ktorom sa vyskytuje a je platny pre neho a vsetky subfoldre
            if file_name_upp == DEFS_FILE:
                defs = {'folder_P': Path(root), 'filename_S': file_name, 'complete_P': Path(root).joinpath(file_name)}

            # ak najdem prikazovy subor, vyhodim existujuci prikazovy subor s rovnakym nazvom, ak je taky, a vlozim aktualny
            if file_name_upp in self.command_file_list:
                commands = [dict for dict in commands if dict['filename_S'].upper() != file_name_upp]
                commands.append({'folder_P': Path(root), 'filename_S': file_name, 'complete_P': Path(root).joinpath(file_name)})
            
            # ak najdem prikaz na vynechanie tak preskocim aktualny folder
            skip_folder = skip_folder or file_name_upp in EXCLUDE_FOLDER
            
        
        # ak existuje definicny subor tak som nasiel platny folder na spracovanie
        # prikaz nemusi existovat, pretoze chcem aj upozornovat na foldre, v ktorych existuju nespracovane alebo nepremenovane subory
        if skip_folder: return
        if defs:
            try:
                #ef = EmailFolder(Path(root), defs, rename_file, commands, self.command_file_list )
                ef = EmailFolder(Path(root), defs, commands, self.command_file_list, self.from_addresses_list)
            except Exception:
                self.log_traverse.SendErrorLog((
                        'Nastala chyba pri vytvarani EmailFolder objektu:\n'
                        'Definicny subor {df} pre folder: {fol}.\n'
                        'Nepokracujem dalej v spracovani tohto foldra, ale pokracujem jeho subfoldrami.\n'
                        ).format(df = str(defs['complete_P']), fol = CurrentFolder))                
            else:
                # ak nenastala ziadna vynimka pri vytvarani ef objektu, pokracuje v spracovani foldra
                ef.ProcessFolder()

        for current_folder in dir_names:
            self.ValidFolders(Path(root).joinpath(current_folder), defs, commands.copy())

        # ak je v commands prikaz z tohto foldra tak po spracovani vsetkych suborov v tomto foldri a vsetkych subfoldrov ho vymazem podla definicie v defs.json

        if defs:
            #ak defs adresa je obsiahnuta v prikaze na urovni foldra root tak vymaz prikaz ak treba podla defs
            if 'ef' in locals():
                for cmd in filter(lambda x: (Path(root) == x['folder_P']) and (defs['folder_P'] in x['complete_P'].parents), commands):
                    try:
                        cmd_dict = ef.defs_obj.GetCommandDict(cmd['filename_S'])
                    except KeyError:
                        continue
                    if cmd_dict['delete-command']:
                        cmd['complete_P'].unlink()
            else:
                self.log_traverse.SendErrorLog((
                        'Nastala chyba pri vymazavani prikazoveho suboru po spracovani vsetkych subfoldrov pretoze definicny subor obsahoval chybu. '
                        'Definicny subor {df} pre folder: {fol}. '
                        ).format(df = str(defs['complete_P']), fol = CurrentFolder))                   

