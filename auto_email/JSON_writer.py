# -*- coding: utf-8 -*-
"""
Created on Mon Mar 18 11:57:44 2019

@author: rudolf.gilanik
"""

import json
from pathlib import Path
import pandas as pd

from AutoEmailClasses import Defs
from AutoEmailClasses import ProcessFilesFolders
import Common as comm

def Check_JSON():
    """
    Zadaj cestu k foldru, ktory obsahuje _defs.json ktory chces skontrolovat do globalnej premennej defs_folder a zavolaj tuto funkciu.\n
    Skontroluje, ci json ma spravnu strukturu a ci je logika vyplnenia jednotlivych atributov spravna.
    """
    defs_file = Path(defs_folder).joinpath('_defs.json')
    try:
        with open(defs_file, 'r', encoding = 'utf-8') as f:
            f = json.load(f)

        defs_dict = {'folder_P': Path(defs_folder), 'filename_S': '_defs.json', 'complete_P': defs_file}
        acfl = pd.read_excel(comm.METADATA_FILE, sheet_name = 'defs1')['Commands'].dropna().tolist()
        fal = pd.read_excel(comm.METADATA_FILE, sheet_name = 'defs1')['From addresses'].dropna().tolist()
        defs_obj = Defs(Path(defs_folder), DefsDict = defs_dict, CommandList = [], AllCommandsFileList = acfl, FromAddressesList = fal)
        process_file_obj = ProcessFilesFolders(defs_obj)
        process_file_obj.SendReminder(True)
        print (f'Subor {defs_file} je v poriadku.')
    except Exception as exc:
        print(exc)

    
def Write_JSON():
    """
    Do foldra srvdc01\DataScience\Internal Jobs\AutomateEmails zapise do subora _defs_fill.json vyplnenu json strukturu v spravnom formate.\n
    Potom treba skontrolovat logiku spravneho vyplnenia cez Check_JSON().
    """
    json_file=Path(r'\\srvdc01\DataScience\Internal Jobs\AutomateEmails\_defs_fill.json')
    with open(json_file, 'w') as fp:
        json.dump(json_pretty_filled, fp, indent = 4, default=str)



json_pretty_filled = [
{
'command': '_FIN.TXT', 
'delete-command': True,
'rules': 
    [
    {
    'rule': 'TCCC',
    'from': '',
    'to': ['rudolf.gilanik@starmedia.sk'],
    'cc': [],
    'bcc': [],
    'reply': ['rudolf.gilanik@starmedia.sk'],
    'subject': 'Invoice - @ph1 - SK',
    'body': """Hello,

attached you can find invoices for AVON Offline  –  in March 2019.

Kind regards.
    """,
    'placeholders': 
        {
        '@ph1': 'monthfole',
        '@ph2': 'pos_file3',
        '@ph3': 'pos_file4'
        },
    'send-as-html': False,
    'files': ['*.pdf'],
    'command-done': '',
    'byone': True,
    'filters': 
        {
        'filename-root': [],
        'filename-pos': [[]],
        'folder-root': [],
        'folder-pos-reversed': [[]],
        'not-filename-root': [],
        'not-filename-pos': [[]],
        'not-folder-root': [],
        'not-folder-pos-reversed': [[]]
        
        },
    'alert': ['rudolf.gilanik@starmedia.sk'],
    'reminder': ['rudolf.gilanik@starmedia.sk'],
    'reminder-sch': 24,
    'checkpoints': ['TCCC']
    }
    ]
}
]


# sem zadaj cestu k foldru, ktory obsahuje _defs.json ktory chces skontrolovat    
defs_folder = r'\\srvdc01\STARMEDIA\Clients\STOCK\2019\ANALYZY\COMPETITIVE DATA\Na odoslanie'

#Write_JSON()
Check_JSON()
        
comm.mysqldb.CloseConnection()
comm.SMTP_sender.QuitSMTP()