See description in English at the end of this document.

# Popis

Script automatizuje zasielanie suborov podla vopred zadanych definicii a prikazov zadanych uzivatelmi. Rekurzivne prechadza zadane foldre, hlada definicne subory a prikazy na zaklade ktorych vie, ci vobec a ako ma posielat subory klientom.  

Funkcionalita, ktoru script umoznuje:  

- v stanovenom case automaticky vygeneruje emaily a pripoji subory (prip. len samostatny email bez suborov)  
- vopred definovany zoznam foldrov (vratane ich subfoldrov) v xlsx subore sa bude scanovat a hladat subory na odoslanie. Folder (a jeho subfoldre) tvori teda zakladnu jednotku, pre ktoru sa podla zadanych pravidiel generuju emaily  
- odosielaju sa len nove subory, ktore do scanovanych foldrov pribudli a ktore splnaju podmienku na odoslanie. Pre tento ucel sa odoslane subory eviduju v databaze aby nedochadzalo k ich opatovnemu zasielaniu.  
- podmienky na odoslanie sa urcuju definicnymi subormi _defs.json a prikazmi vo forme textovych suborov, pre kazdy folder resp. subfoldre zvlast  
- pre kazdy folder zvlast je mozne v definicnom subore _defs.json urcit zoznam prijemcov, reply emailovu adresu pre odpoved, dynamicky generovany subject a body emailu (napr. je mozne vkladat cislo faktury, aktualny mesiac, fragmenty z nazvu suboru alebo foldra atd.), posielanie novych suborov po jednom alebo vsetky naraz, podmienky pre zaradenie suborov do jednotlivych emailov, upozornenie emailom na interne adresy na nove este neodoslane subory, upozornenie emailom na interne adresy na nadbytocne subory ktore sa nikdy neodoslu (t.j. nesplnaju ziadnu z podmienok na odoslanie, napr. v pripade preklepu v nazve suboru)
- viackrokove odosielanie suborov, napr. v prvom kroku sa odoslu subory na schvalenie, v druhom kroku sa odoslu subory po schvaleni  
- uzivatel pre dany folder riadi odosielanie emailov cez prikazy vo forme textovych suborov, napr. *_FIN.txt*. Prikazy moze zadavat manualne podla potreby (napr. prikaz zada az po skompletizovani vsetkych suborov), alebo moze byt prikaz pritomny stale a vtedy sa vsetky nove subory odoslu pri najblizsej prilezitosti automaticky  
- v kazdom foldri sa nachadza textovy subor s informaciami o uz odoslanych suboroch  
- specialnym prikazom je mozne vynechat nejaky folder (subfolder) zo spracovania  
- emaily mozu byt vo forme obycajneho textu (t.j. bez moznosti formatovania textu) alebo v html formate (t.j. s moznostou formatovania textu)
- je mozne zasielat emaily, ktore budu obsahovat linky na ine subory alebo stranky
- je mozne dynamicky vkladat do emailu udaje, ktore sa nachadzaju v pomocnych textovych suboroch (napr. v pripade, ze sa kazdy mesiac meni obsah vkladany do emailu)
- pri kazdom spusteni scriptu sa informacie o spracovani zapisu do logovacieho suboru
- v pripade chyby pocas behu scriptu sa odosle sprava o chybe na zadefinovany interny email  

## Umiestnenie

Zdrojove kody su umiestnene na Bitbuckete v repozitari pripojenom k starmedia repozitari: git clone git@bitbucket.org:rudy_gilanik/auto_email.git  
Script zbieha na \\\\srvapp1 a tam je umiestneny v: C:\Users\script\Projects\auto_email

## Konstanty

Konstanty definovane v **Common.py**:  
**METADATA_FILE** = Path(r'\\srvdc01\DataScience\Internal Jobs\AutomateEmails\Metadata.xlsx') - zoznam foldrov pre prechadzanie a zoznam vsetkych prikazov, ktore sa pouzivaju pri zasielani suborov (pre kontrolne ucely)  
**LOG_FILE** =  Path(r'\\srvdc01\DataScience\Internal Jobs\AutomateEmails\LogAutomateEmails.txt') - na zapis logov. Do logu sa zapisuju vsetky spustenia scriptu, chybove hlasky a info o ukonceni scriptu. Okrem toho sa chybove spravy zasielaju aj emailom na adresu/y  
**SEND_ERROR_IN_CODE_ALERT_TO** = ['rudolf.gilanik@starmedia.sk']
**SEND_ERROR_IN_CODE_ALERT_FROM** = 'AutoEmailLogger@starmedia.sk' - emailova adresa, z ktorej sa budu odosielat chybove spravy  
**EMAIL_COUNT_TRESHOLD** = 100  - pocet odoslanych emailovych sprav na klientov, po ktorych script povazuje svoje spravanie za chybne/zacyklene a ukonci vykonavanie scriptu chybovou hlaskou  
**MAILHOST** = adresa mailoveho servera (moze byt IP napr. 192.168.1.21 alebo text, napr. mail.starmedia.sk)  
**MAILPORT** = port mailoveho servera  
**MAILCREDS** = dict loginov a hesiel pre prihlasenie do mailoveho servera, ak je na nom zapnuta autentifikacia. Aktualne je vypnuta.  

**DBHOST** = 'xxx' - adresa mysql servera  
**DATABASE** = 'xxx' - nazov databazy pre potreby scriptu  
**DB_ADMIN** = '', **DB_PASSWORD** = '' - obsahuju prazdne retazce, zapis tam pristupove udaje pre potreby vytvorenia databazy na inom mysql serveri a potom ich vymaz  

Pristupove udaje do databazy definovane v subore **creds.json** v aktualnom pracovnom adresari:  
**user** a **password** - json struktura s tymito dvoma atributmi  
Subor creds.json je zaradeny do .gitignore.  

Konstanty definovane v **AutoEmailClasses.py**:  
**EXCLUDE_FOLDER** = {'\_VYNECHAT.TXT', '\_VYNECHAT.XLSX', '\_VYNECHAT.DOCX', '\_VYNECHAT.JSON'} - subory, ktorymi mozem vylucit folder a jeho subfoldre zo spracovania  
**SUPPORT_FILES** = {'\_PODKLADY\*.\*'} - wildcard notacia pre pomocne subory pre script, napr. pre podklady na generovanie textu do emailu alebo pre vypisy z databazy  
**DEFS_FILE** = '\_DEFS.JSON' - definicny subor s nastavenim pre spracovanie suborov v danom foldri/foldroch  
**FROM_ADDRESS** = 'invoice@starmedia.sk' - string s jednou adresou odosielatela. Musi to byt @starmedia.sk adresa.  
**FROM_INFO_ADDRESS** = FROM_ADDRESS - string s jednou adresou odosielatela, pre interne alerty a upozornenia  

## Databaza

Vykonane akcie zapisuje do mysql databazy do dvoch tabuliek: status a sent.  
Tabulka **status**:  
**id** - poradove cislo akcie  
**folder** - nazov foldra v ktorom sa nachadza spracovany subor  
**file_name** - nazov subora spracovaneho prikazom a pravidlom podla nasledujucich dvoch stlpcov  
**command, rule**  
**defs** - cesta k definicnemu suboru  
**email_id** - id emailovej spravy z tabulky sent, v ktorej bol subor pripojeny  

Tabulka **sent**:  
**id** - poradove cislo emailu  
**from** - emailova adresa odosielatela, jedna, string  
**to** - emailove adresy prijimatela, aj viac adries, list stringov, s vacsim rezervovanym priestorom pre text ako pri cc, bcc, reply  
**cc, bcc, reply** - emailove adresy dalsich prijemcov, aj viac adries, list stringov,  
**subject, body** - text emailu  
**files** - subory s kompletnou cestou odoslane danym emailom  
**folder** - folder z ktoreho sa spracovali subory podla nasledujucich dvoch stlpcov  
**command, rule**  
**sent_as_html** - true ak email bol odoslany vo formate html, false pre plain text  
**date_time** - datum a cas odoslania emailu  

## Popis procesu spracovania

Script bude zbiehat napr. 2x za den a prechadzat vopred definovane foldre a hladat subory na zaslanie.
Jeden z takych foldrov moze byt napr.  W:\General\Buying\Archiv faktur\Skeny faktur YYYY. Su v nom klientske foldre a v nich mozu byt foldre po mesiacoch, v nich foldre po kampaniach atd., script prejde postupne vsetky.
Komunikacia so scriptom bude prebiehat pomocou prikazov vo forme prazdnych txt suborov, t.j. v prislusnom foldri sa takyto subor vytvori a script bude vediet, co ma urobit (napr. po vytvoreni prikazu  \_FIN.txt sa subory vo foldri odoslu na finalne spracovanie u klienta). Tento txt subor sa vytvori jednoducho right-clickom v danom foldri a v menu sa vyberie New/Text document a po vytvoreni subor premenovat na \_FIN.txt. Tieto subory budu vytvarat bud ludia z cls alebo traffic manager, prip. podla dohody. Mal by to byt vzdy clovek, ktory vie, v akom stave sa proces zasielania faktur u daneho klienta nachadza. Dalsi taky prikaz okrem \_FIN.txt moze byt \_APP.txt, podla ktoreho bude script vediet, ze subory sa maju najskor odoslat na schvalenie. Vtedy sa najskor do foldra, v ktorom su uz ulozene vsetky subory na odoslanie, vlozi prikaz \_APP.txt, script ich odosle na schvalenie a potom, ked ich klient schvali, tak sa do foldra vlozi prikaz \_FIN.txt a script ich posle na finalne spracovanie u klienta. Po kazdom spracovani suborov sa do databazy ulozi zaznam o tom, aky prikaz sa pouzil na spracovanie suborov, t.j. vidime z toho, v akom stave sa subory v danom foldri nachadzaju. Tento sposob komunikacie so scriptom povazujem za najjednoduchsi a najlahsie implementovatelny bez potreby nejakej grafickej aplikacie na zadavanie prikazov. Zatial ;)  Prikazov moze byt viac, podla toho ake kroky treba vykonat. Vsetky tieto prikazy treba zapisat do suboru METADATA_FILE spolu so zoznamom foldrov na spracovanie.  

Priklad takeho spracovania faktury ktora ide najskor na schvalenie ku klientovi:  
-do foldra sa vlozi subor faktura.pdf  
-vlozi sa do foldra prikaz \_APP.txt  
-script najde prikaz \_APP.txt a podla neho vie, ze ho ma odoslat najskor na schvalenie, takze ho odosle podla nastavenia v definicnom subore DEFS_FILE pre tohto klienta a tento prikaz  
-zapise spracovanie suboru faktura.pdf do db a vymaze prikaz \_APP.txt  
-script pri naslednom zbiehani nerobi nic, lebo vo foldri nie je ziadny prikaz  
-ak je faktura u klienta schvalena, tak sa vlozi do foldra prikaz \_FIN.txt.  
-script posle subor faktura.pdf na finalne spracovanie u klienta podla udajov v definicnom subore DEFS_FILE pre tohto klienta a tento prikaz  
-zapise spracovanie suboru faktura.pdf do db a vymaze prikaz \_FIN.txt  
-hotovo, dalej sa uz so subormi pracovat nebude  

Moze sa zadefinovat, ze prikazove subory sa nebudu vymazavat ale ostanu vo foldri (t.j. nebude nutne tam zakazdym vytvarat prikazovy txt subor) a potom vzdy, ked tam pribudnu nove subory, tak sa hned spracuju podla daneho prikazu. Nenechavaj trvaly prikaz \_FIN.txt v pripade, ze je sucastou viac-krokoveho spracovania (napr. najskor \_APP.txt a potom \_FIN.txt). Inak nastane situacia, ze po odoslani suborov na schvalenie v prvom kroku cez \_APP.txt (a teda treba cakat na schvalenie od klienta) sa hned odoslu cez \_FIN.txt aj na finalne spracovanie.  
Ak script pocas spracovania najde subory, ktore nie su spracovane finalnym prikazom (t.j. poslednym v poradi na seba nadvazujucich prikazov v subore DEFS_FILE, vacsinou teda prikazom \_FIN.txt ale nie je to podmienka) tak posle reminder so subjectom ktory zacina "Nespracovane ...".  
Ak script pocas spracovania najde subory, ktore nie su spracovane ziadnym prikazom (napr. kvoli preklepu v nazve suboru ak ho pravidlo pouziva) tak posle reminder so subjectom ktory zacina "NADBYTOCNE ...".  

Ak niektory folder budeme chciet vynechat zo spracovania, vlozi sa prikaz \_VYNECHAT.txt (napr. do foldrov v ktorych uz boli subory odoslane klientovi). Tento prikaz plati aj pre jeho subfoldre.
Prikaz, ktory script najde je platny pre dany folder a aj jeho subfoldre. Plati posledny prikaz v poradi ak je viac rovnakych.  

Script ma problem pri spracovani suborov, ktore maju v nazve interpunkcne znamienka, preto subory na odoslanie nesmu obsahovat interpunkciu, inak sa nazov zmeni napr. na Untitled 00012.pdf. Nazvy sa zmenia iba externym prijemcom, v emailoch na interne adresy sa zobrazuju spravne.  

## Detailny popis suboru DEFS_FILE  

Pre kazdeho klienta s pomocou modulu JSON_writer.py, funkcie Write_JSON() a vyplneneho dict-u json_pretty_filled vytvori jednorazovo specialny definicny subor s nazvom podla DEFS_FILE v ktorom budu definovane pravidla. Po vytvoreni suboru ho skontroluj cez funkciu Check_JSON(). Ta skontroluje json syntax a niektore logicke vazby a podmienky platne pre definicny subor.  
Logicke vazby, ktore sa kontroluju:  
-rule, ktore ma prazdny atribut 'command-done', musi mat unikatny nazov  
-ak je atribut 'command-done' vyplneny, tak musi obsahovat existujuci prikaz v DEFS_FILE  
-ak je v rule A v atribute 'command-done' uvedeny prikaz C, tak prikaz C musi mat vo svojej definicii rule A (t.j. rule s rovnakym nazvom)  
-prikazy v DEFS_FILE musia byt uvedene v takom poradi, v akom sa maju vykonat  

Na prvej, najvyssej urovni dictu su atributy:  
-**'command'**: povinny, velke pismena, nazov prikazu, napr. \_FIN.TXT, alebo \_APP.TXT. Su to nazvy suborov=prikazov, ktore bude script hladat v danom foldri. Moze to byt text do 10 znakov. Musi byt uvedeny v subore METADATA_FILE.  
-**'delete-command'**: povinny, true ak sa ma prikaz po jeho aplikacii vymazat. Vymazat sa vsak moze len prikaz, ktory je v rovnakom foldri ako je subor DEFS_FILE alebo v jeho subfoldroch.  
-**'rules'**: povinny, list jedneho alebo viacerych pravidiel. V ramci aplikacie prikazu mozu byt pouzite pravidla, podla ktorych sa z daneho foldra vyberu do emailov rozne subory. Napr. sa do jedneho emailu vyberu vsetky subory zo subfoldra CocaCola a do druheho vsetky subory zo subfoldra Fanta.  

V atribute "rules" su atributy:  
-**'rule'** - povinny, nazov pravidla, moze to byt text do 30 znakov. Ak je definovanych viacero prikazov, musia byt v kazdom prikaze pouzite rovnake nazvy pravidiel lebo na seba nadvazuju.  
-**'from'** - adresa odosielatela. Bud bude prazdna a vtedy sa pouzije hodnota z FROM_ADDRESS, alebo sa vyplni a pouzije sa uvedena adresa  
-**'to'** - povinny, list stringov = emailovych adries prijimatelov, ak je rozsiahly zoznam adries, tak to daj sem do to, nie do cc.  
-**'cc'** - list stringov = emailovych adries cc prijimatelov  
-**'bcc'** - list stringov = emailovych adries bcc prijimatelov  
-**'reply'** - list stringov = emailovych reply adries na ktore pride odpoved klienta. Ak je prazdny, tak sa priradi hodnota z atributu 'from'  
V niektorom z 'to', 'cc' alebo 'bcc' musi byt uvedena aspon jedna adresa @starmedia.sk  
V 'from' musi byt uvedena adresa @starmedia.sk ak je uvedena hodnota v definicnom subore.  
-**'subject', 'body'** - povinne, text do emailu, aj cez viac riadkov s oddelovacom \n (v plain text) alebo &lt;br&gt; a inym html formatovanim textu (v html texte). Mozu sa vyuzit placeholders.  
-**'placeholders'** - dict v ktorom su placeholders ako kluce v tvare '@phn':keyword_string kde n je cislo, aj viacciferne a keyword_string je vyhradene slovo z nasledujuceho zoznamu. Priklad zapisu: '@ph1':'monthfols'. Keywords a ich vyznam:  
_'month':_  
+ _'monthfols', 'monthfole'_ - nahrad placeholder textom v tvare mesiac yyyy ak je s na konci alebo month yyyy ak je e na konci. Obdobie hlada v nazve foldra a musi byt v tvare mm-yyyy, prip. vyhovuje patternu '(0?\[1-9\]|1\[012\])\[-_ \](\d{4})'  
+ _'monthfils', 'monthfile'_ - nahrad placeholder textom v tvare mesiac yyyy ak je s na konci alebo month yyyy ak je e na konci. Obdobie hlada v nazve niektoreho zo suborov, ktore vyhovuju podmienkam a musi byt v tvare mm-yyyy, prip. vyhovuje patternu '(0?\[1-9\]|1\[012\])\[-_ \](\d{4})'  

*'pos_folder':*  
+ *'pos_folder_path-m'* - kde m je cislo zodpovedajuce m-tej casti od konca v kompletnej ceste foldra. Jednociferne. Oddelovac je \\. Napr. ak je cesta '\\\srvdc01\STARMEDIA\General\Buying\Archiv faktur\Skeny faktur Test\4Life\03_2019' tak 'pos_folder_path-2' nahradi textom '4Life'. Cast '\\\srvdc01\STARMEDIA\' neberie do uvahy a neda sa pouzit ako nahrada za placeholder. Celkovy pocet casti je v tejto ceste 6.  
+ *'pos_folderm'* - kde m je cislo zodpovedajuce m-temu elementu v nazve foldra od zaciatku. Jednociferne. Oddelovac je medzera. Napr. ak je nazov foldra 'abc def gh' tak 'pos_folder2' nahradi textom 'def'  
+ *'pos_foldermn'* - vrati m-ty az n-ty element v nazve foldra od zaciatku. Ak n je vyssie ako pocet elementov, nevyhlasi chybu a vrati m-ty az posledny element. Jednociferne cisla m a n. Oddelovac je medzera. Napr. ak je nazov 'abc def gh' tak 'pos_folder12' nahradi textom 'abc def'  
+ *'pos_folderm-'* - vrati m-ty az posledny element v nazve foldra od zaciatku. Jednociferne. Oddelovac je medzera. Napr. ak je nazov 'abc def gh' tak 'pos_folder2-' nahradi textom 'def gh'  

*'pos_file':* - vrati elementy z nazvu suboru. Ak je suborov viac, spracuje prvy subor v zozname.  
+ *'pos_filem'* - kde m je cislo zodpovedajuce m-temu elementu v nazve suboru od zaciatku. Jednociferne. Oddelovac je medzera. Napr. ak je nazov 'abc def gh.txt' tak 'pos_file2' nahradi textom 'def'  
+ *'pos_filemn'* - vrati m-ty az n-ty element v nazve suboru od zaciatku. Ak n je vyssie ako pocet elementov, nevyhlasi chybu a vrati m-ty az posledny element. Jednociferne cisla m a n. Oddelovac je medzera. Napr. ak je nazov 'abc def gh.txt' tak 'pos_file12' nahradi textom 'abc def'  
+ *'pos_filem-'* - vrati m-ty az posledny element v nazve suboru od zaciatku. Jednociferne. Oddelovac je medzera. Napr. ak je nazov 'abc def gh.txt' tak 'pos_file2-' nahradi textom 'def gh'  

_'txtn'_ - kde n je cislo zodpovedajuce n-temu elementu bud v subore \_PODKLADY_{command}{rule}.txt alebo v subore \*_DONT_SEND_{command}{rule}.txt. Hodnoty n zacinaju od 1, co zodpoveda prvej polozke v zozname. Polozky v subore su oddelene znakom '>'. Zoznam poloziek v subore moze mat tvar napr. "prva polozka>druha polozka". Subor \*_DONT_SEND_{command}{rule}.txt je ekvivalent k \_PODKLADY_{command}{rule}.txt, tiez sa z neho vezme obsah do vytvaraneho emailu ale nakoniec sa subor neodosle, sluzi pre odosielanie emailov bez prilozenych suborov.    
Ak pouzijem \*_DONT_SEND_{command}{rule}.txt subor, nemozem zaroven pouzit subor \_PODKLADY_{command}{rule}.txt. Pri pouziti \*_DONT_SEND_{command}{rule}.txt suboru treba do definicneho suboru zapisat byone=true a do files "\*_DONT_SEND_{command}{rule}.txt".   
Priklady nazvov suborov: \_PODKLADY_FINSamsung.txt, 190626_DONT_SEND_FINSamsung.txt   

_'file':_  Podobne ako pri bode vyssie, subory \*_DONT_SEND_BODY_{command}{rule}.txt alebo \*_DONT_SEND_BODY_{command}{rule}.html sluzia na odosielanie emailov bez prilozenych suborov. Z tychto suborov sa vezme obsah pre skonstruovanie emailu ale subor sa neodosle. Pri pouziti tychto suborov treba do definicneho suboru zapisat byone=true a do files "\*_DONT_SEND_BODY_{command}{rule}.txt" alebo "\*_DONT_SEND_BODY_{command}{rule}.html" a do send-as-html true ak pripona suboru je html, inak false. Vid priklady definicnych suborov vo foldri Priklady. Ak pouzijem \*_DONT_SEND_BODY_{command}{rule}.txt alebo html subor, nemozem zaroven pouzit subor \_PODKLADY_BODY_{command}{rule}.txt alebo html subor.    
+ _'filetxt'_ - vlozi obsah suboru txt. Nazov suboru musi byt \_PODKLADY_BODY_{command}{rule}.txt alebo \*_DONT_SEND_BODY_{command}{rule}.txt.  
+ _'filehtml'_ - vlozi obsah suboru html. Nazov suboru musi byt \_PODKLADY_BODY_{command}{rule}.html alebo \*_DONT_SEND_BODY_{command}{rule}.html.  

-**'send-as-html'** - povinny, true ak sa ma email poslat ako html. V texte/body emailu nezabudni potom pouzit &lt;br&gt; ako oddelovac riadkov. Email sa automaticky posiela ako html ak je niektory z placeholders typu 'filehtml'  
-**'files'** - list stringov v tvare wildcards podla ktorych sa maju vyberat subory do emailu. Medzi jednotlivych zapismi v liste je OR. Automaticky sa vylucuju rezijne subory, t.j. vsetky subory z EXCLUDE_FOLDER, SUPPORT_FILES, DEFS_FILE a prikazy zo suboru METADATA_FILE. Atribut 'files' moze byt aj prazdny, vtedy vyberie vsetky subory, ktore su vo foldri okrem rezijnych.  
-**'byone'** - povinny, true, ak sa maju subory, ktore vyhovuju podmienkam posielat v emailoch po jednom. pri generovani textu do emailu sa sa potom aplikuju placeholders suvisiace s nazvom suboru pre kazdy subor zvlast, takze na vsetky subory vo foldri, ktore maju v nazve pattern mm-yyyy,  mozes pouzit placeholder napr. 'monthfils' a ak je 'byone'=true, tak sa do textu kazdeho emailu vlozi obdobie zistene zvlast z nazvu jednotlivych suborov  
-**'filters'** - dict atributov _'filename-root', 'filename-pos', 'folder-root' a 'folder-pos-reversed'_ medzi ktorymi je operator AND a medzi polozkami atributu je operator OR  
_'filename-root'_ - list stringov (case insensitive) ktore sa maju vyskytovat v nazve suboru, medzi polozkami plati operator OR  
_'filename-pos'_ - list listov stringov (case insensitive) ktore sa maju vyskytovat v nazve suboru na danej pozicii (ako root), poradie listov v liste zodpoveda pozicii elementu v nazve foldra. Oddelovac je medzera. Napr. [[],['abc', 'def']] znamena, ze na druhej pozicii v nazve suboru sa musi v nazve foldra vyskytovat bud 'abc' alebo 'def'. Medzi polozkami plati operator OR  
_'folder-root'_ - list stringov (case insensitive) ktore sa maju vyskytovat v nazve foldra, medzi polozkami plati operator OR  
_'folder-pos-reversed'_ - list listov stringov (case insensitive) ktore sa maju vyskytovat v nazve foldra, poradie listov v liste zodpoveda pozicii elementu v nazve foldra, napr. [[],['abc', 'def']] znamena, ze na druhej pozicii od konca sa musi v nazve foldra vyskytovat bud 'abc' alebo 'def'. Medzi polozkami plati operator OR  
_'not-filename-root'_ - list stringov (case insensitive) ktore sa nemaju vyskytovat v nazve foldra, ani jeden z nich nesmie byt v nazve subora  
_'not-filename-pos'_ - list listov stringov (case insensitive) ktore sa nemaju vyskytovat v nazve suboru na danej pozicii (ako root, ziaden z nich), poradie listov v liste zodpoveda pozicii elementu v nazve foldra. Oddelovac je medzera. Napr. [[],['abc', 'def']] znamena, ze na druhej pozicii v nazve suboru sa nesmie v nazve foldra vyskytovat ani 'abc' ani 'def'.  
_'not-folder-root'_ - list stringov (case insensitive) ktore sa nemaju vyskytovat v nazve foldra, ani jeden z nich nesmie byt v nazve foldra  
_'not-folder-pos-reversed'_ - list listov stringov (case insensitive) ktore sa nemaju vyskytovat v nazve foldra, poradie listov v liste zodpoveda pozicii elementu v nazve foldra, napr. [[],['abc', 'def']] znamena, ze na druhej pozicii od konca sa nesmie v nazve foldra vyskytovat ani 'abc' ani 'def'.  
-**'command-done'** - ak sa aplikuje viacero prikazov (napr. najskor schvalovanie potom finalizacia) tak sa tu v druhom a dalsich prikazoch v poradi uvedie predosly prikaz a tym sa zabezpeci nadvaznost prikazov na seba. Prikazy  definovane cez atribut 'command', na prvej urovni dictu, musia byt definovane v takom poradi, v akom sa vykonavaju, ak sa vyuziva nadvaznost prikazov definovana cez 'command-done'. Atributy 'files' a 'filters' musia byt potom prazdne. Bud je teda definovany 'command-done' a 'files' a 'filters' su prazdne alebo je 'command-done' prazdny a aplikuju sa  podmienky z 'files' a 'filters'. Cize len v prvom prikaze v poradi sa definuju 'files' a 'filters', v ostatnych su prazdne a zapisuje sa len do 'command-done'. Z db sa vezmu subory, na ktore sa aplikoval prikaz uvedeny v 'command-done', preto nie su potrebne ziadne podmienky. Zapis prikazu v command-done je napr. "\_APP.TXT", t.j. aj s priponou a velkymi pismenami.  
-**'alert'** - povinny, list stringov = emailovych adries prijimatelov chybovych hlasok pocas behu scriptu. Mal by tu byt niekto, kto vie opravit chybovy stav. V 'alert' musi byt uvedena aspon jedna adresa @starmedia.sk  
-**'reminder'** - povinny, list stringov = emailovych adries prijimatelov upozorneni pocas behu scriptu na existenciu suborov, ktore neboli spracovane finalnym prikazom, t.j. prikazom, poslednym v poradi v DEFS_FILE. Reminder sa moze poslat pri kazdom spusteni scriptu. Subory, na ktore sa reminder vztahuje sa zistuje z prveho prikazu v poradi, zaslanie remindera podla existencie spracovania poslednym prikazom.  
-**'reminder-sch'** - neimplementovana funkcionalita, zamer bol posielat reminders az po uplynuti casu tu uvedeneho od posledneho zaslania remindra  
-**'checkpoints'** - povinny, list stringov = kontrolne stringy (case insensitive), ktore sa musia vyskytovat v nazve foldra kazdeho foldra, pre ktory plati dany DEFS_FILE. T.j. nemozes mat v ramci viacerych pravidiel pri jednom prikaze rozne checkpoints. Kvoli kontrole, aby sa neaplikovali nahodou nastavenia z DEFS_FILE ktore patria inemu foldru. Musi tu byt koren slova z kompletnej cesty foldra, ktory je specificky pre daneho klienta.  

Cize funkcia tohto definicneho suboru je taka, ze ak script najde nejaky prikaz, tak sa pozrie do tohto definicneho suboru a vytiahne si z neho informacie o tom, ake subory a kam sa maju zaslat a s akym textom v emaili.  
Nazvy rezijnych suborov su case insensitive.  

Ak su nespravne adresy prijimatelov, tak pride odpoved nie na reply ale na from sdresu. Preto treba zabezpecit preposielanie tychto sprav o nedoruceni na konkretnu adresu cloveka, ktory potom opravi preklepy v adresach prijemcov.  

## Dalsie moznosti pouzitia scriptu  

Definicne subory aj prikazy je mozne pouzit aj tak, ze sa ich platnost bude prenasat aj na vsetky subfoldre. To znamena, ze definicny subor sa nebude musiet umiestnovat do kazdeho foldra 01-2019, 02-2019 atd. ale umiestni sa do foldra vo vyssej urovni, a jeho platnost bude pre vsetky subfoldre daneho klienta. Podobne to plati aj pre prikazove subory.  
V definicnom subore je mozne vytvorit viacero pravidiel pre jeden prikaz. Napr. pre prikaz \_APP.txt sa mozu vytvorit dve pravidla, ktore inak spracuju subory, ktore maju v nazve CocaCola a inak tie ktore maju v nazve Fanta (pomocou filters ktore som spomenul vyssie).  
V definicnom subore je mozne vytvorit viacero prikazov ktore na seba nadvazuju, ako je popisane vyssie, ale aj take, ktore na seba nenadvazuju. To je vhodne v situaciach, ak je potrebne vykonat viacero krokov ale nad roznymi subormi (t.j. s roznymi nazvami). Napr. \_APP.txt sa pouzije na subory ktore obsahuju v nazve slovo "schvalit" a \_FIN.txt na tie ktore neobsahuju "schvalit". To by sa dalo vykonat aj jednym prikazom \_FIN.txt a dvoma roznymi pravidlami, ale cez dva prikazy to bude pochopitelnejsie. Alebo mas v dvoch roznych subfoldroch subory, pricom tie z prveho chces odoslat hned a tie z druheho az ked budu vsetky pohromade. Vtedy pre subory z prveho foldra vytvoris \_APP.txt a pre druhy \_FIN.txt ktore mozes zadavat v foldri, ktory obsahuje tieto dva subfoldre.  
Bude mozne pouzit ako telo emailu aj txt (bez formatovania textu) alebo html (s formatovanim textu) subor, v ktorom bude napr. popisana situacia za posledne obdobie. Ak ten subor niekto vytvori a vlozi sa do daneho foldra, tak sa moze pouzit ako text emailu.  
Moze sa pouzit aj pomocny txt subor, v ktorom budu specificke udaje ktore sa menia z mesiaca na mesiac a ktorymi sa nahradia placeholders v generickom texte emailu.  

## Zaverecne poznamky  

Adresa **FROM_ADDRESS** alebo ta, ktora je vypisana priamo v DEFS_FILE subore v atribute 'from', musi byt priradena k existujucemu kontu v Starmedii. Ak sa totiz vyskytne preklep v niektorej z adries prijemcov, tak mailserver prijemcu odosle email o nedoruceni na adresu FROM_ADDRESS, resp. 'from' a nie na adresu 'reply'. Aktualne je adresa FROM_ADDRESS nastavena ako samostatne emailove konto s nazvom "invoice@starmedia.sk" (bez konta v AD). Toto konto je pripojene v outlooku k uzivatelom rudolf.gilanik a na traffic oddeleni. Na tomto konte su vytvorene pravidla a subfoldre pre triedenie emailov odoslanych na klientov, pre nespracovane a pre nadbytocne subory.  
Ked bola adresa FROM_ADDRESS nastavena ako alias ku script@starmedia.sk tak, sa emaily s fakturami neodosielali z adresy FROM_ADDRESS ale z adresy script@starmedia.sk, t.j. alias nefungoval ako odosielatel.  
V buducnosti sa bude prechadzat na novy server - zmeni sa mailhost a bude mozne usetrit licencie za emailove konta pre script a invoice.  

Skontroluj aj pristupove prava pre uzivatela script pre subory vo foldroch, ktore prechadza. Napr. pre folder W:\General\Buying\Archiv faktur ma script R+W prava (cez clenstvo v skupine FS_Starmedia_General_RW) ale vyskytuju sa tam aj subory s pravami len pre skupinu FS_Starmedia_Internal_Ostatne_RW, takze uzivatel script sa musel pridat aj do skupiny FS_Starmedia_Internal_Ostatne_RW.  

Obcas cez webmail [mail.starmedia.sk](https://mail.starmedia.sk) skontroluj, ci nie su v schranke pre FROM_ADDRESS nejake nepreposlane emaily (napr. odmietnute emaily od klienta kvoli preklepu v adrese).  

Obcas cez Workbench skontroluj, ci neostali nejake neuzavrete connection na databazu data_science_1. Vidis to ked sa prihlasis ako Root a pod linkom Client Connections vlavo v paneli.  

Ak je from odlisny od reply tak moze znizovat spam score. Informuj o tom klienta a ak ma dosah na nastavenie spam filtra, moze ho nastavit aby score neznizoval.  

Moze sa pouzit ako from aj adresa kolegov, ktora sa bude zhodovat s adresou reply to. Je to mozne a mam to overene. Ked som vsak poslal email z Misovej adresy na chybnu gmail adresu, s nastavenym reply na moju adresu, tak prisla odpoved o nedoruceni na Misovu adresu.  

# Description

The script is sending files by definitions (= json configuration files) and commands created by the users. The given directories are traversing recursively, looking for definition files and commands by which the script decides, whether and how to send the files in that folder to the clients (the external recipients).

## Functions available  

-System task scheduler runs this script regularly which in turn generates the emails with the attached files, links to the ftp or just an email without any files.  
-The given list of folders (including their subofolders) in the given xlsx file will be scanned and looked for files to send. In such a way the folder (and its subfolders) is the basic entity for which the script generates the emails by given definition.  
-Only new files added to the scanned folders and only files which fulfil the rules in definitions are sent. For this reason all files sent are recorded in the database to avoid their repeated sending.  
-The rules by which the files are sent are defined in the configuration file *_defs.json* and with the commands in the form of text files separately for each folder.  
-Following settings and rules are defined for each folder in the *_def.json* file:

- list of recipients (to, cc, bcc)
- reply address
- dynamically generated subject and body consisted of static text and dynamic elements like invoice number, current month, fragments from file names sent, fragments from folder name and so on  
- sending one file per email or with all files at once in one email
- rules defining which files are allowed for sending as an attachment
- internal reminder email address for files in folder not sent or for files in folder which don't fulfil any rules for sending (e.g. because of typo in the file name)
- html (with the formatted text) or plain text format (without the text formatting) of an email  

-User can control sending of emails for a given folder through the commands in the form of empty text files, e.g. *_FIN.txt*. User can create the commands manually when needed (e.g. when all files in that folder are already placed) or the command can be placed permanently and in that case all the new files placed in that folder are sent automatically at the earliest opportunity  
-Sending emails in more steps which reflect the processes for a given recipient (e.g. the first step is invoice approval, the second step is sending invoices to the clients'system after approval). Steps are triggered with the individual commands for each step (e.g *_APP.txt* and *_FIN.txt*).
-There is a file for users with information about the sent files in each folder  
-There is a special command for excluding the given folder from the further processing  
-Emails can contain links to the other files (e.g. on ftp) or to the pages instead of large attached files  
-There is a possibility to dynamically insert elements from the auxiliary txt files (e.g. in the case of individual text messages each month)
-Result of each run of the script is written into the log file  
-If an error appears during the processing then an email with error details is sent to the given email address  
