# -*- coding: utf-8 -*-
"""
Created on Tue Mar 19 10:44:02 2019

@author: rudolf.gilanik
"""
import logging
import logging.handlers
from pathlib import Path, PurePath

from datetime import datetime
import json

import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.utils import formataddr
from email.header import Header
from email import encoders
import mimetypes

import mysql.connector


#jednorazovo vyplnit, vytvorit databazu a potom vymazat
DB_ADMIN = ''
DB_PASSWORD = ''




with open(r'../creds.json', 'r', encoding = 'utf-8') as f:
        cred_dict = json.load(f)['autoemail']
        
        METADATA_FILE = Path(cred_dict['metadata_file'])
        LOG_FILE =  Path(cred_dict['log_file'])
        
        MAILHOST = cred_dict['mailhost']
        MAILPORT = cred_dict['port']
        MAILCREDS = cred_dict['credentials']
        SEND_ERROR_IN_CODE_ALERT_TO = cred_dict['send_error_in_code_alert_to']
        SEND_ERROR_IN_CODE_ALERT_FROM = cred_dict['send_error_in_code_alert_from']
        EMAIL_COUNT_TRESHOLD = cred_dict['email_count_treshold']
        
        DBHOST = cred_dict['dbhost']
        DATABASE = cred_dict['database']
        USER = cred_dict['user']
        PASSWORD = cred_dict['password']


class AutoEmailLogger:

    def __init__(self, Name, MailHost, LogFile, ToAddr, FromAddr = SEND_ERROR_IN_CODE_ALERT_FROM, Subject = 'Exception in code!', Credentials = None):
        """
        Trieda pre logovanie sprav typu INFO a ERROR:
            -INFO a ERROR (napr. exceptions) spravy zapisuje do log suboru\n
            -ERROR spravy zasiela aj do emailu\n
            
        Argumenty:
            Name - pomenovanie objektu alebo modulu ku ktoremu sa vztahuje logovanie\n
            MailHost - adresa emailoveho servera\n
            LogFile - uplna cesta k suboru pre zapis logov\n
            ToAddr - zoznam emailovych adries, pre zasielanie emailov s ERROR spravami\n
            FromAddr - adresa, z ktorej budu prichadzat emaily s ERROR spravami\n
            Subject - predmet emailu s ERROR spravami\n
            Credentials - tuple ('user','pwd') s prihlasovacimi udajmi na server\n
        """
        
        self.logger = logging.getLogger(Name)
        # nemusim pridavat handler ak uz nejaky existuje pre dany Name, sposobilo by to duplikaciu sprav v log vystupe
        if not len(self.logger.handlers):
            self.logger.setLevel(logging.INFO)
            formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    
            logHandlerFile = logging.handlers.RotatingFileHandler(LogFile, maxBytes=1000000, backupCount=2, encoding='utf-8')
            logHandlerFile.setLevel(logging.INFO)
            logHandlerFile.setFormatter(formatter)
            self.logger.addHandler(logHandlerFile)
            
            logHandlerEmail = logging.handlers.SMTPHandler(mailhost = MailHost, 
                                                           fromaddr = FromAddr, 
                                                           toaddrs = ToAddr,
                                                           subject = Subject,
                                                           credentials = Credentials,
                                                           secure = None)
            logHandlerEmail.setLevel(logging.WARNING)
            logHandlerEmail.setFormatter(formatter)
            self.logger.addHandler(logHandlerEmail)
        
    def SendInfoLog(self, Message):
        """Zapis spravu do logovacieho suboru na urovni INFO"""
        self.logger.info(Message)

    def SendWarningLog(self, Message):
        """Zapis spravu do logovacieho suboru a posli email na urovni WARNING"""
        self.logger.warning(Message)

    def SendErrorLog(self, Message):
        """Zapis spravu do logovacieho suboru a posli email na urovni ERROR"""
        self.logger.error(Message, exc_info=True)



class SMTPSender:
    """
    Trieda na zasielanie emailov vratane priloh.\n
    Interne si kontroluje kolko emailov pocas jednej session poslal, ak prkroci treshold, generuje vynimku.
    """

    def __init__(self):
        self.email_counter = 0
        try:
            self.server = smtplib.SMTP(MAILHOST, MAILPORT)
            self.server.starttls() # Secure the connection
            #self.server.login(From, MAILCREDS[From])            
        except Exception as exc:
            raise ConnectionError('Problem s pripojenim k Exchange serveru\n{}\n'.format(exc))
        
    def SendEmail(self, From, To, Subject, Text, Cc = None, Bcc = None, Reply = None, Files = None, SendTextAsHTML = False, DontSendFiles = True):
        """
        Odosle email s nastavenim podla vstupnych argumentov. \n
        From - jeden string s odosielatelom \n
        To, Cc, Bcc, Reply - listy so stringovymi adresami \n
        Subject - string s predmetom emailu \n
        Text - string, obsah body emailu. Defaultne je ako PlainText. \n
        SendTextAsHTML - True ak Text je v Html formate \n
        Files - list Path objektov \n
        DontSendFiles - True ak mam zaslat aj *_DONT_SEND_*.* files
        
        Raise: FileNotFoundError, TypeError, Exception
        """
        DONT_SEND_FILE = '*_DONT_SEND_*.*'    #tieto subory sa beru ako subory na odoslanie, vezme sa z nich text do emailu a vo faze odosielania sa vyradia z prilohy, mozu existovat samostatne vo foldri a vyvolaju odoslanie aj bez suborov do prilohy
        
        if self.email_counter > EMAIL_COUNT_TRESHOLD:
            raise ConnectionRefusedError('Prekroceny pocet povolenych odoslani emailovych sprav v ramci jednej session!')
        
        # predchadzanie pripadu zmeny hodnot argumentov vo funkcii cez .append a pod.
        if Cc is None: Cc=[]
        if Bcc is None: Bcc=[]
        if Reply is None: Reply=[]
        if Files is None: Files=[]
        
        msg = MIMEMultipart()
        #msg.set_charset('utf-8')
        try:
            #msg['from'] = From
            msg['from'] = formataddr((str(Header('Starmedia', 'utf-8')), From))
            msg['to'] = ", ".join(To)
            if Cc: msg['cc'] = ", ".join(Cc)
            if Reply: msg.add_header('reply-to', ", ".join(Reply))
        except:
            raise TypeError('Problem pri tvorbe zoznamu prijemcov vo funkcii SendEmail, skontroluj ci su prijemcovia v To, CC a Reply uvedeni ako list stringov')
            
        msg['subject'] = Subject
            
        msg.attach(MIMEText(Text, 'html' if SendTextAsHTML else 'plain', _charset = 'utf-8'))
            
        for f in Files:
            # vynecham *_DONT_SEND_*.* files ak DontSendFiles == False
            if not PurePath(f.name).match(DONT_SEND_FILE) or DontSendFiles:
                try:
                    with open(f, "rb") as file:
                        mime_types, mime_encoding = mimetypes.guess_type(f.name)
                        try:
                            mime_type = mime_types.split(r'/',1)[0]
                            mime_subtype = mime_types.split(r'/',1)[1]
                        except:
                            mime_type = 'application'
                            mime_subtype = 'octet-stream'

                        part = MIMEBase(mime_type, mime_subtype)
                        part.set_payload(file.read())
                        encoders.encode_base64(part)
                        part.add_header('Content-Disposition', 'attachment', filename = f.name)
                        msg.attach(part)

                except FileNotFoundError as exc:
                    raise FileNotFoundError('Problem s nacitanim suboru {}\n{}\n'.format(f, exc))
        
        try:
            #self.server.starttls() # Secure the connection
            #self.server.login(From, MAILCREDS[From])
            self.server.sendmail(From, To + Cc + Bcc, msg.as_string())
            self.email_counter += 1
        except TypeError as exc:
            raise TypeError('Emailove adresy treba vlozit ako listy stringov\n{}\n'.format(exc))
        except Exception:
            raise

    def QuitSMTP(self):
        self.server.quit()




class MySQL:
    def __init__(self):
        self.cnx = mysql.connector.connect(user=USER, password=PASSWORD, host=DBHOST, database=DATABASE)
        self.cursor = self.cnx.cursor()
    

    def InsertSent(self, From, To, Reply, Subject, Body, Files, Folder, Command, Rule, Sent_as_html, Cc = '', Bcc = ''):
        """
        Vstupom su atributy a ich hodnoty pre INSERT\n
        Vlozi zaznam o odoslani emailu so vsetkymi jeho detailami\n
        """

        add_sent = (
                "INSERT INTO sent (`from`, `to`, `cc`, `bcc`, `reply`, `subject`, `body`, "
                "`files`, `folder`, `command`, `rule`, `sent_as_html`, `date_time`) "
                "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
                )
        if not isinstance(From, str): From = ", ".join(str(f) for f in From)
        if not isinstance(To, str): To = ", ".join(str(f) for f in To)
        if not isinstance(Reply, str): Reply = ", ".join(str(f) for f in Reply)
        if not isinstance(Files, str): Files = ", ".join(f.name for f in Files)
        if not isinstance(Folder, str): Folder = str(Folder)
        if not isinstance(Cc, str): Cc = ", ".join(str(f) for f in Cc)
        if not isinstance(Bcc, str): Bcc = ", ".join(str(f) for f in Bcc)
        
        data_sent = (From, To, Cc, Bcc, Reply, Subject, Body, Files, Folder, Command, Rule, Sent_as_html, datetime.now())

        try:
            self.cursor.execute(add_sent, data_sent)
            self.cnx.commit()
        except Exception:
            raise
        
        return self.cursor.lastrowid
        
    def InsertStatus(self, Folder, Files, Command, Rule, EmailID, Defs):
        """
        Vstupom su atributy a ich hodnoty pre INSERT\n
        Vlozi zaznam do DB o spracovani suborov vo Files z foldra Folder danym prikazom Command a Rule.\n
        EmailID je ID prislusneho emailu, ktory bol odoslany.\n
        Defs je cesta k defs.json suboru z ktoreho sa aplikovali pravidla\n
        Pre kazdy file v zozname Filenames zapisem zaznam do db\n
        """

        add_status = (
                "INSERT INTO status (folder, file_name, command, rule, defs, email_id) "
                "VALUES (%s, %s, %s, %s, %s, %s)"
                )

        if not isinstance(Folder, str): Folder = str(Folder)
        if not isinstance(Defs, str): Defs = str(Defs)
        
        for f in Files:
            data_status = (Folder, f.name, Command, Rule, Defs, EmailID)
            self.cursor.execute(add_status, data_status)
        self.cnx.commit()


    def GetFilesFromDB(self, Folder, Command, Rule):
        """
        Vrati set suborov (Path objektov) z foldra Folder ktore boli spracovane prikazom Command a Rule\n
        Ak Command a Rule je None, tak vrati iba subory z daneho Foldra\n
        """

        if not isinstance(Folder, str): Folder = str(Folder)

        if (Command is None) and (Rule is None):
            query = "SELECT file_name FROM status WHERE folder = %s"
            data_status = (Folder, )
        else:
            query = "SELECT file_name FROM status WHERE folder = %s AND command = %s AND rule = %s"
            data_status = (Folder, Command.upper(), Rule)
            
        self.cursor.execute(query, data_status)
    
        return {Path(Folder).joinpath(file_name[0]) for file_name in self.cursor}


    def GetCurrentStatus(self, Folder):
        """
        Vrati textovy vypis pre zadany folder s informaciami o stave spracovania suborov.
        """
        
        if not isinstance(Folder, str): Folder = str(Folder)
        
        #query = "SELECT folder, file_name, command, rule, date_time FROM status WHERE folder=%s ORDER BY command, rule, file_name, date_time"
        query = ("SELECT status.folder, status.file_name, status.command, status.rule, sent.date_time "
        "FROM status LEFT JOIN sent ON status.email_id = sent.id "
        "WHERE status.folder=%s ORDER BY command, rule, file_name, date_time;")

        data_status = (Folder,)
        self.cursor.execute(query, data_status)
        
        list_of_tuples = self.cursor.fetchall()
        list_txt = ''
        if len(list_of_tuples)>0:
            list_txt =  f'Stav spracovania k: {datetime.now().strftime("%d.%m.%Y %H:%M:%S")}\n'
            list_txt += f'Pre folder:         {Folder}\n\n'
            list_txt += '{fn:<50}{cm:<10}{rl:<30}{dt:<20}\n'.format(fn = 'Názov súboru', cm = 'Príkaz', rl = 'Pravidlo', dt = 'Dátum a čas spracovania')
            for folder, file_name, command, rule, date_time in list_of_tuples:
                list_txt += '{fn:<50}{cm:<10}{rl:<30}{dt:<20}\n'.format(fn = file_name, cm = command, rl = rule, dt = date_time.strftime("%d.%m.%Y %H:%M:%S"))

        return list_txt
        

    
    def CreateDatabase(self):
        """
        Manualne vloz prihlasovacie udaje pre admin konto.\n
        Vytvori databazu v mysql potrebnu pre chod tohto scriptu\n
        Tato funkcia sa zavola len jednorazovo, manualne, ako sucast instalacie scriptu\n
        """
        try:
            cnx_admin = mysql.connector.connect(user= DB_ADMIN, password= DB_PASSWORD, host=DBHOST)
            cursor_admin = cnx_admin.cursor()
            cursor_admin.execute("CREATE DATABASE {} DEFAULT CHARACTER SET 'utf8'".format(DATABASE))        
            cnx_admin.close()
        except mysql.connector.Error as err:
            print("Failed creating database: {}".format(err))
            exit(1)

    def CreateTable_status(self):
        """
        Vytvori tabulku status v mysql potrebnu pre chod tohto scriptu\n
        Tato funkcia sa zavola len jednorazovo, manualne, ako sucast instalacie scriptu\n
        """
        query = (
            "  CREATE TABLE `status` ("
            "  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,"
            "  `folder` varchar(300) NOT NULL,"
            "  `file_name` varchar(100) NOT NULL,"
            "  `command` char(10) NOT NULL,"
            "  `rule` char(30) NOT NULL,"
            "  `defs` varchar(150) NOT NULL,"
            "  `email_id` INT(10) UNSIGNED NOT NULL,"
            "  PRIMARY KEY (`id`))"
            )

        try:
            self.cursor.execute(query)        
        except mysql.connector.Error as err:
            print("Failed creating table 'status': {}".format(err))
            exit(1)
        
    def CreateTable_sent(self):
        """
        Vytvori tabulku sent v mysql potrebnu pre chod tohto scriptu\n
        Tato funkcia sa zavola len jednorazovo, manualne, ako sucast instalacie scriptu\n
        """
        query = (
            "  CREATE TABLE `sent` ("
            "  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,"
            "  `from` char(50) NOT NULL,"
            "  `to` varchar(5000) NOT NULL,"
            "  `cc` varchar(5000) NOT NULL,"
            "  `bcc` varchar(500) NOT NULL,"
            "  `reply` varchar(500) NOT NULL,"
            "  `subject` varchar(200) NOT NULL,"
            "  `body` varchar(7000) NOT NULL,"
            "  `files` varchar(2000) NOT NULL,"
            "  `folder` varchar(300) NOT NULL,"
            "  `command` char(10) NOT NULL,"
            "  `rule` char(30) NOT NULL,"
            "  `sent_as_html` BOOLEAN NOT NULL,"
            "  `date_time` datetime NOT NULL,"
            "  PRIMARY KEY (`id`))"
            )

        try:
            self.cursor.execute(query)        
        except mysql.connector.Error as err:
            print("Failed creating table 'sent': {}".format(err))
            exit(1)


        
    def CloseConnection(self):
        if self.cnx is not None: 
            self.cnx.close()



SMTP_sender = SMTPSender()
mysqldb = MySQL()