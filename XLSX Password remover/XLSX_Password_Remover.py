import zipfile
from pathlib import Path
import re
import tempfile
import shutil
import xml.etree.ElementTree as ET
import sys
import argparse

from lxml import etree

def unprotect_xlsx(xlsx_fname, temp_dir = None, dest_dir = None, post_fix = None):
    """
    Removes password protection from worksheets and workbook in xlsx file. \n
    Arguments: 
    -xlsx_fname = absolute path to the xlsx file 
    -temp_dir = path to the directory where temporary directory can be created, if None, directory of xlsx file will be used 
    -dest_dir = path to the directory where the unprotected file will be placed, if None, directory of xlsx file will be used 
    -post_fix = postfix for unprotected filename, if '', the original xlsx file will be replaced with the unprotected one, if None, postfix will be "_unprotected" \n

    Returns dict: { \n
    Status: 
    -'ok' = passwords removed from xlsx file, 
    -'no_passw' = xlsx file contains no passwords, nothing done, 
    -'not_xlsx' = provided file is not xlsx, nothing done, 
    -error message in case of some error occured, nothing done 
    Unprotected_File: Path object of the new unprotected xlsx file if Status='ok', else None \n
    } 
    """
    
    xlsx_fname = Path(xlsx_fname)
    temp_dir = xlsx_fname.parent if temp_dir is None else Path(temp_dir)
    dest_dir = xlsx_fname.parent if dest_dir is None else Path(dest_dir)
    if post_fix is None: post_fix = '_unprotected'

    fin_dict = {'Status': '', 'Unprotected_File': None}
    generated_temp_dir = None

    if xlsx_fname.suffix != '.xlsx':
        fin_dict['Status'] = 'not_xlsx'
        return fin_dict

    try:
        generated_temp_dir = Path(tempfile.mkdtemp(dir = temp_dir))
        temp_name = Path.joinpath(generated_temp_dir, 'new.xlsx')
        with zipfile.ZipFile(xlsx_fname, 'r') as zipread:
            with zipfile.ZipFile(temp_name, 'w') as zipwrite:
                for item in zipread.infolist():
                    #odomkni subory .xml len z foldra xl/worksheets/
                    if ((Path('xl/worksheets') == Path(item.filename).parent) and (Path(item.filename).suffix == '.xml')) or Path(item.filename) == Path('xl/workbook.xml'):
                        sheet_file = Path(zipread.extract(item, path = generated_temp_dir))
                        
                        tree = ET.parse(sheet_file)
                        root = tree.getroot()
                        name_spaces = dict([node for _, node in ET.iterparse(sheet_file, events=['start-ns'])])     #zistenie vsetkych namespaces
                        name_spaces['def_ns'] = name_spaces['']         #default namespace este raz s nejakym pomenovanim, napr. def_ns, '' ako prefix pre default namespace sa pri find neda pouzit

                        sp = root.find('def_ns:sheetProtection', name_spaces)           #najdi tag sheetProtection
                        wp = root.find('def_ns:workbookProtection', name_spaces)        #najdi tag workbookProtection

                        if (sp is not None) or (wp is not None) :
                            if sp is not None: root.remove(sp)                             #vymaz tag sheetProtection
                            if wp is not None: root.remove(wp)                             #vymaz tag workbookProtection

                            #zistuje sa encoding potrebny pri zapise
                            with open(sheet_file, 'rb') as xmlfile:
                                lxml_tree = etree.parse(xmlfile)
                                enc = lxml_tree.docinfo.encoding                        

                            #registracia namespaces
                            for prefix, uri in name_spaces.items():
                                if prefix != 'def_ns': ET.register_namespace(prefix, uri)
                            
                            tree.write(sheet_file, encoding=enc, xml_declaration=True)
                            
                            #unregistracia namespaces, nestandardny sposob!
                            for prefix, uri in name_spaces.items():
                                ET._namespace_map.pop(uri, None)

                            zipwrite.write(sheet_file, item.filename)
                            fin_dict['Status'] = 'ok'
                            continue

                    data = zipread.read(item.filename)
                    zipwrite.writestr(item, data)

        if fin_dict['Status'] == 'ok': 
            fin_dict['Unprotected_File'] = shutil.move(temp_name, Path.joinpath(dest_dir, xlsx_fname.stem + post_fix + xlsx_fname.suffix))  
        else:
            fin_dict['Status'] = 'no_passw'
    
    except Exception as e:
        fin_dict['Status'] = str(e)
    
    finally:
        if generated_temp_dir is None: 
            fin_dict['Status'] = 'Problem creating temporary folder in ' + str(temp_dir)
        else:
            shutil.rmtree(generated_temp_dir)

    return fin_dict

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "Removes password protection from worksheets and workbook in xlsx file(s).")
    parser.add_argument('xlsx_file', nargs = '+', help = 'complete path to xlsx file(s) with protected worksheets or workbook')
    parser.add_argument('-t', '--temp_dir', help = 'path to the directory where temporary directory can be created, if None, directory of xlsx file will be used')
    parser.add_argument('-d', '--dest_dir', help = 'path to the directory where the unprotected file will be placed, if None, directory of xlsx file will be used')
    parser.add_argument('-p', '--post_fix', help = 'postfix for unprotected filename, if empty, the original xlsx file will be replaced with the unprotected one')
    parser.add_argument('-v', '--verbose', action = 'store_true', help = 'print argument summary before processing files')
    args = parser.parse_args()

    if args.verbose:
        print()
        for key, value in parser.parse_args()._get_kwargs():
            print(f'{key + ": ":<11}{value}')

    print()
    for xlsx_file in args.xlsx_file:
        result = unprotect_xlsx(xlsx_file, temp_dir = args.temp_dir, dest_dir = args.dest_dir, post_fix = args.post_fix)
        print(f'{"Status:":<18}{result["Status"]}\n{"Unprotected File:":<18}{result["Unprotected_File"]}\n')
