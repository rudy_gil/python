import sys
import logging
import logging.handlers

#sys.path.append('../starmedia_lib/')
import starmedia_lib.utils as stmlibutils
from adform.code.adform_lib import adform_main

user_settings = stmlibutils.custom_configuration()



#region
main_logger = logging.getLogger(__name__)
main_logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

logHandlerFile = logging.handlers.RotatingFileHandler(stmlibutils.LOG_FILE, maxBytes=1000000, backupCount=2, encoding='utf-8')
logHandlerFile.setLevel(logging.INFO)
logHandlerFile.setFormatter(formatter)

class WarningFilter(logging.Filter):
    def filter(self, rec):
        return rec.levelno == logging.WARNING

logHandlerEmailWarning = logging.handlers.SMTPHandler(mailhost = stmlibutils.MAILHOST, 
                                               fromaddr = stmlibutils.SEND_ALERT_FROM,
                                               toaddrs = stmlibutils.SEND_WARNING_ALERT_TO,
                                               subject = 'Warning in Digital Data Import!',
                                               credentials = None,
                                               secure = None)
logHandlerEmailWarning.setLevel(logging.WARNING)
logHandlerEmailWarning.setFormatter(formatter)
logHandlerEmailWarning.addFilter(WarningFilter())

logHandlerEmailCritical = logging.handlers.SMTPHandler(mailhost = stmlibutils.MAILHOST, 
                                                      fromaddr = stmlibutils.SEND_ALERT_FROM,
                                                      toaddrs = stmlibutils.SEND_ERROR_ALERT_TO,
                                                      subject = 'Error in Digital Data Import!',
                                                      credentials = None,
                                                      secure = None)
logHandlerEmailCritical.setLevel(logging.CRITICAL)
logHandlerEmailCritical.setFormatter(formatter)

handlers = (logHandlerFile, logHandlerEmailWarning, logHandlerEmailCritical)
for handler in handlers:
    main_logger.addHandler(handler)

main_logger.info("ID01: Spustenie main scriptu")
#endregion



try:
    user_campaigns = stmlibutils.custom_period_type()
except Exception:
    main_logger.critical('ID02: Problem s nacitanim uzivatelskych dat!', exc_info = True)
    raise

#adform_main(handlers, user_campaigns, user_settings)
try:
    adform_main(handlers, user_campaigns, user_settings)
except Exception:
    main_logger.critical('ID03: Problem pocas behu adform scriptu!', exc_info = True)
    main_logger.info('ID03: Nasilne ukoncenie adform scriptu!')

main_logger.info("ID04: Uspesne ukoncenie main scriptu")

