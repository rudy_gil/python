campaign_columns = ['id', 'name', 'billingCode', 'type', 'subType', 'budget', 'startDate', 'endDate', 'status', 'advertiserId', 'currency']
advertisers_columns = ['id', 'name', 'status', 'timeZone']

dimensions_list = ["client", "clientID", "campaign", "campaignID", "campaignType", 
    "order", "orderID", "lineItem", "lineItemID", "banner", "bannerID", "bannerFormat", "bannerType", "media", "mediaID", 'mediaLineItem', 'campaignType', 
    "campaignStartDate", "campaignEndDate",  "date", "week", "yearWeek", "month", "monthID", "yearMonth", "year", "yearID"]

metrics_dict = {
#video
    "videoEventsPlayTimePercent_25": {
        "metric": "videoEventsPlayTimePercent",
        "specs": {
            "videoEventType": "25Percent"
        }
    },
    "videoEventsPlayTimePercent_50": {
        "metric": "videoEventsPlayTimePercent",
        "specs": {
            "videoEventType": "50Percent"
        }
    },
    "videoEventsPlayTimePercent_75": {
        "metric": "videoEventsPlayTimePercent",
        "specs": {
            "videoEventType": "75Percent"
        }
    },
    "videoEventsPlayTimePercent_100": {
        "metric": "videoEventsPlayTimePercent",
        "specs": {
            "videoEventType": "100Percent"
        }
    },
    "videoPlayStartCount": "videoPlayStartCount",           #This metric shows the total number of video starts (including both auto-play and user-initiated video plays). TIP: Only unique starts are reported.
    "videoCompleteCount": "videoCompleteCount",             #"This metric shows the number of times a video banner or in-stream video was played to 100% completion."

#non-video
    "impressions_af_all": {
        "metric": "impressions",
        "specs": {
            "dataSource": "adform",
            "adUniqueness": "all"
        }
    },
    "impressions_af_campaignUnique": {
        "metric": "impressions",
        "specs": {
            "dataSource": "adform",
            "adUniqueness": "campaignUnique"
        }
    },
    "impressions_af_mediaUnique": {
        "metric": "impressions",
        "specs": {
            "dataSource": "adform",
            "adUniqueness": "mediaUnique"
        }
    },
    "impressions_af_lineItemUnique": {
        "metric": "impressions",
        "specs": {
            "dataSource": "adform",
            "adUniqueness": "lineItemUnique"
        }
    },
    "impressions_af_bannerUnique": {
        "metric": "impressions",
        "specs": {
            "dataSource": "adform",
            "adUniqueness": "bannerUnique"
        }
    },
    "viewImpressionsIAB_all": {
        "metric": "viewImpressionsIAB",
        "specs": {
            "adUniqueness": "all"
        }
    },
    "viewImpressionsPercentIAB_all": {
        "metric": "viewImpressionsPercentIAB",
        "specs": {
            "adUniqueness": "all"
        }
    },
    "clicks_af_all": {
        "metric": "clicks",
        "specs": {
            "dataSource": "adform",
            "adUniqueness": "all"
        }
    },
    "clicks_af_campaignUnique": {
        "metric": "clicks",
        "specs": {
            "dataSource": "adform",
            "adUniqueness": "campaignUnique"
        }
    },
    "clicks_af_mediaUnique": {
        "metric": "clicks",
        "specs": {
            "dataSource": "adform",
            "adUniqueness": "mediaUnique"
        }
    },
    "clicks_af_lineItemUnique": {
        "metric": "clicks",
        "specs": {
            "dataSource": "adform",
            "adUniqueness": "lineItemUnique"
        }
    },
    "clicks_af_bannerUnique": {
        "metric": "clicks",
        "specs": {
            "dataSource": "adform",
            "adUniqueness": "bannerUnique"
        }
    },    
    "ecpc_af_all": {
        "metric": "ecpc",
        "specs": {
            "dataSource": "adform",
            "adUniqueness": "all"
        }
    },
    "ctr_af_all": {
        "metric": "ctr",
        "specs": {
            "dataSource": "adform",
            "adUniqueness": "all"
        }
    },
    "ctrView_all": {
        "metric": "ctrView",
        "specs": {
            "adUniqueness": "all"
        }
    },
    "ecpm_af_all": {
        "metric": "ecpm",
        "specs": {
            "dataSource": "adform",
            "adUniqueness": "all"
        }
    },
    "avgViewabilityTime": "avgViewabilityTime",
    "cost_af_maxCost": {
        "metric": "cost",
        "specs": {
            "costDataSource": "adform",
            "costType": "maxCost"
        }
    },
    "conversions_allCT_allP_all": {
        "metric": "conversions",
        "specs": {
            "conversionType": "allConversionTypes",
            "pageCategory": "allPages",
            "adInteraction": "all"
        }
    },
    "conversions_allCT_allP_postI": {
        "metric": "conversions",
        "specs": {
            "conversionType": "allConversionTypes",
            "pageCategory": "allPages",
            "adInteraction": "postImpression"
        }
    },
    "conversions_allCT_allP_postC": {
        "metric": "conversions",
        "specs": {
            "conversionType": "allConversionTypes",
            "pageCategory": "allPages",
            "adInteraction": "postClick"
        }
    }
}


# metrics_videos = [
#     {
#         "metric": "videoEventsPlayTimePercent",
#         "specs": {
#             "videoEventType": "25Percent"
#         }
#     },
#     {
#         "metric": "videoEventsPlayTimePercent",
#         "specs": {
#             "videoEventType": "50Percent"
#         }
#     },
#     {
#         "metric": "videoEventsPlayTimePercent",
#         "specs": {
#             "videoEventType": "75Percent"
#         }
#     },
#     {
#         "metric": "videoEventsPlayTimePercent",
#         "specs": {
#             "videoEventType": "100Percent"
#         }
#     },
#     "videoStartRate",
#     "videoCompletionRate"
# ]

# metrics_non_videos = [
#     "impressions",
#     {
#         "metric": "impressions",
#         "specs": {
#             "adUniqueness": "campaignUnique"
#         }
#     },
#     {
#         "metric": "impressions",
#         "specs": {
#             "adUniqueness": "mediaUnique"
#         }
#     },
#     {
#         "metric": "impressions",
#         "specs": {
#             "adUniqueness": "lineItemUnique"
#         }
#     },
#     {
#         "metric": "impressions",
#         "specs": {
#             "adUniqueness": "bannerUnique"
#         }
#     },        
#     "viewImpressions",
#     "viewImpressionsPercent",
#     "clicks",
#     {
#         "metric": "clicks",
#         "specs": {
#             "adUniqueness": "campaignUnique"
#         }
#     },
#     "ecpc",
#     "ctr",
#     "ctrView",
#     "ecpm",
#     "avgViewabilityTime",
#     "cost",
#     "conversions",
#     {
#         "metric": "conversions",
#         "specs": {
#             "adInteraction": "postImpression"
#         }
#     },
#     {
#         "metric": "conversions",
#         "specs": {
#             "adInteraction": "postClick"
#         }
#     }
# ]



