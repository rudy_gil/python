import json
from pathlib import Path
# import sys
import time
from datetime import datetime, timedelta
from functools import reduce
import pytz

# from boltons.iterutils import chunked, one, bucketize, first
from boltons.iterutils import chunked
import pandas as pd
import requests
import logging
import logging.handlers
from ratelimit import limits, sleep_and_retry, RateLimitException
from backoff import on_exception, on_predicate, expo, constant
import sqlalchemy as db
import psycopg2.extras
import openpyxl

#from adform.code.metrics import metrics_non_videos, metrics_videos
from adform.code.adform_defs import *
import starmedia_lib.utils as stmlibutils


#development section
dev = True

af_logger = logging.getLogger(__name__)

class AdFormException(Exception):
    pass

class af_request():
    c_API = 'https://api.adform.com/'
    c_API_VERSION = 'v1/'
    c_REQUEST_LIMIT_PER_DAY = 500
    c_ASYNC_MAX_METRICS = 10
    c_ASYNC_MAX_DIMENSIONS = 8
    c_TOKEN_FILE = Path(r'W:\General\DATA SCIENCE\Support\PB\adform_token.txt')

    c_count_per_day = dict(POST_async = 0, GET_data = 0, GET_op = 0, GET_free = 0, token = 0, others = 0)
    c_session = None
    c_creds = None
    c_PG = None
    c_user_camp_custom_df = None
    c_user_camp_refresh_df = None
    c_user_settings = None

    c_api = {'POST_async': {'fnc': None, 'quota': 10, 'per_seconds': 60}, 
        'GET_data': {'fnc': None, 'quota': 10, 'per_seconds': 60},
        'GET_op': {'fnc': None, 'quota': 10, 'per_seconds': 1},
        'GET_free': {'fnc': None, 'quota': 0, 'per_seconds': 0}}

    @classmethod
    def c_init(cls, user_campaigns, user_settings, dev = False):
        #cls.c_creds = json.load(open(Path.home() / 'credentials.json'))['ADFORM']
        af_creds = json.load(open(Path.home() / 'credentials.json'))
        cls.c_creds = af_creds['ADFORM']
        cls.c_PG = af_creds['POSTGRES']
        cls.c_update_session(dev)
        cls.c_user_camp_custom_df = user_campaigns.get_platform_type('adform', 'custom')
        cls.c_user_camp_refresh_df = user_campaigns.get_platform_type('adform', 'refresh')     
        cls.c_user_settings = user_settings.settings['adform']

    @classmethod
    def c_update_session(cls, from_file = False):

        if from_file:
            try:
                with open(cls.c_TOKEN_FILE) as f:
                    token = f.read()
            except:
                from_file = False
        
        if not from_file:
            cls.c_count_per_day['token'] += 1
            r_token = requests.post(
                'https://id.adform.com/sts/connect/token',
                data={
                    "grant_type":cls.c_creds["grant_type"],
                    "client_id":cls.c_creds["client_id"],
                    "client_secret":cls.c_creds["client_secret"],
                    "scope":cls.c_creds["scope"]
                    },
                headers={
                    'Content-Type':'application/x-www-form-urlencoded'
                }
            )
            try:
                token = r_token.json()['access_token']
                af_logger.info('L03: Token obdrzany.')
            except Exception:
                #af_logger.critical('L04: Problem so ziskanim tokenu!', exc_info = True)
                af_logger.critical(f'\n\tE01a: Problem so ziskanim tokenu!\n'
                    f'\tResponse code: {r_token.status_code}\n'
                    f'\tResponse content: {r_token.json()}')
                raise

            if dev:
                try:
                    with open(cls.c_TOKEN_FILE, 'w') as f:
                        f.write(token)
                except:
                    pass

        cls.c_session = stmlibutils.SessionWithUrlBase(url_base=cls.c_API)
        cls.c_session.headers.update({'Authorization': 'Bearer %s' % token})


    #nasledujuce metody nepouzivaju @classmethod kvoli "classmethod object is not callable"
    @on_predicate(constant, lambda resp: resp.status_code == 429, max_tries = 5, interval = c_api['POST_async']['per_seconds'], jitter = None)
    @on_exception(constant, requests.exceptions.RequestException, max_time = 300, interval = 60)
    @sleep_and_retry
    @limits(calls = c_api['POST_async']['quota'], period = c_api['POST_async']['per_seconds'])
    def c_api_fnc_POST_async(arg_json, header = None):
        """
        arg_json - json, resp. Dict s cislami alebo stringami to send in the body of the Request \n
        header - json, resp. Dict s cislami alebo stringami pre header requestu  \n\n


        Async request POST osetreny podla stanovenych quota limits \n
        napr. https://api.adform.com/v1/buyer/stats/data \n

        limits pocita volania v casovom intervale \n
        sleep_and_retry uspi volanie pri prekroceni limitu \n
        on_exception riesi opakovanie v pripade RequestException (=ConnectionError, HTTPError, Timeout, TooManyRedirects) \n
        on_predicate vola funkciu znova ak som napriek kontrole prekrocil limit a opakuje volania, ak sa prekroci denny limit \n
            zopakuje sa volanie max 5x a potom skonci s navratovym kodom 429. \n
        """
        while True:
            af_request.c_count_per_day['POST_async'] += 1
            url_address = af_request.c_API_VERSION + 'buyer/stats/data'
            resp = af_request.c_session.post(url_address, json = arg_json, headers = header)
            af_logger.info(f'\n\tL05: POST asynchro {url_address}\n'
                f'\tJSON: {arg_json}\n'
                f'\tRequest number: {af_request.c_count_per_day}\n'
                f'\tResponse code: {resp.status_code}')

            #token expiroval, preto refresh
            if resp.status_code == 401:
                af_request.c_update_session(False)  
            else:
                break
        
        #429 = Quota limit exceeded
        return resp


    @on_predicate(constant, lambda resp: resp.status_code == 429, max_tries = 5, interval = c_api['GET_data']['per_seconds'], jitter = None)
    @on_exception(constant, requests.exceptions.RequestException, max_time = 300, interval = 60)
    @sleep_and_retry
    @limits(calls = c_api['GET_data']['quota'], period = c_api['GET_data']['per_seconds'])
    def c_api_fnc_GET_data(url_postfix, header = None):
        """
        url_postfix - alfa-num kod za 'c_API \n
        header - json, resp. Dict s cislami alebo stringami pre header requestu  \n

        Asynchro request GET data osetreny podla stanovenych quota limits \n
        napr. https://api.adform.com/v1/buyer/stats/data/020981f8-24ec-47f7-8e05-5c9d01c93438 \n

        limits pocita volania v casovom intervale \n
        sleep_and_retry uspi volanie pri prekroceni limitu \n
        on_exception riesi opakovanie v pripade RequestException (=ConnectionError, HTTPError, Timeout, TooManyRedirects) \n
        on_predicate vola funkciu znova ak som napriek kontrole prekrocil limit a opakuje volania \n
        """
        while True:
            af_request.c_count_per_day['GET_data'] += 1
            resp = af_request.c_session.get(url_postfix, headers = header)
            af_logger.info(f'\n\tL06: GET_data asynchro {url_postfix}\n'
                f'\tRequest number: {af_request.c_count_per_day}\n'
                f'\tResponse code: {resp.status_code}')

            #token expiroval, preto refresh
            if resp.status_code == 401:
                af_request.c_update_session(False)  
            else:
                break
        
        #429 = Quota limit exceeded
        return resp


    @on_predicate(constant, lambda resp: resp.status_code == 429, max_tries = 5, interval = c_api['GET_op']['per_seconds'], jitter = None)
    @on_exception(constant, requests.exceptions.RequestException, max_time = 300, interval = 60)
    @sleep_and_retry
    @limits(calls = c_api['GET_op']['quota'], period = c_api['GET_op']['per_seconds'])
    def c_api_fnc_GET_op(url_postfix, header = None):
        """
        url_postfix - alfa-num kod za 'c_API \n
        header - json, resp. Dict s cislami alebo stringami pre header requestu  \n

        Asynchro request GET operations osetreny podla stanovenych quota limits \n
        napr. https://api.adform.com/v1/buyer/stats/operations/020981f8-24ec-47f7-8e05-5c9d01c93438 \n

        limits pocita volania v casovom intervale \n
        sleep_and_retry uspi volanie pri prekroceni limitu \n
        on_exception riesi opakovanie v pripade RequestException (=ConnectionError, HTTPError, Timeout, TooManyRedirects) \n
        on_predicate vola funkciu znova ak som napriek kontrole prekrocil limit a opakuje volania \n
        """
        while True:
            af_request.c_count_per_day['GET_op'] += 1
            resp = af_request.c_session.get(url_postfix, headers = header)
            af_logger.info(f'\n\tL07: GET_operations asynchro {url_postfix}\n'
                f'\tRequest number: {af_request.c_count_per_day}\n'
                f'\tResponse code: {resp.status_code}')

            #token expiroval, preto refresh
            if resp.status_code == 401:
                af_request.c_update_session(False)  
            else:
                break
        
        #429 = Quota limit exceeded
        return resp



    @on_exception(constant, requests.exceptions.RequestException, max_time = 300, interval = 60)
    def c_api_fnc_GET_free(url_complete_postfix, arg_json = None, header = None):
        """
        url_complete_postfix - cast url za 'c_API/c_API_VERSION/' vratane parametrov \n
        arg_json - json, resp. Dict s cislami alebo stringami to send in the body of the Request \n
        header - json, resp. Dict s cislami alebo stringami pre header requestu  \n\n

        on_exception riesi opakovanie v pripade RequestException (=ConnectionError, HTTPError, Timeout, TooManyRedirects) \n
        Nepocita sa do celkoveho poctu requestov za den. \n
        """
        while True:
            af_request.c_count_per_day['GET_free'] += 1
            url_address = af_request.c_API_VERSION + url_complete_postfix
            resp = af_request.c_session.get(url_address, json = arg_json, headers = header)
            af_logger.info(f'\n\tL08: GET_free {url_address}\n'
                f'\tJSON: {arg_json}\n'
                f'\tRequest number: {af_request.c_count_per_day}\n'
                f'\tResponse code: {resp.status_code}')

            #token expiroval, preto refresh
            if resp.status_code == 401:
                af_request.c_update_session(False)  
            else:
                break
        
        return resp

    #asynchro
    c_api['POST_async']['fnc'] = c_api_fnc_POST_async           # POST c_API/c_API_VERSION/buyer/stats/data
    c_api['GET_data']['fnc'] = c_api_fnc_GET_data               #  GET c_API
    c_api['GET_op']['fnc'] = c_api_fnc_GET_op                   #  GET c_API
    
    #synchro
    c_api['GET_free']['fnc'] = c_api_fnc_GET_free               #  GET c_API/c_API_VERSION/


    #object functions
    def __init__(self, fnc_key, url_postfix = None, url_parameters = None, json = None, header = None):
        """
        fnc_key - kluc funkcie z dict af_request.c_api, bud 'POST_async' alebo 'GET_free'  
        url_postfix - posledna cast url adresy pred parametrami  
        url_parameters - dict s objektami, ktore su preformatovane do stringov  
        json - dict data pre fnc  
        header - dict data pre header fnc  
        """
        self.fnc_key = fnc_key
        self.fnc = af_request.c_api[fnc_key]['fnc']
        self.url_postfix = '' if url_postfix is None else url_postfix
        self.url_parameters = {} if url_parameters is None else url_parameters
        self.json = {} if json is None else json            #dict s paging, filter ako dicts a dimensions a metrics ako list of keywords to dimensions_dict and metrics_dict
        self.metrics_headers = []                           #nizsie v kode sa po kontrole priradi zoznam metrics keywords
        self.header = {} if header is None else header
        self.is_asynchro = fnc_key == 'POST_async'          #plati len pre POST_async, lebo GET_data a GET_op sa zavolaju interne ako follow-up zadaneho async POST_async-u
        self.response = None                                #finalna odpoved
        self.response_asynchro = None                       #priebezne odpovede - na prvy asynchro request
        self.response_operation = None                      #priebezne odpovede - na get operation requestc
        try:                                                #argument limit je definovany pri synchro v url_parameters, pri asynchro v paging.limit
            self.limit = json['paging']['limit'] if self.is_asynchro else self.url_parameters['limit']
        except KeyError:
            self.limit = None
        """
        Ak je self.limit != 0, tak sa pouzije tato nenulova hodnota.  
        Ak je hodnota self.limit == 0, tak sa ma pracovat s max poctom zaznamov:  
        - pre asynchro sa vezme hodnota z polozky user_settings['adform']['async-max-records']  
        - pre synchro sa vezme hodnota z polozky user_settings['adform']['sync-max-records']  
         
        Ak je self.limit None, tak:  
        - pre synchro:  
            - ak je user_settings['adform']['sync-use-max-records'] == false, tak sa pracuje s default hodnotami, t.j. vynecha sa limit v requeste  
            - ak je user_settings['adform']['sync-use-max-records'] == true, tak sa pracuje s max hodnotami, t.j. s user_settings['adform']['sync-max-records']  
        - pre asynchro:  
            - ak je user_settings['adform']['async-use-max-records'] == false, tak sa pracuje s default hodnotami, t.j. vynecha sa limit v requeste  
            - ak je user_settings['adform']['async-use-max-records'] == true, tak sa pracuje s max hodnotami, t.j. s user_settings['adform']['async-max-records']  

        """
        #definicia self.limit a prislusnych argumentov pre volania requestov podla nastaveni v setting.json. ok-otestovane
        if self.limit is None:
            if self.is_asynchro:
                use_max_records, max_records = af_request.c_user_settings['async-use-max-records'], af_request.c_user_settings['async-max-records']
                if use_max_records:
                    self.limit = max_records
                    self.json.update({'paging': self.json.get('paging', {})})       #doplni 'paging' s povodnou strukturou ak neexistuje
                    self.json['paging']['limit'] = max_records                      #doplni 'limit' do 'paging'
            else:
                use_max_records, max_records = af_request.c_user_settings['sync-use-max-records'], af_request.c_user_settings['sync-max-records']
                if use_max_records: 
                    self.limit = max_records
                    self.url_parameters['limit'] = max_records
        else:
            if self.limit == 0:
                if self.is_asynchro:
                    self.limit = af_request.c_user_settings['async-max-records'] 
                    self.json.update({'paging': self.json.get('paging', {})})       #doplni 'paging' ak neexistuje
                    self.json['paging']['limit'] = self.limit                      #doplni 'limit' do 'paging'
                else: 
                    self.limit = af_request.c_user_settings['sync-max-records']
                    self.url_parameters['limit'] = self.limit


        #odpovede budu vzdy obsahovat info o celkovom pocte zaznamov
        if self.is_asynchro: 
            self.json['includeRowCount'] = 'true'
        else:
            self.header['Return-Total-Count'] = 'true'

        #asynchro:
        #In every request, date period filter and at least one metric must be specified.
        #A maximum number of 8 dimensions and 10 metrics can be queried in one request.
        #kontrola, ze json struktura obsahuje: 
        #dimension list s max dlzkou 8
        #metrics list s dlzkou aspon 1 (dlzku vacsiu ako 10 osetrim nakrajanim na mensie casti)
        #filters s datumovou polozkou
        if self.is_asynchro:
            if len(self.json.get('dimensions',[])) > af_request.c_ASYNC_MAX_DIMENSIONS:
                raise AdFormException(f'E07a: V requeste {self.fnc_key} je prekrocena max. hodnota {af_request.c_ASYNC_MAX_DIMENSIONS} dimenzii: \n'
                    f'\tJSON: {self.json} \n'
                    )

            # tento test nie je potrebny, dimenzie nie su povinne
            # if len(self.json.get('dimensions',[])) == 0:
            #     raise AdFormException(f'E07b: V requeste {self.fnc_key} nie su definovane dimenzie: \n'
            #         f'\tJSON: {self.json} \n'
            #         )

            #kontrola spravnych dimensions
            for dim in self.json.get('dimensions',[]):
                if dim not in dimensions_list:
                    raise AdFormException(f'E07c: V requeste {self.fnc_key} je neznama dimenzia {dim}: \n'
                        f'\tJSON: {self.json} \n'
                        )

            if len(self.json.get('metrics',[])) == 0:
                raise AdFormException(f'E08a: V requeste {self.fnc_key} nie su definovane metriky: \n'
                    f'\tJSON: {self.json} \n'
                    )
            
            #kontrola spravnych metrik a vytvorenie ich specifikacii do requestu
            self.metrics_headers = self.json['metrics']
            metrics_spec = []
            for metric_key in self.json['metrics']:
                try:
                    metrics_spec.append(metrics_dict[metric_key])
                except KeyError:
                    raise AdFormException(f'E08b: V requeste {self.fnc_key} je neznama metrika {metric_key}: \n'
                        f'\tJSON: {self.json} \n'
                        )
            self.json['metrics'] = metrics_spec

            try:
                self.json['filter']['date']
            except KeyError:
                raise AdFormException(f'E09: V requeste {self.fnc_key} nie je v definovany atribut json["filter"]["date"]: \n'
                    f'\tJSON: {self.json} \n'
                    )




    @on_predicate(constant, lambda x: x['repeat_call'], max_tries = 5, interval = 30, jitter = None)
    def call_api(self):
        """
        Zavola prislusnu funkciu z af_request.c_api dictu.  

        Ak je volanie synchronne, vrati hned response.  
        Ak je volanie asynchronne, volanim cez GET_operations caka na data, cez GET_data ich ziska a vrati response.   
        Vsetky odpovede su ulozene v atributoch self.response[_xyz], finalne data su v self.response.  
        Decorator on_predicate riesi situaciu, ak treba znovu zopakovat volanie kvoli vynimke na strane servera.  
        Volanie sa zopakuje aj vtedy, ak pocas 10 opakovani v 2sec intervaloch sa nedosiahne stav asynchro operacie 'succeeded' alebo 'failed'.  

        Metoda Vracia dict, v ktorom  
        'response' je odpoved zo servera, moze byt aj None!  
        'repeat_call' je informacia pre decorator, ci ma opakovat volanie funkcie  
        V 'response' su len odpovede so status code 200 alebo 202. Pri inom status code vyhlasi vynimku.  
        Pri asynchro requeste sa este moze stat, ze 'response' je None, ak sa nepodarilo odobrat pripravene data zo servera.  
        """
        repeat_call = False
        self.response = None                                #finalna odpoved
        self.response_asynchro = None                       #priebezne odpovede - na prvy asynchro request
        self.response_operation = None                      #priebezne odpovede - na get operation requestc        

        if self.is_asynchro:
            #zavola sa asynchronny request, t.j. POST_async, a caka sa na odpoved
            self.response_asynchro = self.fnc(self.json, self.header)
            if self.response_asynchro.status_code != 202:
                #bud je prekroceny limit alebo ina chyba servera, koniec
                raise AdFormException(f'E01b: Chybovy status_code na {self.fnc_key}. Bud je chybne zostaveny prikaz, prekrocena Quota alebo ina chyba servera. \n'
                    f'\tJSON: {self.json} \n'
                    f'\tResponse: StatusCode: {self.response_asynchro.status_code}, Reason: {self.response_asynchro.reason} \n'
                    f'\tRequest number: {af_request.c_count_per_day} \n'
                    )

            #odpoved je ok, t.j. 202, treba cakat na data
            #op_location = self._value_from_inline_response(self.response_asynchro, 'Operation-Location')
            op_location = self.response_asynchro.headers['Operation-Location']
            op_counter = 0
            while True:
                time.sleep(af_request.c_user_settings['async-sleep-time'] + op_counter*3)          #napr. 5sec
                op_counter += 1
                self.response_operation = af_request.c_api_fnc_GET_op(op_location, self.header)
                if self.response_operation.status_code != 200:
                    raise AdFormException(f'E02: Chybovy status_code na GET_operation request pre {self.fnc_key} s JSON argumentom: \n'
                        f'\tJSON: {self.json} \n'
                        f'\tGET_operation response: StatusCode: {self.response_operation.status_code}, Reason: {self.response_operation.reason} \n'
                        f'\tRequest number: {af_request.c_count_per_day} \n'
                        )                        
                
                #odpoved je ok, t.j. 200, ak je operation status == failed treba opakovat cele volanie vratane POST requestu
                operation_body = self.response_operation.json()                 #obsahuje "id", "status", "createdAt", "lastActionAt", "location"

                if (operation_body['status'] == 'failed') or (op_counter > af_request.c_user_settings['async-rounds']):
                    #je potrebne opakovat volanie POST - bud kvoli zlyhaniu na strane servera alebo stav je aj po 10 opakovaniach bud "running" alebo "not-started"
                    repeat_call = True
                    self.response = None
                    break
                    #navratova hodnota moze byt aj {'response': None, 'repeat_call': True} po ukonceni opakovania volania tejto funkcie cez decorator

                if operation_body['status'] == 'succeeded':
                    self.response = af_request.c_api_fnc_GET_data(operation_body['location'], self.header)
                    if self.response.status_code != 200:
                        raise AdFormException(f'E03: Chybovy status_code na GET_data request pre {self.fnc_key} s JSON argumentom: \n'
                            f'\tJSON: {self.json} \n'
                            f'\tGET_data response: StatusCode: {self.response.status_code}, Reason: {self.response.reason} \n'
                            f'\tRequest number: {af_request.c_count_per_day} \n'
                            )                        
                    break

                af_logger.info(f'\n\tL07: Repeating GET_operations asynchro, round: {op_counter+1}\n'
                    f'\tOperation status: {operation_body["status"]}')


        elif self.fnc_key == 'GET_free':
            url_complete_postfix = af_request.c_session.get_postfix_url_with_params(self.url_postfix, self.url_parameters)
            self.response = self.fnc(url_complete_postfix, self.json, self.header)
            if self.response.status_code != 200:
                #bud je prekroceny limit alebo ina chyba servera, koniec
                raise AdFormException(f'E04: Chybovy status_code na {self.fnc_key}. Bud je chybne zostaveny prikaz, prekrocena Quota alebo ina chyba servera. \n'
                    f'\t{self.fnc_key} {url_complete_postfix} \n'
                    f'\tJSON: {self.json} \n'
                    f'\tResponse: StatusCode: {self.response.status_code}, Reason: {self.response.reason} \n'
                    f'\tRequest number: {af_request.c_count_per_day} \n'
                    )     
        else:
            #sem sa nedostane, je to tu len pre istotu
            raise AdFormException('E05: Chybne zadane fnc_key.')

        return {'response': self.response, 'repeat_call': repeat_call}

    def call_paging(self):
        """
        Zabezpeci strankovanie requestov, self. limit je uz nastaveny v konstruktore.  
        1. Zavola prvy request s aktualne nastavenym limitom, ak existuje  
        2. z odpovede zisti #riadkov a celkovy # riadkov  
        3. vypocitaj (offset, limit)  
        4. pri asynchro pridaj paging ak treba  
        5. volaj request_i  
        6. pokracuj 2. 

        Vrati list s responses zo servera. 
        """

        def calculatePageSize(offset, limit, total = 0):
            result = limit

            if total and limit:
                if (offset + limit) > total:
                    result = total - offset
                elif offset == total:
                    result = 0

            return result

        #list s odpovedami
        results, offset = [], 0

        while True:
            call_api_dict = self.call_api()     #odpoved obsahuje ['response'] a ['repeat_call']
            api_response = call_api_dict['response']
            
            if api_response is None:
                raise AdFormException(f'E06: Nepodarilo sa ziskat data requestu {self.fnc_key} (vratena hodnota je None, najpravdepodobnejsie zlyhalo odobratie dat cez GET_data, asi docasna chyba servera): \n'
                    f'\turl_postfix: {self.url_postfix} \n'
                    f'\turl_parameters: {self.url_parameters} \n'
                    f'\tJSON: {self.json} \n'
                    f'\theader: {self.header} \n'
                    f'\tRequest number: {af_request.c_count_per_day} \n'
                    )                   
            
            results.append(api_response)
            api_response_data = api_response.json()

            #zistenie aktualneho poctu riadkov a celkoveho poctu riadkov z odpovede 
            if self.is_asynchro:
                #region
                """
                struktura api_response_data:
                {
                    'reportData': {
                        'columnHeaders': ['campaign', 'impressions'],
                        'columns': [{
                                        'key': 'campaign'
                                    },
                                    {
                                        'key': 'impressions',
                                        'specs': {'dataSource': 'adform', 'adUniqueness': 'all'}
                                    }],
                        'rows': [
                                    ['AlfaRomeo_AR Giulia Stelvio Btech_JJJJ-11-TT_JJJJ-12-TT_Giulia_SK_11_12_2018', 6973321],
                                    ['FIAT_Range_Black friday_11_2017', 0]
                                ]
                    },
                    'totalRowCount': 38
                }                
                """
                #endregion
                curr_num_rows =  len(api_response_data['reportData']['rows'])
                total_num_rows = api_response_data['totalRowCount']
            else:
                #region
                """
                struktura api_response_data:
                [{data1},{data2}]
                v api_response.headers['Total-Count'] je celkovy pocet riadkov
                """
                #endregion
                curr_num_rows =  len(api_response_data)
                total_num_rows = int(api_response.headers['Total-Count'])

            #vypocet offset a limit, pre vypocet nasledujuceho limitu povazujem curr_num_rows ako limit
            offset += curr_num_rows
            limit = calculatePageSize(offset, curr_num_rows, total_num_rows)
            if limit <= 0: break

            if self.is_asynchro:
                #ak ziadam dalsiu stranku tak pridaj paging ak chyba a nastav v nom offset a limit
                self.json.update({'paging': self.json.get('paging', {})})       #doplni 'paging' s povodnou strukturou ak neexistuje
                self.json['paging']['limit'] = limit
                self.json['paging']['offset'] = offset
            else:
                #nastav offset a limit v url_parameters
                self.url_parameters.update({'offset': offset, 'limit': limit})
        
        return results

    def get_response(self, join_df = True):
        """
        Odosle request cez call_paging a vystup spoji do df ak join_df = True.
        Pri asynchro navyse pre odoslanim requestu rozdeli metriky ak treba. V takom pripade vsak pre opatovne spojenie metrik musi byt join_df = True.
        Pri asynchro: ak je metrik > c_ASYNC_MAX_METRICS a join_df = False, vystupom je list = matica, stlpce = zodpovedaju dimensions + chunky metrik, riadky = responses (kazdy response zodpoveda jednej stranke)
        Pri asynchro: ak je metrik <= c_ASYNC_MAX_METRICS a join_df = False, vystupom je list s responses (kazdy response zodpoveda jednej stranke)
        Pri asynchro: ak join_df = True, vystupom je df so spojenymi metrikami a strankami, s nazvami stlpcov podla klucov v metrics_dict a dimensions_list
        
        Pri synchro: ak join_df = False, vystupom je list s responses (kazdy response zodpoveda jednej stranke)
        Pri synchro: ak join_df = True, vystupom je df so spojenymi strankami
        """

        paging_results = []
        if self.is_asynchro:
            #pri asynchro mam istotou ze existuje json['metrics'] a json['filter']['date']. json['dimensions'] nemusi existovat
            #nakrajanie metrik ak treba
            for metrics_chunk in chunked(self.json['metrics'], af_request.c_ASYNC_MAX_METRICS):
                self.json['metrics'] = metrics_chunk
                result = self.call_paging()             #vrati list stranok pre dany chunk
                paging_results.append(result)       

                #znovu nastavit limit a offset
                try:
                    self.json['paging']['limit'] = self.limit
                    self.json['paging']['offset'] = 0
                except KeyError:
                    pass

            #opatovne spojenie metrik A spojenie do df
            if join_df:
                metrics_slices = chunked(self.metrics_headers, af_request.c_ASYNC_MAX_METRICS)
                df_chunks = []
                for i, chunk in enumerate(paging_results):
                    #spojenie stranok v spolocnom chunku do df_chunks
                    #v chunk su rovnake stlpce (dims+metrics) vo viacerych strankach
                    df_list = [pd.DataFrame(page.json()['reportData']['rows'], columns = self.json['dimensions'] + metrics_slices[i]) for page in chunk]
                    df_chunks.append(pd.concat(df_list, ignore_index = True, sort = True))
            
                return reduce(lambda x, y: pd.merge(x, y, on =self.json['dimensions']), df_chunks)
            else:
                return paging_results
        
        else:
            result = self.call_paging()             #vrati list stranok pre dany chunk
            return pd.concat([pd.DataFrame(page.json()) for page in result], ignore_index = True, sort = True) if join_df else result
            



def adform_main(handlers, user_campaigns, user_settings):
    
    # vytvorenie logovania
    af_logger.setLevel(logging.INFO)
    for handler in handlers:
        af_logger.addHandler(handler)
    af_logger.info('L01: Spustenie adform scriptu')

    af_request.c_init(user_campaigns, user_settings, dev)
    session_ts = datetime.now(pytz.timezone('Europe/Bratislava'))
    pt = timedelta(days=af_request.c_user_settings['period-tracked'])

    
    
    
    # KROK 1 - spracovanie uzivatelskych dat
    #region
    
    # zoznam korektne zadanych kampani na spracovanie
    df_UC = user_campaigns.get_platform_type('adform').filter(['Campaign', 'Start', 'End', 'Type', 'Status', 'Log'])
    df_UC = df_UC[df_UC['Status'].isnull()]
    
    # chybne zadany datum = script nebude pracovat s user datami
    try:
        df_UC = df_UC.astype({'Start': 'datetime64', 'End': 'datetime64'}, errors = 'raise')
    except ValueError:
        af_logger.critical('\n\tE10: V xlsx subore s uzivatelskymi datami je nespravne zadany datum!')
        raise

    grouped = df_UC.groupby('Campaign')

    fnc1 = lambda x: psycopg2.extras.DateRange(x[0],x[1], bounds='[]') if x[0] is not pd.NaT else None
    # dict: key=nazov kampane, value je dict s keys:
    #    periods: ak su uvedene datumy v xlsx tak zoznam uzivatelskych period (od-do) typu DateRange plus dodatocna perioda od posledneho datumu + 1 den po dnesny datum - 1 den, inak []
    #    type: napr. refresh, inak None
    # metriky z obdobia po endDate kampane budu z obdobia [endDate, _time_stamp]
    dict_UC = {name: {
        'Periods': list(map(fnc1, zip(group['Start'].dt.date, group['End'].dt.date))) if group.iloc[0]['Start'] is not pd.NaT else None,
        'Type': group.iloc[0]['Type'] if group.iloc[0]['Type'] is not pd.np.nan else None} for name, group in grouped}
    df_UC_fromdict = pd.DataFrame.from_dict(dict_UC, orient='index').rename_axis('Campaign').reset_index()
    #endregion
    
    
    # KROK 2 - spracovanie adform dat a databazovych dat
    #region

    req_1 = af_request('GET_free', url_postfix = 'buyer/campaigns', url_parameters = {'status':','.join(af_request.c_user_settings['campaign-status'])})
    df_AF = req_1.get_response()
    df_AF['startDate'] = pd.to_datetime(df_AF['startDate'], utc=True)
    df_AF['endDate'] = pd.to_datetime(df_AF['endDate'], utc=True)

    # aktualny zoznam zadavatelov z AF
    req_2 = af_request('GET_free', url_postfix = 'buyer/advertisers')
    df_AF_adv = req_2.get_response()

    # kontrola vyskytu stlpcov v db vo vystupoch z AF
    if not set(df_AF.columns).issuperset(campaign_columns): raise AdFormException('\n\tE11a: Chybaju stlpce v zozname kampani z Adform!')
    if not set(df_AF_adv.columns).issuperset(advertisers_columns): raise AdFormException('\n\tE11b: Chybaju stlpce v zozname zadavatelov z Adform!')

    # z df ostanu len potrebne stlpce
    df_AF = df_AF.filter(campaign_columns)
    df_AF_adv = df_AF_adv.filter(advertisers_columns)
    df_AF_adv['_time_stamp'] = session_ts

    # pripojenie do db
    db_string = f"postgres://{af_request.c_PG['user']}:{af_request.c_PG['passwd']}@{af_request.c_PG['host']}:{af_request.c_PG['port']}/{af_request.c_PG['db']}"
    engine = db.create_engine(db_string)
    connection = engine.connect()

    metadata = db.MetaData()
    af_campaigns_tab = db.Table('af_campaigns', metadata, autoload=True, autoload_with=engine)
    af_advertisers_tab = db.Table('af_advertisers', metadata, autoload=True, autoload_with=engine)
    af_metrics_tab = db.Table('af_metrics', metadata, autoload=True, autoload_with=engine)    

    # zadavatelia z db
    query = db.select([af_advertisers_tab])
    result_set = connection.execute(query).fetchall()
    df_DB_adv = pd.DataFrame(result_set)
    try:
        df_DB_adv.columns = [c.name for c in af_advertisers_tab.columns]
    except ValueError:
        df_DB_adv = pd.DataFrame(columns=[c.name for c in af_advertisers_tab.columns])

    # zistenie novych zadavatelov v AF a ich vlozenie do databazy s aktualnym ts
    df_new_adv = df_AF_adv[df_AF_adv.id.isin(df_DB_adv.id) == False]
    df_new_adv.to_sql('af_advertisers', con=engine, if_exists='append', index=False)

    
    # kampane z db
    query = db.select([af_campaigns_tab])
    result_set = connection.execute(query).fetchall()
    df_DB = pd.DataFrame(result_set)
    try:
        df_DB.columns = [c.name for c in af_campaigns_tab.columns]
    except ValueError:
        df_DB = pd.DataFrame(columns = [c.name for c in af_campaigns_tab.columns])

    # ponecha sa posledny timestamp z kazdej kampane
    df_DB = df_DB.sort_values(by = ['id', '_time_stamp'], ascending = False).drop_duplicates(['id'])
    
    # kampane s NaN v _agency_phase dostanu 'open', nemali by tam byt vsak ziadne
    df_DB['_agency_phase'].fillna(value='open', inplace=True)

    # pripojenie db k vystupu z AF
    df_AF_DB = df_AF.merge(df_DB[['id', '_agency_phase', '_agency_periods', '_time_stamp']], how='outer', on='id', indicator=True)
    df_AF_DB.rename(columns={'_merge': '_merge AF_DB'}, inplace=True)
    df_AF_DB['_state'] = None
    
    # novym kampaniam s merge=='left_only' sa priradi 'open', v dalsom kroku sa vsetkym 'open' kampaniam priradi 'update' do _state
    df_AF_DB.loc[df_AF_DB['_merge AF_DB'] == 'left_only', '_agency_phase'] = 'open'

    # error kampane po ukonceni tracking period 
    # Dovod porovnania voci timestamp: napr. majme datumy: timestamp=14, trackingperiod=5 dni, endDate=10, now=17. Koniec trackovacieho obdobia je 15-teho, takze aj ked uz je 17-teho 
    # tak este mohol zbehnut zber s timestamp=15  pri ktorom uz error nemusel nastat. Preto zbehnem zber teraz a nevadi ze uz je 17-teho
    # pre right_only je s_error = False, kedze endDate = NaN
    s_error = (df_AF_DB['_time_stamp'] > (df_AF_DB['endDate'] + pt)) & (df_AF_DB['_agency_phase'] == 'error')

    # closed kampane
    s_closed = df_AF_DB['_agency_phase'] == 'closed'

    # kampane uz nedostupne v AF
    s_NaN = df_AF_DB['_merge AF_DB'] == 'right_only'

    # vsetky treba vylucit zo spracovania preto negacia disjunkcie troch series
    df_AF_DB.loc[~(s_closed | s_error | s_NaN), '_state'] = 'update'

    # pripojenie vsetkych uzivatelskych kampani
    df_AF_DB_user = df_AF_DB.merge(df_UC_fromdict, how='outer', left_on='name', right_on='Campaign', indicator=True)
    df_AF_DB_user.rename(columns={'_merge': '_user_merge_AF_DB', 'Campaign': '_user_Campaign', 'Type': '_user_Type', 'Periods': '_user_Periods'}, inplace=True)

    # aktualizacia period podla uzivatela
    df_AF_DB_user.loc[df_AF_DB_user['_user_Periods'].notna(), '_agency_periods'] = df_AF_DB_user['_user_Periods']

    # kampane z uzivatelskeho suboru, ku ktorym nenasiel dvojicku su chybne
    df_AF_DB_user.loc[df_AF_DB_user['_user_merge_AF_DB'] == 'right_only', '_state'] = 'error in xlsx'

    # nastavenie 'update' statusu pre vyziadany refresh kampane
    df_AF_DB_user.loc[(df_AF_DB_user['_user_merge_AF_DB'] == 'both') & (df_AF_DB_user['_user_Type'] == 'refresh'), '_state'] = 'update'

    # fcia pre spatny zapis informacii do uzivatelskeho suboru
    def WriteLogToExcel(df_Camp_Type_Log):
        # vstupuje df so zaznamami, ktore sa zapisu do xlsx, nepotrebne zaznamy su uz vymazane, obsahuje tri stlpce Campaign, Type a Log
        workbook = openpyxl.load_workbook(filename=stmlibutils.SUPPORT_FILE)
        sheet = workbook['Periods']
        campaign_col, type_col, log_col = sheet['B'], sheet['E'], sheet['G']
        
        for index, row in df_Camp_Type_Log.iterrows():
            for matched_cell in list(filter(lambda cell: cell.value == row['Campaign'], campaign_col)):
                log_col[matched_cell.row - 1].value = row['Log']
                if row['Type']: type_col[matched_cell.row - 1].value = row['Type']
        
        workbook.save(filename=stmlibutils.SUPPORT_FILE)

    df_error_xlsx = df_AF_DB_user[df_AF_DB_user['_state'] == 'error in xlsx']
    df_error_xlsx.insert(0, 'Type', 'failed')
    df_error_xlsx.insert(0, 'Log', 'Neexistujuca kampan v Adforme, skontroluj nazov kampane!')
    df_error_xlsx = df_error_xlsx[['_user_Campaign', 'Type', 'Log']]
    df_error_xlsx.rename(columns={'_user_Campaign': 'Campaign'}, inplace=True)

    camp_list = df_AF_DB_user.loc[df_AF_DB_user['_state'] == 'update', ['id', 'startDate', 'endDate', '_agency_periods']]

    # sem treba dat nie nazov kampane ale jeho id lebo napr. nazvy 2 kampani sa zopakovali
    req_json = {
        #"dimensions": ["campaign"],
        "dimensions": ["campaignID", "order", "mediaLineItem", "banner", "date"],
        #"metrics": metrics_non_videos + metrics_videos,
        #"metrics": ["impressions_af_all", "impressions_af_campaignUnique", "impressions_af_mediaUnique", "impressions_af_lineItemUnique", "impressions_af_bannerUnique", "viewImpressionsIAB_all", "viewImpressionsPercentIAB_all", 
        #            "clicks_af_campaignUnique", "ecpc_af_all", "ctr_af_all", "ctrView_all", "ecpm_af_all", "avgViewabilityTime", "cost_af_maxCost", "conversions_allCT_allP_all", "conversions_allCT_allP_postI", "conversions_allCT_allP_postC",
        #            "videoEventsPlayTimePercent_25", "videoEventsPlayTimePercent_50", "videoEventsPlayTimePercent_75", "videoEventsPlayTimePercent_100", "videoStartRate", "videoCompletionRate"],
        "metrics": list(metrics_dict.keys()),
        "paging": {"limit": 0},
        #"filter": {"date": "campaignStartToToday", "client": {"id": [139471]}}
        "filter": {"date": "campaignStartToToday", "campaign": list(camp_list['id'])}
        #"filter": {"date": "campaignStartToToday"}
    }
    req_metrics = af_request('POST_async', json = req_json)
    df_AF_metrics_update = req_metrics.get_response()
    if set(df_AF_metrics_update.columns) != set(list(metrics_dict.keys())).union(set(req_json['dimensions'])): raise AdFormException('\n\tE12: Chybaju stlpce v zozname metrik z Adform!')
    df_AF_metrics_update['date'] = pd.to_datetime(df_AF_metrics_update['date'], utc=True)

    df_AF_metrics_update_camplist = df_AF_metrics_update.merge(camp_list, how='outer', left_on='campaignID', right_on='id', indicator=True)
    df_AF_metrics_update_camplist.rename(columns={'id': '_camp_list_id'}, inplace=True)
    # v df metrik su len tie kampane, ktore este AF poskytuje (_merge=both) a b) 
    # trackingove kampane sa tu nevyskytuju lebo sa stahuju metriky iba pre vybrane kampane (trackingove kampane nevyskytuju sa ani v zozname kampani)
    # metriky pre trackingove kampane by si ziskal vtedy, aky by si stiahol metriky s filtrom iba pre campaignStartToToday

    # update agency_phase v zozname kampani, pri state='update' kampaniach sa nastavi '_agency_phase' bud 'open' alebo 'error'
    df_AF_DB_user.loc[df_AF_DB_user['_state'] == 'update', '_agency_phase'] = 'open'
    camp_na_list = df_AF_metrics_update_camplist[df_AF_metrics_update_camplist['_merge'] == 'right_only']['_camp_list_id']
    df_AF_DB_user.loc[df_AF_DB_user['id'].isin(camp_na_list), '_agency_phase'] = 'error'

    # v df kampani su vsetky kampane
    df_AF_to_DB = df_AF_DB_user[df_AF_DB_user['_state'] == 'update'].filter(campaign_columns + ['_agency_phase', '_agency_periods', '_time_stamp'])
    df_AF_to_DB['_time_stamp'] = session_ts

    # vezmu sa len metriky pre dostupne kampane
    df_AF_metrics_to_DB = df_AF_metrics_update_camplist[df_AF_metrics_update_camplist['_merge'] == 'both'].copy()
    df_AF_metrics_to_DB['_time_stamp'] = session_ts
    
    weekday = df_AF_metrics_to_DB['date'].apply(datetime.weekday)
    weekstart = df_AF_metrics_to_DB['date'] - weekday.map(timedelta)
    weekend = weekstart + timedelta(days = 6)
    df_AF_metrics_to_DB['week_date'] = weekstart.apply(datetime.strftime, args=('%d.%m.-',)) + weekend.apply(datetime.strftime, args=('%d.%m.%Y',))
    df_AF_metrics_to_DB['week_num_year'] = df_AF_metrics_to_DB['date'].apply(lambda d: f"{d.isocalendar()[1]:02}-{d.isocalendar()[0]}") 
    df_AF_metrics_to_DB['month'] = df_AF_metrics_to_DB['date'].dt.month
    df_AF_metrics_to_DB['year'] = df_AF_metrics_to_DB['date'].dt.year

    def is_in_daterange_list(arg):
        #if not math.isnan(arg[0]):
        if isinstance(arg[0], list):
            for dr in arg[0]:
                if arg[1] in dr: return dr    

    df_AF_metrics_to_DB['custom_period'] = list(map(is_in_daterange_list, zip(df_AF_metrics_to_DB['_agency_periods'], df_AF_metrics_to_DB['date'])))
    df_AF_metrics_to_DB['campaign_period'] = (df_AF_metrics_to_DB['startDate'] <= df_AF_metrics_to_DB['date']) & (df_AF_metrics_to_DB['date'] <= df_AF_metrics_to_DB['endDate'])
    df_AF_metrics_to_DB['media'] = df_AF_metrics_to_DB['mediaLineItem'].apply(lambda s: s.split(' :: ')[0])
    df_AF_metrics_to_DB['lineitem'] = df_AF_metrics_to_DB['mediaLineItem'].apply(lambda s: s.split(' :: ')[1])
    df_AF_metrics_to_DB = df_AF_metrics_to_DB.drop(['_camp_list_id', '_merge', 'startDate', 'endDate', '_agency_periods'], axis=1)
    #endregion



    # KROK 3 - zapis do db a cistenie db
    #region

    # v jednej transakcii sa zapisu udaje do db do tabuliek af_campaigns a af_metrics
    with engine.begin() as t_conn:
        df_AF_to_DB.to_sql('af_campaigns', con=t_conn, if_exists='append', index=False, method='multi')
        df_AF_metrics_to_DB.to_sql('af_metrics', con=t_conn, if_exists='append', index=False, method='multi')

    # nasleduje cistenie databazy
    max_ts = connection.execute(db.select([db.func.max(af_campaigns_tab.c._time_stamp)])).scalar()
    if connection.execute(db.select([db.func.max(af_metrics_tab.c._time_stamp)])).scalar() != max_ts: raise AdFormException('\n\tE13: V db sa nezhoduju hodnoty poslednych time_stamps v tabulkach af_campaigns a af_metrics!')        


    # 1. tracking period uplynul, pre najnovsi ts je agency_phase = 'open' -> agency_phase = 'closed'
    query1_cond = db.and_(af_campaigns_tab.c._time_stamp > (af_campaigns_tab.c.endDate + pt), af_campaigns_tab.c._agency_phase == 'open', af_campaigns_tab.c._time_stamp == max_ts)
    query1 = af_campaigns_tab.update().where(query1_cond).values(_agency_phase = 'closed')
    connection.execute(query1)


    # 2. tracking period uplynul, pre najnovsi ts je agency_phase = 'closed' -> vymaz vsetky zaznamy so starsimi timestamps v af_campaigns_tab aj v af_metrics_tab
    # zisti id kampani ktore su closed, s uplynutym period tracked a s max_ts a pre tieto kampane vymaz vsetky zaznamy so starsimi timestamps
    # pre tieto kampane sa vymazu aj vsetky metriky so starsimi timestamps - zabezpecene cez FOREIGN KEY v af_metrics
    query2_cond = db.and_(af_campaigns_tab.c._time_stamp > (af_campaigns_tab.c.endDate + pt), af_campaigns_tab.c._agency_phase == 'closed', af_campaigns_tab.c._time_stamp == max_ts)
    query2_id = db.select([af_campaigns_tab.c.id]).where(query2_cond)
    query2_delete = af_campaigns_tab.delete().where(db.and_(af_campaigns_tab.c.id.in_(query2_id), af_campaigns_tab.c._time_stamp != max_ts))
    #query2_checkfordelete = db.select([af_campaigns_tab]).where(db.and_(af_campaigns_tab.c.id.in_(query2_id), af_campaigns_tab.c._time_stamp != max_ts))
    connection.execute(query2_delete)
    
    
    # 3. pre najnovsi ts, pre sparovane kampane z xlsx uzivatelskeho suboru a so stavom 'refresh' -> zapisy do xlsx uzivatelskeho suboru
    df_3 = None
    camp_xlsx_list = df_AF_DB_user[(df_AF_DB_user['_user_merge_AF_DB'] == 'both') & (df_AF_DB_user['_user_Type'] =='refresh')]['id'].tolist()
    if camp_xlsx_list:
        query3 = db.select([af_campaigns_tab]).where(db.and_(af_campaigns_tab.c.id.in_(camp_xlsx_list), af_campaigns_tab.c._time_stamp == max_ts))
        result_set = connection.execute(query3)
        if result_set.rowcount:
            df_3 = pd.DataFrame(result_set.fetchall())
            try:
                df_3.columns = [c.name for c in af_campaigns_tab.columns]
            except ValueError:
                df_3 = pd.DataFrame(columns=[c.name for c in af_campaigns_tab.columns])

            df_3.rename(columns={'name': 'Campaign'}, inplace=True)
            df_3['Log'] = None
            df_3['Type'] = None

            # 3.1 tracking period uplynul, agency_phase = 'closed' -> 
            # a) zmen Type v xlsx na 'refresh-done'
            # b) do Log v xlsx zapis ts z db v tvare 'Uzavreta kampan bola aktualizovana, ts'
            cond_31 = (df_3['_time_stamp'] > (df_3['endDate'] + pt)) & (df_3['_agency_phase'] == 'closed')
            df_3.loc[cond_31, 'Log'] = 'Uzavreta kampan bola aktualizovana ' + session_ts.strftime("%d/%m/%Y, %H:%M:%S")
            df_3.loc[cond_31, 'Type'] = 'refresh-done'

            # 3.2 tracking period neuplynul, agency_phase = 'open' -> 
            # a) zmen Type v xlsx na 'refresh-done'
            # b) do Log v xlsx zapis ts z db v tvare 'Otvorena kampan bola aktualizovana, ts'
            cond_32 = (df_3['_time_stamp'] <= (df_3['endDate'] + pt)) & (df_3['_agency_phase'] == 'open')
            df_3.loc[cond_32, 'Log'] = 'Otvorena kampan bola aktualizovana ' + session_ts.strftime("%d/%m/%Y, %H:%M:%S")
            df_3.loc[cond_32, 'Type'] = 'refresh-done'

            # 3.3 tracking period uplynul, agency_phase = 'error' -> 
            # a) zmen Type v xlsx na 'failed'
            # b) do Log v xlsx zapis ts z db v tvare 'Uzavretu kampan sa nepodarilo aktualizovat, ts'
            cond_33 = (df_3['_time_stamp'] > (df_3['endDate'] + pt)) & (df_3['_agency_phase'] == 'error')
            df_3.loc[cond_33, 'Log'] = 'Uzavretu kampan sa nepodarilo aktualizovat ' + session_ts.strftime("%d/%m/%Y, %H:%M:%S")
            df_3.loc[cond_33, 'Type'] = 'failed'

            # 3.4 tracking period neuplynul, agency_phase = 'error' -> 
            # a) zmen Type v xlsx na 'failed'
            # b) do Log v xlsx zapis ts z db v tvare 'Otvorenu kampan sa nepodarilo aktualizovat, ts'
            cond_34 = (df_3['_time_stamp'] <= (df_3['endDate'] + pt)) & (df_3['_agency_phase'] == 'error')
            df_3.loc[cond_34, 'Log'] = 'Otvorenu kampan sa nepodarilo aktualizovat ' + session_ts.strftime("%d/%m/%Y, %H:%M:%S")
            df_3.loc[cond_34, 'Type'] = 'failed'

            df_error_xlsx = df_error_xlsx.append(df_3[['Campaign', 'Log', 'Type']], sort=False, ignore_index=False)

    if df_error_xlsx.size:
        af_logger.warning(f'\n\tW01: Niektore kampane sa nepodarilo aktualizovat:\n'
            f'\t{df_error_xlsx}')
        try:
            WriteLogToExcel(df_error_xlsx)
        except:
            af_logger.warning(f'\n\tW02: Problem so zapisom do uzivatelskeho xlsx suboru!\n')

    # 4. tracking period neuplynul, pre najnovsi ts je agency_phase = 'open' -> vymaz zaznamy pre agency_phase = 'open' alebo 'error' so starsimi timestamps v af_campaigns_tab aj v af_metrics_tab
    # pre tieto kampane sa vymazu aj vsetky metriky so starsimi timestamps - zabezpecene cez FOREIGN KEY v af_metrics
    query4_cond = db.and_(af_campaigns_tab.c._time_stamp <= (af_campaigns_tab.c.endDate + pt), af_campaigns_tab.c._agency_phase == 'open', af_campaigns_tab.c._time_stamp == max_ts)
    query4_id = db.select([af_campaigns_tab.c.id]).where(query4_cond)
    query4_delete = af_campaigns_tab.delete().where(db.and_(af_campaigns_tab.c.id.in_(query4_id), af_campaigns_tab.c._time_stamp != max_ts))
    connection.execute(query4_delete)
    # query4_checkfordelete = db.select([af_campaigns_tab]).where(db.and_(af_campaigns_tab.c.id.in_(query4_id), af_campaigns_tab.c._time_stamp != max_ts))


    # 5. pre najnovsi ts je agency_phase = 'error' -> vymaz zaznamy pre agency_phase = 'error' so starsimi timestamps v af_campaigns_tab
    query5_cond = db.and_(af_campaigns_tab.c._agency_phase == 'error', af_campaigns_tab.c._time_stamp == max_ts)
    query5_id = db.select([af_campaigns_tab.c.id]).where(query5_cond)
    query5_delete = af_campaigns_tab.delete().where(db.and_(af_campaigns_tab.c.id.in_(query5_id), af_campaigns_tab.c._time_stamp != max_ts, af_campaigns_tab.c._agency_phase == 'error'))
    connection.execute(query5_delete)
    #query5_checkfordelete = db.select([af_campaigns_tab]).where(db.and_(af_campaigns_tab.c.id.in_(query5_id), af_campaigns_tab.c._time_stamp != max_ts, af_campaigns_tab.c._agency_phase == 'error'))


    # 7. pre najnovsi ts je agency_phase = 'error' -> vymaz zaznamy pre agency_phase = 'error' so starsimi timestamps v af_campaigns_tab
    query7_cond = db.and_(af_campaigns_tab.c._agency_phase == 'error', af_campaigns_tab.c._time_stamp == max_ts)
    query7 = db.select([af_campaigns_tab.c.name]).where(query7_cond)
    result_set7 = connection.execute(query7)
    if result_set7.rowcount:
        err_camp_list = pd.DataFrame(result_set7.fetchall())[0].tolist()
        af_logger.warning(f'\n\tW03: Pre tieto kampane sa nepodarilo ziskat udaje z Adform:\n'
            f'\t{err_camp_list}')        


    # 9. tracking period uplynul, pre najnovsi ts je agency_phase = 'error', pre starsi ts je agency_phase = 'open' 
    query9_id_list = None
    query9_cond = db.and_(af_campaigns_tab.c._time_stamp > (af_campaigns_tab.c.endDate + pt), af_campaigns_tab.c._agency_phase == 'error', af_campaigns_tab.c._time_stamp == max_ts)
    query9_id = db.select([af_campaigns_tab.c.id]).where(query9_cond)
    result_set9 = connection.execute(query9_id)
    if result_set9.rowcount:
        query9_id_list = pd.DataFrame(result_set9.fetchall())[0].tolist()

    # 9a. a ak je starsi ts kampane > end tak -> vymaz zaznam pre agency_phase = 'error' s aktualnym timestamp v af_campaigns_tab a pre starsiu kampan daj agency_phase = 'closed'
    # starsia 'open' kampan, ktorej ts je po 'end', moze nadobudnut stav 'closed' aj ked to nie je z posledneho dna trackovacieho obdobia
    if query9_id_list:
        query9a_id = db.select([af_campaigns_tab.c.id]).where(db.and_(af_campaigns_tab.c.id.in_(query9_id_list), af_campaigns_tab.c._time_stamp > af_campaigns_tab.c.endDate, af_campaigns_tab.c._agency_phase == 'open', af_campaigns_tab.c._time_stamp != max_ts))
        result_set9a = connection.execute(query9a_id)
        if result_set9a.rowcount:
            query9a_id_list = pd.DataFrame(result_set9a.fetchall())[0].tolist()

            # vymaz error s najnovsim ts
            query9a_delete = af_campaigns_tab.delete().where(db.and_(af_campaigns_tab.c.id.in_(query9a_id_list), query9_cond))
            connection.execute(query9a_delete)
            # query9a_checkfordelete = db.select([af_campaigns_tab]).where(db.and_(af_campaigns_tab.c.id.in_(query9a_id_list), query9_cond))

            # zmen open na closed pre starsi ts, v af_metrics netreba ziadne zmeny
            query9a_update = af_campaigns_tab.update().where(db.and_(af_campaigns_tab.c.id.in_(query9a_id_list), af_campaigns_tab.c._time_stamp > af_campaigns_tab.c.endDate, af_campaigns_tab.c._agency_phase == 'open', af_campaigns_tab.c._time_stamp != max_ts)).values(_agency_phase="closed")
            connection.execute(query9a_update)
            
    # 9b. a ak je starsi ts kampane <= end tak -> vymaz zaznam pre agency_phase = 'open' so starym timestamp v af_campaigns_tab a af_metrics_tab
    # starsia 'open' kampan, ktorej ts je pred 'end' nema dokoncene stahovanie za vsetky dni preto sa vymaze, takisto aj jej metriky - zabezpecene cez FOREIGN KEY v af_metrics
    if query9_id_list:
        query9b_id = db.select([af_campaigns_tab.c.id]).where(db.and_(af_campaigns_tab.c.id.in_(query9_id_list), af_campaigns_tab.c._time_stamp <= af_campaigns_tab.c.endDate, af_campaigns_tab.c._agency_phase == 'open', af_campaigns_tab.c._time_stamp != max_ts))
        result_set9b = connection.execute(query9b_id)
        if result_set9b.rowcount:
            query9b_id_list = pd.DataFrame(result_set9b.fetchall())[0].tolist()
            
            # vymaz open so starsim ts z af_campaigns, automaticky aj v af_metrics
            query9b_delete = af_campaigns_tab.delete().where(db.and_(af_campaigns_tab.c.id.in_(query9b_id_list), af_campaigns_tab.c._time_stamp <= af_campaigns_tab.c.endDate, af_campaigns_tab.c._agency_phase == 'open', af_campaigns_tab.c._time_stamp != max_ts))
            connection.execute(query9b_delete)


    # 10. tracking period uplynul, pre najnovsi ts je agency_phase = 'error' a existuje zaznam so starsim ts a agency_phase = 'closed'  -> vymaz zaznam s aktualnym timestamp a agency_phase = 'error' v af_campaigns_tab
    # vyuzije sa zoznam id z bodu 9
    if query9_id_list:
        query10_id = db.select([af_campaigns_tab.c.id]).where(db.and_(af_campaigns_tab.c.id.in_(query9_id_list), af_campaigns_tab.c._agency_phase == 'closed', af_campaigns_tab.c._time_stamp != max_ts))
        result_set10 = connection.execute(query10_id)
        if result_set10.rowcount:
            query10_id_list = pd.DataFrame(result_set10.fetchall())[0].tolist()

            # vymaz error s najnovsim ts
            query10_delete = af_campaigns_tab.delete().where(db.and_(af_campaigns_tab.c.id.in_(query10_id_list), query9_cond))
            connection.execute(query10_delete)
            #query10_checkfordelete = db.select([af_campaigns_tab]).where(db.and_(af_campaigns_tab.c.id.in_(query10_id_list), query9_cond))

    #endregion



    connection.close()
    af_logger.info('L02: Uspesne ukoncenie adform scriptu')
