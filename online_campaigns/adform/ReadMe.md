# Konfiguracne nastavenia
Konfiguracny subor settings.json: [W:\General\DATA SCIENCE\Support\PB\settings.json](file://W:/General/DATA%20SCIENCE/Support/PB/settings.json)  
Uzivatelsky subor support_data.xlsx: [W:\General\DATA SCIENCE\Support\PB\support_data.xlsx](file://W:/General/DATA%20SCIENCE/Support/PB/support_data.xlsx)

## Adform settings  
Subor settings.json:

- **async-use-max-records** - pri asynchro requestoch bez definovaneho argumentu limit sa vyziada max. pocet riadkov ak hodnota je *true*, pri hodnote *false* sa vyziada default pocet riadkov  
- **sync-use-max-records** - pri synchro requestoch bez definovaneho argumentu limit sa vyziada max. pocet riadkov ak hodnota je *true*, pri hodnote *false* sa vyziada default pocet riadkov  
- **"async-max-records"** - maximalny pocet riadkov, ktory moze vratit asynchronny prikaz, [ak je 0, vracia unlimited response](https://api.adform.com/help/references/buyer-solutions/reporting/stats)  
- **"sync-max-records"** - maximalny pocet riadkov, ktory moze vratit synchronny prikaz. Vid nizsie popis pri metode *call_paging*  
- **"async-sleep-time"** - bazicky cas v sec ktory caka medzi dopytovanim na stav operacie, k nemu sa v kazdom dalsom kole pridava counter*3 sec  
- **"async-rounds"** - pocet opakovani dopytovania na stav operacie, po vycerpani sa POST prikaz opakuje  
- **"period-tracked"** - pocet dni sledovaneho obdobia. Data z kampane sa spracuju ak koniec kampane je v obdobi od dna spracovania minus period-tracked dni  
- **"campaign-status"** - data akych kampani sa maju stiahnut v prvom kroku, bolo by fajn aby ich bolo co najmenej, da sa to riadit oznacovanim kampani v adform platforme. S tymito kampanami sa dalej pracuje a vyhodnocuju traffic filters a points. List stringov, moznosti:  *'Active','Inactive'*
- **"delay-weekly"** - pocet dni po ktorom sa spracuju tyzdenne data kampane (0=v nedelu, 1=v pondelok, 2=v utorok,...)  
- **"delay-monthly"** - pocet dni po ktorom sa spracuju mesacne data kampane (0=v poslednom dni aktualneho mesiaca, 1=v prvom dni nasled. mesiaca, ...)  
- **"delay-campaign"** - pocet dni po ktorom sa spracuju kampanove data kampane (0=v poslednom dni kampane, 1=v prvom dni po skonceni kampane, ...)  
- **"delay-custom"** - pocet dni po ktorom sa spracuju data kampane pre custom period (0=v poslednom dni custom period, 1=v prvom dni po konci custom period, ...)  

## Adform_lib dokumentacia

Logovanie obsahuje identifikatory *'Lxy'* pre lahsiu identifikaciu miesta v kode, kde sa zapisal log.  
Vyvolanie vynimky **AdformException** obsahuje identifikatory *'Exy'* pre lahsiu identifikaciu miesta v kode, kde vznikla vynimka.  
Premenna **dev** je pre ucely testovania pocas pisania kodu s hodnotou *True*, v ostrej prevadzke jej nastav *False*. Pouziva sa napr. pre vytvorenie pomocneho suboru s tokenom.  
Funkcia **adform_main** je hlavna funkcia pre stahovanie dat z Adformu.

### class af_request  
Pre kazdy request sa vytvori jedna instancia tejto triedy ktora zabezpeci jeho spracovanie.  
Atributy a metody s prefixom 'c_' ktore su spolocne pre vsetky instancie:  
Spolocne konstanty:
- **c_API**: base url pre vsetky requesty, ulozena v objekte c_session  
- **c_API_VERSION**: pri zmene [verzie API](https://api.adform.com/help/guides/what-do-you-need-to-know-about-adform-apis/versioning) na strane servera staci zmenit tu  
- **c_REQUEST_LIMIT_PER_DAY**: [denny limit na pocet volani](https://api.adform.com/help/references/buyer-solutions/reporting/stats), nikde sa v kode nekontroluje aby sme sa neobmedzovali, ak sa kontroluje na strane servera s nejakou rezervou. Takisto nie je uplne jasne, ktore requesty sa do limitu pocitaju (ci len POST_async alebo aj GET_op a GET_data, zrejme len POST_async). Pri prekroceni denneho limitu sa vyvola vo funkcii self.call_api vynimka AdFormException a script adformu skonci. Preto z casu na cas skontroluj log, ci sa tam nevyskytuje casto *"Response code: 429"*  
- **c_ASYNC_MAX_METRICS**: max. [pocet metrik](https://api.adform.com/help/references/buyer-solutions/reporting/stats) pre asynchro volanie. V kode sa metriky sa automaticky nakrajaju podla tejto hodnoty.  
- **c_ASYNC_MAX_DIMENSIONS**: max. [pocet dimenzii](https://api.adform.com/help/references/buyer-solutions/reporting/stats) pre asynchro volanie.  
- **c_TOKEN_FILE**: cesta k tokenu. Uzitocne len pocas vyvoja, v ostrej prevadzke sa nepouziva vdaka *dev = False*, vid vyssie  

Spolocne atributy:
- **c_count_per_day**: dict pocitadiel pre jednotlive requesty. *POST_async, GET_data a GET_op* su pre asynchronne requesty, ostatne su pre synchronne. *GET_free* je request, ktory nema limit na pocet volani za casovu periodu, *token* je request pre ziskanie tokenu, *others* su ostatne requesty.  
- **c_session**: objekt odvodeny od SessionWithUrlBase ktory uchovava base url z **c_API** a token. Pocas vyvoja sa token moze ziskat z pomocneho suboru aby sa obmedzili requesty na token pri kazdom novom spusteni kodu. Pri prvom spusteni kodu pocas testovania vymaz subor s tokenom aby sa vyziadal novy token.  
- **c_creds**: credentials pre komunikaciu s adform serverom, nacitane zo suboru *credentials.json* v home adresari  
- **c_user_camp_custom_df**: zoznam kampani, ktore maju nestandardne (t.j. uzivatelske) obdobia pre vyhodnotenie  
- **c_user_camp_refresh_df**: zoznam kampani, ktore je potrebne znova refreshnut  
- **c_user_settings**: uzivatelske nastavenia pre adform zo suboru [settings.json](file://W:/General/DATA%20SCIENCE/Support/PB/settings.json). Atributy su popisane v sekcii Adform settings.  
- **c_api**: dict roznych typov requestov s ich [limitmi](https://api.adform.com/help/references/buyer-solutions/reporting/stats) na pocet volani za casovu periodu v sekundach. Konkretna funkcia z triedy sa do kluca *'fnc'* priradi po tom, ako je funkcia zadefinovana.  

Spolocne metody:  
- **c_init**: nacita credentials, vytvori *c_session* objekt, nacita nastavenia adform kampani z uzivatelskeho xlsx suboru a uzivatelske adform nastavenia. Ak je dev = True, token sa pokusi najskor ziskat zo suboru.  
- **c_update_session**: vytvori *c_session* objekt s tokenom a base url pre vsetky volania. Pocas vyvoja pouziva pomocny subor s tokenom. Ak pocas vykonavania kodu vyprsi platnost tokenu, automaticky sa zavola tato funkcia a token sa obnovi.   
- **c_create_column_mapping**: vytvori mapovaci dict tym, ze vyziada vsetky dostupne dimenzie a metriky s ich povodnym databazovymi a vystupnymi ("peknymi") uzivatelskymi nazvami. Dokaze vyuzit expirovany token (napr. zo suboru pocas developovania), nie vsak neplatny token.  
- **c_api_fnc_POST_async**: odosle asynchronny request s datami z argumentu *arg_json* v argumente *json* requestu a zadanym *header* v argumente *headers* requestu. Navysi pocet *POST_async* requestov a zapise info do logu. Ak medzitym vyprsi platnost tokenu, t.j. v response je *status_code==401*, tak automaticky si vyziada novy token.  
Pre tento aj nasledujuce funkcie pre odoslanie requestov sa vyuzivaju dekoratory s nasledujucou logikou:  
  - *@limits* kontroluje pocet volani *calls* za casovu periodu *period* (tieto hodnoty su definovane pre kazdy typ requestu zvlast v dicte *c_api*). Ak dojde k prekroceniu poctu volani, tak vyvola vynimku ratelimit.RateLimitException.  
  - *@sleep_and_retry* odchyti vynimku ratelimit.RateLimitException a pozastavi vykonavanie na pocet sekund podla argumentu *period* v *@limits*  
  - *@on_exception* zachyti vynimky typu RequestException (=ConnectionError, HTTPError, Timeout, TooManyRedirects) a pozastavi vykonavanie scriptu na pocet sekund podla argumentu *interval*. Toto cele bude opakovat maximalne pocas periody definovanej v argumente *max_time*. Potom vrati vykonavanie spat do volajucej funkcie.  
  - *@on_predicate* sa pozrie na navratovu hodnotu funkcie (t.j. response) a ak je v nom *status_code == 429*, t.j. Quota limit exceeded, tak opakuje volanie funkcie *max_tries* krat kazdych *interval* sekund. Je to dodatocna kontrola na pocet volani k dekoratoru *@limits*. Ak sa prekroci denny limit volani, tak sa volanie vykona *max_tries* krat a skonci.  
- **c_api_fnc_GET_op**: odosle request na stav asynchronneho requestu. V argumente *url_postfix* je len postfix url adresy (alfanumericky kod), kde sa ma dopytovat a *header* obsahuje udaje pre argument *headers* requestu. Navysi pocet *GET_op* requestov a zapise info do logu. Ak medzitym vyprsi platnost tokenu, t.j. v response je *status_code==401*, tak automaticky si vyziada novy token. Vyznam dekoratorov vid popis k *c_api_fnc_POST_async*  
- **c_api_fnc_GET_data**: odosle request na data k asynchronnemu requestu. V argumente *url_postfix* je len postfix url adresy (alfanumericky kod), kde sa ma dopytovat a *header* obsahuje udaje pre argument *headers* requestu. Navysi pocet *GET_data* requestov a zapise info do logu. Ak medzitym vyprsi platnost tokenu, t.j. v response je *status_code==401*, tak automaticky si vyziada novy token. Vyznam dekoratorov vid popis k *c_api_fnc_POST_async*  
- **c_api_fnc_GET_free**: odosle request na data k beznemu synchronnemu requestu. V argumente *url_complete_postfix* je cely zvysok url adresy vratane parametrov, *arg_json* obsahuje data do tela requestu a *header* obsahuje udaje pre argument *headers* requestu. Navysi pocet *GET_free* requestov a zapise info do logu. Ak medzitym vyprsi platnost tokenu, t.j. v response je *status_code==401*, tak automaticky si vyziada novy token. Vyznam dekoratorov vid popis k *c_api_fnc_POST_async*. Netreba riesit pocet volani za casovu periodu, lebo pre tento typ requestu nie su definovane limity.  

Metody su spolocne preto, aby vsetky instancie triedy posielali requesty cez  metody, na ktorych su nastavene pocitadla volani pre vsetky instancie spolu a nie pre kazdu instanciu zvlast. Takisto staci mat jeden objekt *c_session*, a *c_create_column_mapping* ktore budu vyuzivat vsetky instancie.  

Metody triedy:  
- **__init__**  
  Vstupne argumenty:  
  - *fnc_key*: textovy popis funkcie, ktora sa ma zavolat, kluc do dict *c_api*  
  - *url_postfix*: postfix k bazovej url, bez parametrov  
  - *url_parameters*: postfix k bazovej url, len parametre  
  - *json*: data (dict) pre request  
  - *header*: udaje (dict) do headers requestu  
  
  Inicializuje atributy:
  - *fnc_key*: textovy popis funkcie, ktora sa ma zavolat, kluc do dict *c_api*  
  - *fnc*: funkcia, ktora sa ma zavolat, objekt funkcie zodpovedajuci *fnc_key*  
  - *url_postfix*: postfix k bazovejurl, posledna cast url adresy pred parametrami  
  - *url_parameters*: postfix k bazovej url+specifickej url, su to parametre po ? v url pre synchro prikazy, dict s objektami, ktore su preformatovane do stringov  
  - *json*: data pre request  
  - *metrics_headers*: zoznam metric's keywords vytvoreny zo vstupneho json['metrics']  
  - *header*: data pre header requestu  
  - *is_asynchro*: True ak je request asynchronny  
  - *response*: finalny response  
  - *response_asynchro*: odpoved na odoslany asynchronny POST, inak None  
  - *response_operation*: odpoved na odoslany GET operation pre asynchronny POST, inak None  
  - *limit*: poziadavka na pocet riadkov v odpovedi. V requeste bud limit nie je zadany alebo je konkretna hodnota zadana ako argument. Asynchro prikazy maju limit zadany v *json.paging* strukture, synchro prikazy v *url_parameters*. Hodnoty pre *limit* sa spocitaju nasledovne:  
    - **limit nie je definovany** - request bude pracovat podla hodnot v *async-use-max-records* a *sync-use-max-records* v [setting.json](file://W:/General/DATA%20SCIENCE/Support/PB/settings.json) nasledovne:  
      - s aktualnou default hodnotou ktora je urcena Adformom v case volania pre synchro request ak *sync-use-max-records = false* (aktualna hodnota je 100 alebo bez obmedzenia)
      - s aktualnou default hodnotou ktora je urcena Adformom v case volania pre asynchro request ak *async-use-max-records = false* ((aktualna hodnota je 100 000)
      - s max. hodnotou ktora je manualne zistena z chybovych hlasok (vid nizsie) pre synchro request ak *sync-use-max-records = true*. (aktualna hodnota je 10 000)
      - s max. hodnotou ktora je urcena Adformom v case volania pre asynchro request ak *async-use-max-records = true* (aktualna hodnota pre pracu s max poctom je 0) 
    - **limit = 0** - request bude pracovat s maximalnym poctom riadkov, ktore su definovane v *async-max-records* a *sync-max-records* setting.json
    - **limit = n** - request si vyziada **n** riadkov
  - *json['includeRowCount']=True* (pre asynchro) alebo *header['Return-Total-Count']* (pre synchro): vzdy sa bude vyzadovat informacia o celkovom pocte riadkov  
  
  Pre asynchro requesty nasleduje kontrola poctu dimenzii (0-8) a ich spravnych nazvov, poctu metrik (> 0) a ich spravnych nazvov. Zaroven sa pre metriky generuje detailnejsia struktura *metrics_spec*, ktora vstupuje do requestu. Zdroj struktur je v *dims_metrics.py*. Nakoniec sa skontroluje, ci vstupuje povinny datumovy parameter.  

- **call_api**  
  Zavola prislusnu funkciu z *af_request.c_api* dictu cez *self.fnc*.  
  Ak je volanie synchronne, vrati hned response. Finalne data su  v *self.response*.  
  Ak je volanie asynchronne, volanim cez *af_request.c_api_fnc_GET_op* caka na data, cez *af_request.c_api_fnc_GET_data* ich ziska a vrati response. Vsetky odpovede su ulozene v atributoch *self.response[_xyz]*, finalne data su  v *self.response*.  

  Ak nastane prekrocenie denneho limitu, funkcie *self.fnc* vratia response so *status_code == 429* a nasledne sa hned vyvola vynimka *AdFormException* a adform script skonci.  
  Decorator *@on_predicate* sa zavola celkovo *max_tries* krat, po uplynuti *interval* sekund od skoncenia funkcie a riesi situaciu, ak treba znovu zopakovat volanie kvoli 
  - zlyhaniu spracovania dat na strane servera - t.j. *af_request.c_api_fnc_GET_op* vrati *'status=failed'*  
  - ak pocas 10 opakovani v 2sec intervaloch sa nedosiahne stav asynchro operacie *'succeeded'* alebo *'failed'*  

  Metoda vracia dict, v ktorom
  - *'response'* je odpoved zo servera, moze byt aj *None*!  
  - *'repeat_call'* je informacia pre decorator, ci ma opakovat volanie funkcie kvoli nestandardnemu stavu pocas spracovania asynchronneho volania  

- **call_paging**  
  Zavola call_api a zabezpeci strankovanie. Vrati list s response objektami. Ak je niektory response None (t.j. GET_data prikazu sa nepodarilo odobrat data ani po viacerych pokusoch, asi kvoli docasnej chybe servera), generuje sa *AdFormException*. Vsetky response objekty v liste su korektne. Ak sa pocas spracovania vyskytol iny kod ako 200 alebo 202, resp. 401 pre refresh tokenu, generovala sa vynimka.  
  
  Pocet vratenych riadkov sa da riadit argumentom *limit* v requeste (synchro ho ma v *url_parameters* a asynchro v body v strukture *paging*).  
  
  Paging suvisi s limitom na pocet requestov za dany den. Kedze limit je definovany pre asynchro requesty, tak pre synchro requesty sa moze pouzivat default hodnota poctu riadkov, ktora je aktualne = 100, a neobmedzovat sa poctom volani synchro requestov. Vyziadanie default poctu riadkov pre requesty bez definovaneho argumentu *limit* je urcene polozkou *sync-use-max-records = false* v setting.json.  
  Pri asynchro prikazoch je default hodnota aktualne 100 000 a to je postacujuce, takze aktualne sa aj pre asynchro requesty bez definovaneho argumentu *limit* pouziva default hodnota. Vyziadanie default poctu riadkov je urcene polozkou *async-use-max-records = false* v settings.json. V buducnosti sa default hodnoty pre asynchro requesty mozu znizit a ak nebudu postacovat, t.j. prekroci sa limit na pocet volani za den, tak nastavenim polozky *async-use-max-records = true* v setting.json sa vyziada max. pocet riadkov a mozeme dosiahnut plnenie limitu. Kod tym padom nie je potrebne upravovat a doplnat limity do prikazov, ale namiesto default strankovania sa bude pouzivat maximalne.  
  
  V polozkach *async-max-records* a *sync-max-records* setting.json su definovane max. hodnoty. Pri asynchro requestoch je maximalny pocet riadkov neobmedzeny a je to v requeste vyziadane cez *limit = 0*. Preto je aj v setting.json polozka *async-max-records = 0*.  
  Pri synchro requestoch to nie je Adformom definovane, preto sa pouzije uzivatelska hodnota *sync-max-records* z settings.json. Bola manualne zistena z chybovych hlasok, ktore vracali niektore synchronne prikazy (napr. so zoznamom kampani) pri prekroceni povolenej hodnoty pre argument *limit*. Rozne synchro prikazy to maju rozne nastavene, napr. niektore asi nemaju ziadny limit. V case pisania kodu bola odpozorovana minimalna hodnota 10 000. Hodnotu treba obcas skontrolovat, ci je aktualna napr. prikazom https://api.adform.com/v1/buyer/campaigns?limit=100000 a preskumanim odpovede.  

  V kode je pouzity prisposobeny [algoritmus](https://api.adform.com/help/references/buyer-solutions/reporting/stats/paging) na vypocet offsetu a limity doporuceny Adformom.  

- **get_response**  
  Odosle request cez call_paging a vystup spoji do df ak vstupny parameter *join_df = True*. Inak vrati list povodnych responsov z adform servera.  
  Pri asynchro navyse pre odoslanim requestu rozdeli metriky ak treba. V takom pripade vsak pre opatovne spojenie metrik musi byt join_df = True.  
  
  Pri asynchro: ak je **metrik > *c_ASYNC_MAX_METRICS* a *join_df = False***, vystupom je list = matica, stlpce = zodpovedaju dimensions + chunky metrik, riadky = responses (kazdy response zodpoveda jednej stranke)  
  Pri asynchro: ak je **metrik <= *c_ASYNC_MAX_METRICS* a *join_df = False***, vystupom je list s responses (kazdy response zodpoveda jednej stranke)  
  Pri asynchro: ak ***join_df = True***, vystupom je df so spojenymi metrikami a strankami, s nazvami stlpcov podla klucov v *metrics_dict* a *dimensions_list*.  
  
  Pri synchro: ak ***join_df = False***, vystupom je list s responses (kazdy response zodpoveda jednej stranke)  
  Pri synchro: ak ***join_df = True***, vystupom je df so spojenymi strankami  
