import json
import urllib.parse
from pathlib import Path
import pandas as pd
import requests

LOG_FILE = Path(r'\\srvdc01\STARMEDIA\General\DATA SCIENCE\Support\PB\log.txt')
SUPPORT_FILE = Path(r'\\srvdc01\STARMEDIA\General\DATA SCIENCE\Support\PB\support_data.xlsx')
SETTINGS_FILE = Path(r'\\srvdc01\STARMEDIA\General\DATA SCIENCE\Support\PB\settings.json')

MAILHOST = ('192.168.1.21', 25)         #To specify a non-standard SMTP port, use the (host, port) tuple format for the mailhost argument
SEND_WARNING_ALERT_TO = ['rudolf.gilanik@starmedia.sk']
SEND_ERROR_ALERT_TO = ['rudolf.gilanik@starmedia.sk']
SEND_ALERT_FROM = ['DigitalDataLogger@starmedia.sk']

class SessionWithUrlBase(requests.Session):
    # In Python 3 you could place `url_base` after `*args`, but not in Python 2.
    def __init__(self, url_base=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.url_base = url_base

    def request(self, method, url, **kwargs):
        modified_url = urllib.parse.urljoin(self.url_base, url)
        return super().request(method, modified_url, **kwargs)
    
    def get_complete_url_with_params(self, postfix_url = '', params = None):
        """
        postfix_url - postfixova cast adresy ktora sa pripaja k url_base \n
        params - parametre ktore su sucastou url adresy po ?, vsetko ako string, napr. list stringov skonvertuj cez ','.join(list) \n
        """
        return urllib.parse.urljoin(self.url_base, postfix_url) + self.get_params_for_url(params)
    
    def get_postfix_url_with_params(self, postfix_url = '', params = None):
        """
        postfix_url - postfixova cast adresy ktora sa pripaja k url_base \n
        params - parametre ktore su sucastou url adresy po ?, vsetko ako string, napr. list stringov skonvertuj cez ','.join(list) \n
        """         
        #return (postfix_url if postfix_url is not None else '') + self.get_params_for_url(params)
        return postfix_url + self.get_params_for_url(params)

    def get_params_for_url(self, params):
        """
        V params musi byt vsetko ako string, napr. list stringov skonvertuj cez ','.join(list)
        """
        result = ''
        if (params is not None) and (params != {}) and (params != ''): 
            result = '?' + urllib.parse.urlencode(params)
        return result
        



def find_first_sublist(seq, sublist, start=0):
    length = len(sublist)
    for index in range(start, len(seq)):
        if seq[index:index+length] == sublist:
            return index, index+length

def replace_sublist(seq, sublist, replacement):
    """Replace sublist in a list.
    
    # source: https://stackoverflow.com/questions/12898023/replacing-a-sublist-with-another-sublist-in-python

    Usage::

        >>> a = [1, 3, 5, 10, 13]
        >>> replace_sublist(a, [3, 5, 10], [9, 7])
        >>> a
        [1, 9, 7, 13]
    """
    length = len(replacement)
    index = 0
    for start, end in iter(lambda: find_first_sublist(seq, sublist, index), None):
        seq[start:end] = replacement
        index = start + length

class custom_period_type():
    def __init__(self):
        self.df = pd.read_excel(SUPPORT_FILE, sheet_name = 'Periods', header = 1)

    def get_platform_type(self, platform, ptype = None):
        """
        Vyfiltruje polozky pre danu platformu. Volitelne sa da definovat aj typ pre danu platformu.
        """
        return self.df.query('Platform == @platform' + ('& Type == @ptype' if ptype else ''))

class custom_configuration():
    def __init__(self):
        self.settings = json.load(open(SETTINGS_FILE))


